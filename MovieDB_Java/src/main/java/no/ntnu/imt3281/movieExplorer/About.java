package no.ntnu.imt3281.movieExplorer;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import no.ntnu.imt3281.dataHandling.DBHandler;
import no.ntnu.imt3281.dataHandling.TheMovieDBConfiguration;

/**
 * controller class for popup-window
 * 
 * @author HKNormann
 *
 */
public class About {

    private static TheMovieDBConfiguration configuration = null;

    @FXML // fx:id="text"
    private TextArea text; // Value injected by FXMLLoader

    @FXML
    void emptyCache(ActionEvent event) {
	configuration.emptyCache();
	setText();
    }

    @FXML
    void emptyDatabase(ActionEvent event) {
	DBHandler.wipeDB();
	setText();
    }

    /**
     * close stage
     * 
     * @param event
     */
    @FXML
    void exit(ActionEvent event) {
	Stage stage = (Stage) text.getScene().getWindow();
	stage.close();
    }

    /**
     * sets dialogtext
     */
    public void setText() {
	text.setText(getDialogText());
    }

    /**
     * sets configuration
     * 
     * @param config
     */
    public void setConfig(TheMovieDBConfiguration config) {
	About.configuration = config;
    }
    
    /**
     * gets String describing storage usage of application formatted to be put in
     * popup window
     * 
     * @return Assembled string of application information
     */
    private String getDialogText() {
	String returnText = "Diskforbruk:" + "\nDatabasen\t" + DBHandler.getDBSize() + "\nPosterbilder\t"
		+ configuration.getImagesSize("poster") + "\nProfilbilder\t"
		+ configuration.getImagesSize("profile") + "\nBackdropbilder\t"
		+ configuration.getImagesSize("backdrop") + "\nLogobilder\t"
		+ configuration.getImagesSize("logo") + "\nStillbilder\t"
		+ configuration.getImagesSize("still");
	return returnText;
    }

}
