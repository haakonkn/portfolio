package no.ntnu.imt3281.movieExplorer;

import java.io.IOException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.imt3281.dataHandling.JSON;
import no.ntnu.imt3281.dataHandling.Search;
import no.ntnu.imt3281.dataHandling.TheMovieDBConfiguration;

/**
 * @author HKNormann
 *controller class for handling GUI
 */
public class GUI {
    @FXML
    private TextField searchField;
    @FXML
    private TreeView<SearchResultItem> searchResult;
    @FXML
    private Pane detailPane;

    // Root node for search result tree view
    private TreeItem<SearchResultItem> searchResultRootNode = new TreeItem<SearchResultItem>(new SearchResultItem(""));
    private Details detailsController;
    private static TheMovieDBConfiguration config;

    /**
     * Called when the object has been created and connected to the fxml file. All
     * components defined in the fxml file is ready and available. Spawns the detail
     * window and sets listener functionalities
     * 
     * change listener code gotten from
     * https://stackoverflow.com/questions/13857041/tree-item-select-event-in-javafx2
     * at 17:00 - 07.12.2017
     */
    @FXML
    public void initialize() {
	searchResult.setRoot(searchResultRootNode);
	setChangeListener();
	config = new TheMovieDBConfiguration(Search.getConfig());
	spawnDetailsWindow();
    }

    /**
     * user clicks about button. Brings up popup menu
     * 
     * @param event
     */
    @FXML
    void about(ActionEvent event) {

	FXMLLoader loader = new FXMLLoader(getClass().getResource("About.fxml"));
	final Stage dialog = new Stage();
	dialog.initModality(Modality.APPLICATION_MODAL);
	dialog.initOwner(new Stage());
	About dialogController = null;

	try {
	    BorderPane about = (BorderPane) loader.load();
	    dialogController = loader.getController();
	    dialogController.setConfig(config);
	    dialogController.setText();
	    Scene dialogScene = new Scene(about);
	    dialog.setScene(dialogScene);
	    dialog.show();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }



    /**
     * user clicks preferences button. Brings up file chooser for image folder
     * 
     * @param event
     */
    @FXML
    void preferences(ActionEvent event) {
	detailsController.showPreferences();
    }

    /**
     * spawns Details.fxml window inside detailPane
     */
    private void spawnDetailsWindow() {
	FXMLLoader loader = new FXMLLoader(getClass().getResource("Details.fxml"));
	try {
	    VBox detailWindow = (VBox) loader.load();
	    detailsController = loader.getController();
	    detailsController.setConfig(config);
	    detailPane.getChildren().addAll(detailWindow);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Defines the change listener for the search result tree view Makes it so that
     * the user can click on movies to search for actors in movie or click on actors
     * to search for movies they've been in
     */
    private void setChangeListener() {
	searchResult.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
	    @Override
	    public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
		TreeItem<SearchResultItem> selectedItem = (TreeItem<SearchResultItem>) newValue;

		if (selectedItem != null) {
		    if (selectedItem.getValue().getMediaType().equals("person")) { // get movies with actor
			// get json object result containing all movies with actor
			JSON result = Search.takesPartIn(selectedItem.getValue().getID()).get("results");
			selectedItem.getChildren().clear();
			for (int i = 0; i < result.size(); i++) { // add movies as child nodes of actor
			    JSON movie = result.get(i);
			    SearchResultItem item = new SearchResultItem(movie, "takesPartIn");
			    selectedItem.getChildren().add(new TreeItem<SearchResultItem>(item));
			}
			selectedItem.setExpanded(true);

		    } else if (selectedItem.getValue().getMediaType().equals("movie")) { // get actors in movie
			int movieID = (int) selectedItem.getValue().getID();
			JSON result = Search.actors(movieID).get("cast");
			selectedItem.getChildren().clear();
			for (int i = 0; i < result.size(); i++) { // add actors as child nodes of movie
			    JSON actor = result.get(i);
			    SearchResultItem item = new SearchResultItem(actor, "actors");
			    selectedItem.getChildren().add(new TreeItem<SearchResultItem>(item));
			}
			selectedItem.setExpanded(true);

			detailsController.displayMovie(movieID);
		    }
		}
	    }
	});
    }

    @FXML
    /**
     * Called when the search button is pressed or enter is pressed in the
     * searchField. Perform a multiSearch using theMovieDB and add the results to
     * the searchResult tree view.
     * 
     * @param event
     *            ignored
     */
    void search(ActionEvent event) {
	JSON result = Search.multiSearch(searchField.getText()).get("results");
	TreeItem<SearchResultItem> searchResults = new TreeItem<>(
		new SearchResultItem("Searching for : " + searchField.getText()));
	searchResultRootNode.getChildren().clear(); // clear previous search statement
	searchResultRootNode.getChildren().add(searchResults);

	for (int i = 0; i < result.size(); i++) {
	    SearchResultItem item = new SearchResultItem(result.get(i), "multiSearch");
	    searchResults.getChildren().add(new TreeItem<SearchResultItem>(item));
	}

	searchResultRootNode.setExpanded(true);
	searchResults.setExpanded(true);
    }

    /**
     * class for holding search result data
     * 
     * @author HKNormann
     *
     */
    class SearchResultItem {
	private String media_type = "";
	private String name = "";
	private long id;
	private String profile_path = "";
	private String title = "";

	/**
	 * Create new SearchResultItem with the given name as what will be displayed in
	 * the tree view.
	 * 
	 * @param name
	 *            the value that will be displayed in the tree view
	 */
	public SearchResultItem(String name) {
	    this.name = name;
	}

	/**
	 * Create a new SearchResultItem with data form this JSON object. seeing as json
	 * data can be structured differently based on type of search, the search type
	 * also has to be taken into account
	 * 
	 * @param json
	 *            contains the data that will be used to initialize this object.
	 * @param searchType
	 *            type of search that the json data is retrieved from
	 */
	public SearchResultItem(JSON json, String searchType) {

	    if (searchType.equals("multiSearch")) { // initialize with data from multi-search
		media_type = (String) json.getValue("media_type");
		if (media_type.equals("person")) { // if it is actor data
		    name = (String) json.getValue("name");
		    profile_path = (String) json.getValue("profile_path");
		} else if (media_type.equals("movie")) {// if it is movie data
		    title = (String) json.getValue("title");
		} else {
		    name = (String) json.getValue("name");
		}
		id = (Long) json.getValue("id");

	    } else if (searchType.equals("takesPartIn")) { // initialize with data from "takes part in"-search
		media_type = "movie";
		title = (String) json.getValue("title");
		id = (Long) json.getValue("id");

	    } else if (searchType.equals("actors")) { // initialize with data from actors-search
		media_type = "person";
		name = (String) json.getValue("name");
		profile_path = (String) json.getValue("profile_path");
		id = (Long) json.getValue("id");
	    }
	}

	/**
	 * @return String - media type
	 */
	public String getMediaType() {
	    return media_type;
	}

	/**
	 * @return long - ID
	 */
	public long getID() {
	    return id;
	}

	/**
	 * Used by the tree view to get the value to display to the user.
	 */
	@Override
	public String toString() {
	    if (media_type.equals("person")) {
		return name;
	    } else if (media_type.equals("movie")) {
		return title;
	    } else {
		return name;
	    }
	}
    }
}
