package no.ntnu.imt3281.movieExplorer;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import no.ntnu.imt3281.dataHandling.Genres;
import no.ntnu.imt3281.dataHandling.JSON;
import no.ntnu.imt3281.dataHandling.Search;
import no.ntnu.imt3281.dataHandling.TheMovieDBConfiguration;

/**
 * @author HKNormann
 *controller class for Detail for displaying movie data and image
 */
public class Details {
    
    private static TheMovieDBConfiguration configuration = null;

    @FXML // fx:id="movieLabel"
    private Label movieLabel; // Value injected by FXMLLoader

    @FXML // fx:id="movieImage"
    private ImageView movieImage; // Value injected by FXMLLoader

    @FXML // fx:id="text"
    private TextArea text; // Value injected by FXMLLoader

    @FXML // fx:id="genreLabel"
    private Label genreLabel; // Value injected by FXMLLoader


    /**
     * Gets data about movie to display in display port
     * 
     * @param movieID
     *            ID of movie to display
     */
    public void displayMovie(int movieID) {
	JSON movieData = Search.movie(movieID);

	movieLabel.setText((String) movieData.getValue("title"));
	text.setText((String) movieData.getValue("overview"));

	String genresText = "";
	JSON genres = movieData.get("genres");

	for (int i = 0; i < genres.size(); i++) { // creates genre String
	    genresText += Genres.resolve((long) genres.get(i).getValue("id")) + "\n";
	}
	genreLabel.setText(genresText);

	// gets and sets image to poster image
	setImage(movieData);
    }

    private void setImage(JSON movieData) {
	String posterImageURL = configuration.getPosterURL((String) movieData.getValue("poster_path"));
	Image posterImage = configuration.getImage(posterImageURL, "poster");
	movieImage.setImage(posterImage);
    }

    /**
     * show file explorer, allowing user to change  dir of images folder
     */
    public void showPreferences() {
	configuration.setImageDirectory();
    }

    /**
     * sets TheMovieDBConfiguration config object
     * @param config
     */
    public void setConfig(TheMovieDBConfiguration config) {
	Details.configuration = config;
    }
}
