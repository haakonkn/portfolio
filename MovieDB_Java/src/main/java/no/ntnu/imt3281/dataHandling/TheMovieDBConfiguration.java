package no.ntnu.imt3281.dataHandling;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Arrays;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import no.ntnu.imt3281.dataHandling.JSON;

/**
 * @author HKNormann class that handles assembling image URL based on image data
 */
public class TheMovieDBConfiguration {

    Preferences prefs = Preferences.userRoot().node(this.getClass().getName());
    private static final String IMG_DIR = "image_dir";

    private JSON imageObject = null;

    /**
     * Reads string of json data and creates internal json object with image-data
     * 
     * @param jsonString
     *            string of json data
     */
    public TheMovieDBConfiguration(String jsonString) {
	imageObject = new JSON(jsonString).get("images");
    }

    /**
     * constructor that takes JSON object directly
     * 
     * @param json
     *            JSON object containing image-data
     */
    public TheMovieDBConfiguration(JSON json) {
	this.imageObject = json.get("images");
    }

    /**
     * assembles imageURL based on image name and type of image
     * 
     * @param imageName
     *            name of image
     * @param sizeType
     *            type of image size
     * @return String - URL of largest image of that type
     */
    private String assembleImageURL(String imageName, String sizeType) {
	String baseURL = (String) imageObject.getValue("base_url");
	JSON subObject = imageObject.get(sizeType);
	String largestSize = (String) subObject.getValue(subObject.size() - 2);
	String returnURL = baseURL + largestSize + "/" + imageName;
	return returnURL;
    }

    /**
     * @param imageName
     *            name of image to look for
     * @return String - URL of largest backdrop image with that name
     */
    public String getBackdropURL(String imageName) {
	return assembleImageURL(imageName, "backdrop_sizes");
    }

    /**
     * @param imageName
     *            name of image to look for
     * @return String - URL of largest logo image with that name
     */
    public String getLogoURL(String imageName) {
	return assembleImageURL(imageName, "logo_sizes");
    }

    /**
     * @param imageName
     *            name of image to look for
     * @return String - URL of largest poster image with that name
     */
    public String getPosterURL(String imageName) {
	return assembleImageURL(imageName, "poster_sizes");
    }

    /**
     * @param imageName
     *            name of image to look for
     * @return String - URL of largest profile image with that name
     */
    public String getProfileURL(String imageName) {
	return assembleImageURL(imageName, "profile_sizes");
    }

    /**
     * @param imageName
     *            name of image to look for
     * @return String - URL of largest still image with that name
     */
    public String getStillURL(String imageName) {
	return assembleImageURL(imageName, "still_sizes");
    }

    /**
     * return image file from image URL. If a local version exists in the selected
     * images folder, that will be returned instead loading it from WWW
     * 
     * @param imageURL
     * @param type type of image to get
     * @return Image - imagefile form URL
     */
    public Image getImage(String imageURL, String type) {

	// try to get image dir from prefs
	String imageDirectory = null;
	imageDirectory = prefs.get(IMG_DIR, null);

	// initialize new dir if it can't be found
	if (imageDirectory == null) {
	    setImageDirectory();
	}
	// get absolute path to image
	String imagePath = imageDirectory + System.getProperty("file.separator")
		+ imageURL.replace((String) imageObject.getValue("base_url"), "").replace("//",
			System.getProperty("file.separator") + type);

	File imageFile = new File(imagePath);
	if (imageFile.exists()) { // if already exists, load from file
	    return new Image("file:" + imagePath);
	}

	Image newImage = null; // if not, load from URL
	newImage = new Image(imageURL);
	String[] strings = imagePath.split("\\.");
	String imageType = strings[strings.length - 1]; // get image ending jpg, gif etc.

	try { // save to file
	    BufferedImage bImage = SwingFXUtils.fromFXImage(newImage, null);
	    ImageIO.write(bImage, imageType, imageFile);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return newImage;
    }

    /**
     * requests user to choose a folder to put the images folder in will use images
     * folder inside selected folder if it already exists
     */
    public void setImageDirectory() {
	String imageDirectory;
	JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
	    imageDirectory = fileChooser.getSelectedFile().getAbsolutePath() + System.getProperty("file.separator")
		    + "images";

	    prefs.put(IMG_DIR, imageDirectory);
	    new File(imageDirectory).mkdir(); // create main directory
	    String dirStart = imageDirectory + System.getProperty("file.separator");
	    new File(dirStart + "w1280").mkdir(); // create sub-directories
	    new File(dirStart + "w500").mkdir();
	    new File(dirStart + "w780").mkdir();
	    new File(dirStart + "w623").mkdir();
	    new File(dirStart + "w300").mkdir();
	}
    }

    /**
     * Traverses imagefolders and returns combined size of all images of type
     * 
     * @param string
     * @return String - imagesize in kb + " KB"
     */
    public String getImagesSize(String string) {
	String imageDirectory = prefs.get(IMG_DIR, null);
	File imageFolder = new File(imageDirectory);

	// parts of code from
	// https://stackoverflow.com/questions/5125242/java-list-only-subdirectories-from-a-directory-not-files
	// and
	// https://stackoverflow.com/questions/5694385/getting-the-filenames-of-all-files-in-a-folder
	// gotten 13:15 - 08.12.2917

	long size = 0;

	if (imageFolder.exists()) {
	    String[] directories = imageFolder.list(new FilenameFilter() {
		@Override
		public boolean accept(File current, String name) {
		    return new File(current, name).isDirectory();
		}
	    });

	    //go through subdirs
	    for (String directory : directories) {
		String directoryPath = imageDirectory + System.getProperty("file.separator") + directory;

		File subfolder = new File(directoryPath);
		File[] listOfFiles = subfolder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) { //add size of files
		    if (listOfFiles[i].isFile() && listOfFiles[i].getPath().contains(string))
			size += listOfFiles[i].length();
		}
	    }
	}
	//return size of images as KB in string
	return (size / 1000) + " KB";
    }

    /**
     * removes all images from cache
     * A lot of this and the previous function is duplicated.
     */
    public void emptyCache() {
	String imageDirectory = prefs.get(IMG_DIR, null);
	File imageFolder = new File(imageDirectory);

	String[] directories = imageFolder.list(new FilenameFilter() {
	    @Override
	    public boolean accept(File current, String name) {
		return new File(current, name).isDirectory();
	    }
	});

		//traverses all sub directories
	for (String directory : directories) {
	    String directoryPath = imageDirectory + System.getProperty("file.separator") + directory;

	    File subfolder = new File(directoryPath);
	    File[] listOfFiles = subfolder.listFiles();

	    for (int i = 0; i < listOfFiles.length; i++) {
		if (listOfFiles[i].isFile()) {
		    listOfFiles[i].delete();	//deletes files
		}
	    }
	}
    }
}
