package no.ntnu.imt3281.dataHandling;

import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * @author HKNormann
 * Class for getting genre name based on ID
 */
public class Genres {

    static DBHandler dbhandler;

    private static void initializeDB() {
	dbhandler = new DBHandler();
    }

    /**
     * Tries to find genre in DB, gets from WWW if it can't be found in DB
     * 
     * @param genreID
     *            ID of genre to find
     * @return String - name of genre with ID = genreID
     */
    public static String resolve(long genreID) {

	if (dbhandler == null)
	    initializeDB();

	String genreName = DBHandler.getGenre(genreID);

	if (genreName == null) {
	    JSONArray genres = Search.getGenres();
	    genreName = findGenre(genreID, genres);
	    dbhandler.insertGenre(genreID, genreName);
	}
	return genreName;
    }

    /**
     * @param genreID
     *            ID of genre to find
     * @param genreArray
     *            JSON array containing genres
     * @return String - name of genre with genreID
     */
    private static String findGenre(long genreID, JSONArray genreArray) {

	for (Object object : genreArray) {
	    JSONObject jsonObject = (JSONObject) object;
	    Set<String> keys = jsonObject.keySet();

	    for (String key : keys) {
		if (key.equals("id") && (Long) jsonObject.get(key) == genreID) {
		    return (String) jsonObject.get("name");
		}
	    }
	}
	return null;
    }

}
