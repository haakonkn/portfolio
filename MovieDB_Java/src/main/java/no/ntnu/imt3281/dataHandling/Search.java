package no.ntnu.imt3281.dataHandling;

import org.json.simple.JSONArray;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * @author HKNormann class that that searches in themoviedb.org and returns JSON
 *         objects containing result data
 */
public class Search {

    private static DBHandler dbhandler = new DBHandler();

    private final static String MultiSearchQuery = "https://api.themoviedb.org/3/search/multi?api_key=51184cf99070b4a72e36261809bd1e62&language=en-US&query=insertQuery&page=1&include_adult=false";
    private final static String ActorSearchQuery = "https://api.themoviedb.org/3/movie/movieID/credits?api_key=51184cf99070b4a72e36261809bd1e62";
    private final static String TakesPartInQuery = "https://api.themoviedb.org/3/discover/movie?api_key=51184cf99070b4a72e36261809bd1e62&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_people=personID";
    private final static String ListMvGenresQuery = "https://api.themoviedb.org/3/genre/movie/list?api_key=51184cf99070b4a72e36261809bd1e62&language=en-US";
    private final static String ListTVGenresQuery = "https://api.themoviedb.org/3/genre/tv/list?api_key=51184cf99070b4a72e36261809bd1e62&language=en-US";
    private final static String MovieInfoQuery = "https://api.themoviedb.org/3/movie/movieID?api_key=51184cf99070b4a72e36261809bd1e62&language=en-US";
    private final static String ConfigurationQuery = "https://api.themoviedb.org/3/configuration?api_key=51184cf99070b4a72e36261809bd1e62";

    /**
     * @param searchString
     *            string to do multisearch for
     * @return JSON object containing search results
     */
    public static JSON multiSearch(String searchString) {
	String legalString = searchString.replaceAll(" ", "%20");
	String queryString = MultiSearchQuery.replace("insertQuery", legalString);
	return searchMovieDB(queryString);
    }

    /**
     * @param movieID
     * @return JSON object containing actors in movie with movieID
     */
    public static JSON actors(int movieID) {

	String queryString = ActorSearchQuery.replace("movieID", Integer.toString(movieID));
	return searchMovieDB(queryString);
    }

    /**
     * @param movieID
     *            ID of movie to return data of
     * @return JSON object containing movie data
     */
    public static JSON movie(int movieID) {

	String queryString = MovieInfoQuery.replace("movieID", Integer.toString(movieID));
	return searchMovieDB(queryString);
    }

    /**
     * @param ID
     *            ID of actor whose movies to look for
     * @return JSON object containing movies with actor ID
     */
    public static JSON takesPartIn(long ID) {
	String queryString = TakesPartInQuery.replace("personID", Long.toString(ID));
	return searchMovieDB(queryString);
    }

    /**
     * @return JSONArray object containing all genres
     */
    @SuppressWarnings("unchecked")
    public static JSONArray getGenres() {
	JSON result;
	JSONArray genres = new JSONArray();
	result = searchMovieDB(ListMvGenresQuery); // get movie genres
	genres.addAll((JSONArray) result.getValue("genres")); // add to list
	result = searchMovieDB(ListTVGenresQuery); // get TV genres
	genres.addAll((JSONArray) result.getValue("genres")); // add to list

	return genres; // return list of genres
    }

    /**
     * @return JSON object containing config info
     */
    public static JSON getConfig() {
	return searchMovieDB(ConfigurationQuery);
    }

    /**
     * 
     * @param queryString
     *            URL string to query DB with
     * @return new JSON object containing response from DB
     */
    private static JSON searchMovieDB(String queryString) {

	JSON returnJSON = null;

	returnJSON = dbhandler.getSearch(queryString);

	if (returnJSON == null) {
	    String result = "";
	    try {
		result = Unirest.get(queryString).asString().getBody();
		returnJSON = new JSON(result);
	    } catch (UnirestException e) {
		e.printStackTrace();
	    }
	    dbhandler.addSearch(queryString, returnJSON);
	}
	return returnJSON;
    }
}
