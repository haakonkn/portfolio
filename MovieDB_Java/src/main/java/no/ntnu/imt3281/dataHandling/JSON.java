package no.ntnu.imt3281.dataHandling;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSON {

    private Object obj;

    /**
     * @param jsonInput
     *            Constructor taking JSON string and converting it to JSONObject
     */
    public JSON(String jsonInput) {
	try {
	    JSONParser parser = new JSONParser();
	    obj = (JSONObject) parser.parse(jsonInput);
	} catch (ParseException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Constructor creating new JSON with obj = object
     * 
     * @param object
     */
    private JSON(Object object) {
	obj = object;
    }

    /**
     * @return Object obj - is either JSONObject, JSONArray or a simple value
     */
    public Object getObject() {
	return obj;
    }

    @Override
    public String toString() {
	if (obj instanceof JSONArray) { // Key points to a JSON array
	    return ((JSONArray) obj).toJSONString();
	} else if (obj instanceof JSONObject) { // Key points to a JSON object
	    return ((JSONObject) obj).toJSONString();
	} else { // Key points to a simple value
	    return "";
	}
    }

    /**
     * @param value
     *            either a string key or index number
     * @return Object at index number of with String key = value
     */
    public Object getValue(Object value) {
	if (obj instanceof JSONArray) { // Key points to a JSON array
	    JSONArray array = (JSONArray) obj;
	    return array.get((Integer) value);
	} else if (obj instanceof JSONObject) { // Key points to a JSON object
	    if (value instanceof Long)
		return ((JSONObject) obj).get(Long.toString((long) value));
	    else if (value instanceof Integer)
		return ((JSONObject) obj).get(Integer.toString((int) value));
	    else
		return ((JSONObject) obj).get((String) value);
	} else { // Key points to a simple value
	    return obj;
	}
    }

    /**
     * @param index
     *            either a string key or index number
     * @return JSON object containing branch with number or key = index
     */
    public JSON get(Object index) {
	return new JSON(getValue(index));
    }

    /**
     * @return size of JSONArray or JSONObject return 1 if only contains simple
     *         value
     */
    public int size() {

	if (obj instanceof JSONArray) { // Key points to a JSON array
	    JSONArray array = (JSONArray) obj;
	    return array.size();
	} else if (obj instanceof JSONObject) { // Key points to a JSON object
	    JSONObject object = (JSONObject) obj;
	    return object.size();
	} else { // Key points to a simple value
	    return 1;
	}
    }
}
