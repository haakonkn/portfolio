package no.ntnu.imt3281.dataHandling;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.simple.JSONObject;

//based on code form the 2016 exam - starwars

/**
 * @author HKNormann Class setting up the database
 */
public class DBHandler {
    private static final String URL = "jdbc:derby:movieExplorerDB";

    /**
     * connection to database
     */
    public static Connection con;

    DBHandler() {
	try {
	    con = DriverManager.getConnection(URL);
	    System.out.println("Connected to existing database");
	} catch (SQLException sqle) { // No database exists
	    try { // Try creating database
		con = DriverManager.getConnection(URL + ";create=true");
		setupDB();
		System.out.println("New database created");
	    } catch (SQLException sqle1) { // Unable to create database, exit server
		System.err.println("Unable to create database" + sqle1.getMessage());
		System.exit(-1);
	    }
	}
    }

    /**
     * sets up contents of database
     * 
     * @throws SQLException
     */
    private static void setupDB() throws SQLException {
	Statement stmt = con.createStatement();
	stmt.execute(
		"CREATE TABLE genre (id char(64) NOT NULL, " + "name varchar(128) NOT NULL, " + "PRIMARY KEY  (id))");

	stmt.execute("CREATE TABLE search (query varchar(256) NOT NULL, " + "json clob, " + "PRIMARY KEY  (query))");
    }

    /**
     * gets genre name from database
     * 
     * @param genreID
     * @return String - name of genre
     */
    public static String getGenre(long genreID) {
	String genreQuery = "SELECT name FROM genre WHERE genre.id = '" + genreID + "'";

	try {
	    Statement statement = con.createStatement();
	    ResultSet res = statement.executeQuery(genreQuery);
	    if (res.next())
		return res.getString("name");

	} catch (SQLException | NullPointerException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * inserts genre into database
     * 
     * @param genreID
     * @param genreName
     */
    public void insertGenre(long genreID, String genreName) {

	final String insertGenreQuery = "INSERT INTO genre VALUES ('" + genreID + "','" + genreName + "')";
	try (Statement stmnt = con.createStatement()) {
	    stmnt.executeUpdate(insertGenreQuery);
	} catch (SQLException e) {
	    e.printStackTrace();
	}
    }

    /**
     * will look for query in database, if it has been previously done, will return
     * stored JSON object instead of requesting new from server
     * 
     * @param queryString
     * @return JSON - json data from previous identical search
     */
    public JSON getSearch(String queryString) {
	String formattedQuery = queryString.replace("\'", "''");
	String searchQuery = "SELECT json FROM search WHERE query = '" + formattedQuery + "'";
	String json = "";
	try {
	    Statement statement = con.createStatement();
	    ResultSet res = statement.executeQuery(searchQuery);
	    if (res.next()) {
		json = res.getString("json");
		return new JSON(json);
	    }
	    System.out.println("json string from db: " + json);
	} catch (SQLException | NullPointerException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * adds a query with returned json data to the database
     * 
     * @param queryString
     * @param returnJSON
     */
    public void addSearch(String queryString, JSON returnJSON) {
	String json = ((JSONObject) returnJSON.getObject()).toJSONString().replace("\'", "''");
	String formattedQuery = queryString.replace("\'", "''"); // checkout illegal characters
	final String insertGenreQuery = "INSERT INTO search VALUES ('" + formattedQuery + "','" + json + "')";
	try (Statement stmnt = con.createStatement()) {
	    stmnt.executeUpdate(insertGenreQuery);
	} catch (SQLException e) {
	    // json part of query is probably too long
	    e.printStackTrace();
	}
    }

    /**
     * gets number of elements in DB
     * 
     * @return String - nSøk søk, nSjangre sjangre
     */
    public static String getDBSize() {

	final String countSearchQuery = "SELECT COUNT(*) FROM search";
	final String countGenreQuery = "SELECT COUNT(*) FROM genre";
	ResultSet SearchRes = null;
	ResultSet GenreRes = null;
	int genreCount = 0;
	int searchCount = 0;

	try { // execute queries
	      // get number of searches in DB
	    Statement statement = con.createStatement();
	    SearchRes = statement.executeQuery(countSearchQuery);
	    SearchRes.next();
	    searchCount = SearchRes.getInt(1);
	    SearchRes.close();

	    // get number of genres in DB
	    GenreRes = statement.executeQuery(countGenreQuery);
	    GenreRes.next();
	    genreCount = GenreRes.getInt(1);
	    GenreRes.close();
	} catch (SQLException | NullPointerException e) {
	    e.printStackTrace();
	} finally {
	    try {
		SearchRes.close();
		GenreRes.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }
	}

	// return string describing size of DB
	return searchCount + " søk, " + genreCount + " sjangre";
    }

    /**
     * removes all data from DB
     */
    public static void wipeDB() {

	// requests database to delete all from table
	final String deleteSearchQuery = "DELETE FROM search WHERE 1=1";
	final String deleteGenreQuery = "DELETE FROM genre WHERE 1=1";
	try (Statement stmnt = con.createStatement()) {
	    stmnt.executeUpdate(deleteSearchQuery);
	    stmnt.executeUpdate(deleteGenreQuery);
	} catch (SQLException e) {
	    e.printStackTrace();
	}
    }
}
