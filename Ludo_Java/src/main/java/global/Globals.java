/**
 * 
 */
package global;

/**
 * @author okaad_000
 *
 */
public class Globals {

    private Globals() {

    }

    /**
     * Checks if text is within legal length and contains only numbers and
     * letters
     * 
     * @param text
     *            - text to check if valid
     * @param minLength
     *            - minimum length of text
     * @param maxLength
     *            - maximum length of text
     * @return true if text matches regular expression
     */
    public static boolean isValid(String text, int minLength, int maxLength) {
        return text.matches(String.format("[A-Za-z0-9]{%d,%d}$", minLength, maxLength));
    }
}
