package no.ntnu.imt3281.ludo.server;

import java.util.Vector;

import no.ntnu.imt3281.ludo.server.Server.ClientStruct;

/**
 * Chat object holding all clients in the chat and handling broadcasting
 * 
 * @author okaad_000
 * @author HKNormann
 */
public class Chat {
    /**
     * unique name of chat
     */
    private String chatName;
    /**
     * clients in the chat
     */
    private Vector<ClientStruct> clients = new Vector<>();

    /**
     * Constructor giving the chat its name
     * 
     * @param name
     */
    Chat(String name) {
        this.chatName = name;
    }

    /**
     * Broadcasts message to all clients in this chat
     * 
     * @param clientName
     * @param message
     */
    public void broadcastMessage(String clientName, String message) {

        String sendMessage = "Chat:" + "Text:" + chatName + ":" + clientName + ":" + message;

        for (ClientStruct client : clients) {
            client.socketOut.println(sendMessage);
        }
    }

    /**
     * Remove a client from the chat and broadcast that he disconnected should
     * be internationalized
     * 
     * @param client
     *            client to disconnect
     * @return true if there are no more people in this chat
     */
    public boolean removeClient(ClientStruct client) {
        boolean emptyChat = false;

        if (clients.contains(client)) {
            broadcastMessage("*SERVER*", client.username + " disconnected");
            clients.remove(client);

            if (clients.isEmpty()) {
                emptyChat = true;
            }
        }
        return emptyChat;
    }

    /**
     * @return the chatName
     */
    public String getChatName() {
        return chatName;
    }

    /**
     * @param chatName
     *            - the chatName to set
     */
    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    /**
     * @return the clients
     */
    public Vector<ClientStruct> getClients() {
        return clients;
    }

    /**
     * @param clients
     *            - the clients to set
     */
    public void setClients(Vector<ClientStruct> clients) {
        this.clients = clients;
    }
}
