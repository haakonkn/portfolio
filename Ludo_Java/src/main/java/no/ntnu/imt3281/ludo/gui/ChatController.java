package no.ntnu.imt3281.ludo.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * Class controlling a chat window and all client side chat functionalities
 * 
 * @author okaad_000
 * @author HKNormann
 */
public class ChatController {

    /**
     * reference to client
     */
    private Client owner = null;

    @FXML // fx:id="chatInput"
    private TextField chatInput; // Value injected by FXMLLoader

    @FXML
    private TextArea textField;

    @FXML
    private TitledPane titledPane;

    /**
     * button fuction requesting owner to send message of chatInput
     * 
     * @param event
     */
    @FXML
    void chatSend(ActionEvent event) {
        owner.sendChatMessage(getName(), chatInput.getText());
        chatInput.clear();
    }

    /**
     * sets owner to client
     * 
     * @param owner
     */
    void setOwner(Client owner) {
        this.owner = owner;
    }

    /**
     * stores name of chat in the titled pane
     * 
     * @param chatName
     *            new name
     */
    void setName(String chatName) {
        titledPane.setText(chatName);
    }

    /**
     * gets name of chat from titled pane
     * 
     * @return name of chat
     */
    public String getName() {
        return titledPane.getText();
    }

    /**
     * appends a message to a new line of the chat text field
     * 
     * @param clientName
     *            messaging client
     * @param message
     *            message text
     */
    public void addMessage(String clientName, String message) {
        String text = clientName + ": " + message + "\n";
        textField.appendText(text);
    }

    /**
     * sets the preferred size of the chat window so that it can be adjusted to
     * global or windowed chat
     * 
     * @param width
     *            new preferred width
     * @param height
     *            new preferred height
     */
    public void setTextPrefSize(int width, int height) {
        textField.setPrefSize(width, height);
    }
}
