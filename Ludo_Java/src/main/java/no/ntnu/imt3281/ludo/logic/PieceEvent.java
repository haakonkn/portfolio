/**
 * 
 */
package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.logging.Logger;

/**
 * Contains information about each piece.
 * 
 * @author okaad_000
 *
 */
public class PieceEvent extends EventObject implements PieceListener {
    private int player;
    private int piece;
    private int from;
    private int to;
    private static final Logger LOGGER = Logger.getLogger(PieceEvent.class.getName());

    /**
     * A constructor.
     * 
     * @param ludo
     *            - the gameboard to construct this event with
     */
    public PieceEvent(Ludo ludo) {
        super(ludo);
    }

    /**
     * A constructor.
     * 
     * @param ludo
     *            - the gameboard to construct this event with
     * @param player
     *            - the player index
     * @param piece
     *            - the piece index
     * @param from
     *            - tile moved from
     * @param to
     *            - tile moved to
     */
    public PieceEvent(Ludo ludo, int player, int piece, int from, int to) {
        super(ludo);

        this.player = player;
        this.piece = piece;
        this.from = from;
        this.to = to;
    }

    @Override
    public void pieceMoved(PieceEvent event) {
        player = event.player;
        piece = event.piece;
        from = event.from;
        to = event.to;
    }

    @Override
    public int hashCode() {
        return (player * 17 + from) % 12332;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other == null || this.getClass() != other.getClass())) {
            try {
                PieceEvent event = (PieceEvent) other;
                return (player == event.player && piece == event.piece && from == event.from && to == event.to);
            } catch (RuntimeException e) {
                // Is different object
                LOGGER.warning(e.getMessage());
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return player + ", " + piece + ", " + from + ", " + to;
    }
}
