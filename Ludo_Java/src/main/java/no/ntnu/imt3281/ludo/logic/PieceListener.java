/**
 * 
 */
package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * Listener to piece events.
 * 
 * @author okaad_000
 *
 */
public interface PieceListener extends EventListener {
    /**
     * Abstract function to implement.
     * 
     * @param event
     *            - The relevant event
     */
    void pieceMoved(PieceEvent event);
}
