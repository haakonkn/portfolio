/**
 * 
 */
package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.logging.Logger;

/**
 * Contains information about each dice roll.
 * 
 * @author okaad_000
 * @author HKNormann
 *
 */
public class DiceEvent extends EventObject implements DiceListener {
    private int player;
    private int dice;
    private static final Logger LOGGER = Logger.getLogger(DiceEvent.class.getName());

    /**
     * A constructor.
     * 
     * @param ludo
     *            - the gameboard to construct this event with
     */
    public DiceEvent(Ludo ludo) {
        super(ludo);
    }

    /**
     * A constructor.
     * 
     * @param ludo
     *            - the gameboard to construct this event with
     * @param player
     *            - the player index
     * @param dice
     *            - the dice value
     */
    public DiceEvent(Ludo ludo, int player, int dice) {
        super(ludo);

        this.player = player;
        this.dice = dice;
    }

    @Override
    public void diceThrown(DiceEvent diceEvent) {
        player = diceEvent.player;
        dice = diceEvent.dice;
    }

    @Override
    public int hashCode() {
        return (player * 17 + dice) % 9001;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other == null || this.getClass() != other.getClass())) {
            try {
                DiceEvent event = (DiceEvent) other;
                return (player == event.player && dice == event.dice);
            } catch (RuntimeException e) {
                LOGGER.warning(e.getMessage());
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return Integer.toString(dice);
    }
}
