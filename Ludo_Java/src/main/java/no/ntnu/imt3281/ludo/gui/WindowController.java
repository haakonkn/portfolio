package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * Controls the GUI and flow of the game.
 * 
 * @author okaad_000
 *
 */
public class WindowController {
    private Client owner;
    private static final Logger LOGGER = Logger.getLogger(WindowController.class.getName());
    FileHandler fh;

    /**
     * Controller handling the login window and functions
     */
    private LoginController loginController;

    /**
     * Controller handling the chat window and functions
     */
    @FXML
    public Vector<ChatController> chatControllers;

    /**
     * Controller handling the game board window and functions
     */
    @FXML
    public Vector<GameBoardController> gameBoardControllers;

    /**
     * Controller handling the main menu window and functions
     */
    @FXML
    public MainMenuController mainMenuController;

    @FXML
    private MenuItem random;

    /**
     * tabbedpane containing main menu, chats and games
     */
    @FXML
    private TabPane tabbedPane;

    @FXML
    private Tab mainMenuTab;

    @FXML
    void initialize() {
        chatControllers = new Vector<>();
        gameBoardControllers = new Vector<>();

        try { // initialize logger file
            fh = new FileHandler("ClientLog.log");
            LOGGER.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            LOGGER.info("Start of Client log");
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }
    /**
     * creates the main menu window and adds it to the tabbed pane
     */
    public void createMainMenu() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainMenu.fxml"));
        loader.setResources(Client.getMessages());
        try {
            Pane mainMenu = loader.load();
            mainMenuController = loader.getController();
            Tab tab = new Tab("Main menu");
            tab.setContent(mainMenu);
            tab.setClosable(false);
            tabbedPane.getTabs().add(tab);
            mainMenuController.setOwner(owner);
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * removes all tabs from window
     */
    public void clearMenu() {
        tabbedPane.getTabs().clear();
    }

    /**
     * Sets the LudoController's owning client.
     * 
     * @param owner
     *            - the client that owns this controller
     */
    public void setOwner(Client owner) {
        this.owner = owner;
    }

    /**
     * Makes the player join a random game.
     * 
     * @param event
     *            - action performed
     */
    @FXML
    public void joinRandomGame(ActionEvent event) {
        owner.sendMessage("Game:Join:Random");
    }

    /**
     * creates a game
     * 
     * @param gameID
     *            the ID of the new game
     * @param playerNames
     *            name of players to join the game
     */
    public void createGame(String gameID, String playerNames) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
        loader.setResources(Client.getMessages());

        String[] strings = playerNames.split(":");
        Vector<String> players = new Vector<>(Arrays.asList(strings));

        try {
            AnchorPane gameBoard = loader.load();
            GameBoardController controller = loader.getController();
            gameBoardControllers.add(controller);
            Tab tab = new Tab(gameID);
            tab.setContent(gameBoard);
            controller.initializeGame(gameID, players, owner);
            tabbedPane.getTabs().add(tab);
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * Opens a window for login register
     * 
     * @param event - action performed
     */
    @FXML
    public void openLoginRegisterGUI(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Login.fxml"));
        loader.setResources(Client.getMessages());

        try {
            BorderPane root = loader.load();
            Scene scene = new Scene(root);
            Stage loginStage = new Stage();

            setLoginController(loader.getController());
            loginStage.setScene(scene);
            loginStage.show();
            getLoginController().setOwner(owner);

        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * sends request to server to log out
     * 
     * @param event - action performed
     */
    @FXML
    public void logOut(ActionEvent event) {
        owner.handleLogout("true:pls");
    }

    /**
     * requests server to get list of chats
     * 
     * @param event
     */
    @FXML
    void refreshChats(ActionEvent event) {
        owner.sendMessage("List:Chat");
    }

    /**
     * requests server to get list of online players
     * 
     * @param event
     */
    @FXML
    void refreshPlayers(ActionEvent event) {
        owner.sendMessage("List:Players");
    }

    /**
     * sets title of window, this is used to indicate who is logged in
     * 
     * @param title
     *            new title of window
     */
    public void setTitle(String title) {
        Stage stage = (Stage) tabbedPane.getScene().getWindow();
        stage.setTitle(title);
    }

    @FXML
    void exit(ActionEvent event) {
        Runtime.getRuntime().exit(0);
    }

    @FXML
    void about(ActionEvent event) {
        owner.createDialog("", Client.getMessages().getString("popup.notYetImplemented"));
    }

    /**
     * adds a chat tab to the tabbed pane
     * 
     * @param chatName
     *            name of new chat
     */
    public void addChat(String chatName) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"));
        loader.setResources(Client.getMessages());

        TitledPane chat;
        try {
            chat = loader.load();
            chat.setText(chatName);
            ChatController chatController = loader.getController();
            chatController.setOwner(owner);
            chatControllers.add(chatController);

            Tab tab = new Tab("Chat: " + chatName);
            tab.setContent(chat);
            tabbedPane.getTabs().add(tab);
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * Gets chat with name, returns null if it can't be found
     * 
     * @param chatName
     *            - name of the chat
     * @return ChatController - relevant chat controller object
     */
    public ChatController getChat(String chatName) {
        ChatController returnChatController = null;

        for (ChatController chat : chatControllers) {
            if (chat.getName().equals(chatName)) {
                returnChatController = chat;
            }
        }
        return returnChatController;
    }

    /**
     * Returns GameBoardController object with gameName
     * 
     * @param gameName
     *            - name of the game
     * @return GameBoardController - relevant game board controller object
     */
    public GameBoardController getGame(String gameName) {
        GameBoardController returnGameBoardController = null;

        for (GameBoardController game : gameBoardControllers) {
            if (game.getGameID().equals(gameName)) {
                returnGameBoardController = game;
            }
        }
        return returnGameBoardController;
    }

    /**
     * Adds message to chatwindow
     * 
     * @param clientName
     *            name of clients sending the message
     * @param chatName
     *            name of chat
     * @param message
     *            message being sent
     */
    public void addMessage(String clientName, String chatName, String message) {

        if ("global".equals(chatName)) {
            mainMenuController.addGlobalMessage(clientName, message);
        }

        ChatController chat = null;
        if ((chat = getChat(chatName)) != null) {
            chat.addMessage(clientName, message);
        }
    }

    /**
     * Finds correct game and passes update to correct function based on type
     * 
     * @param type
     *            type of update/message
     * @param string
     *            what the message contains
     */
    public void passMessageToGame(String type, String string) {
        String[] strings = string.split(":", 2);
        String gameID = strings[0];
        String data = strings[1];
        GameBoardController gameBoard = null;
        gameBoard = getGame(gameID);

        for (GameBoardController game : gameBoardControllers) {
            if (game.getGameID().equals(gameID)) {
                switch (type) {
                    case "Request" :
                        gameBoard.handleRequest(data);
                        break;
                    case "Update" :
                        gameBoard.handleUpdate(data);
                        break;
                    case "Disconnect" :
                        gameBoard.handleDisconnect(data);
                        break;
                    case "Winner" :
                        gameBoard.handleWinner(data);
                        break;
                    default :
                        LOGGER.warning("windowController-passMessageToGame Invalid type");
                        break;
                }
            }
        }
    }
    public LoginController getLoginController() {
        return loginController;
    }
    public void setLoginController(LoginController loginController) {
        this.loginController = loginController;
    }
}
