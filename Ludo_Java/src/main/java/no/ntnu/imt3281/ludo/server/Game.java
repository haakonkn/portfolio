package no.ntnu.imt3281.ludo.server;

import java.util.Arrays;
import java.util.Vector;
import java.util.logging.Logger;

import no.ntnu.imt3281.ludo.logic.Ludo;
import no.ntnu.imt3281.ludo.server.Server.ClientStruct;

/**
 * Class containing all information about a single game instance
 * 
 * @author okaad_000
 * @author HKNormann
 */
public class Game {
    private final static Logger LOGGER = Logger.getLogger(Server.class.getName());
    private Server server;
    /**
     * name of game
     */
    private String gameName;
    /**
     * vector containing clientStructs for clients in game
     */
    private Vector<ClientStruct> clients;
    /**
     * object containing and handling all the logic of the game instance
     */
    private Ludo ludoLogic;
    int numFinished = 0;

    Game(String name, Server server) {
        this.server = server;
        this.gameName = name;
        this.clients = new Vector<>();
    }

    /**
     * Sets a reference to the server
     * 
     * @param server
     */
    public void setOwner(Server server) {
        this.server = server;
    }

    /**
     * Starts the game and notifies players
     */
    public void startGame() {
        String createMessage = "Game:" + gameName + ":Created";
        String[] playerNames = new String[4];
        Arrays.fill(playerNames, null);

        int iterator = 0;
        for (ClientStruct client : clients) {
            createMessage = createMessage.concat(":" + client.username);
            playerNames[iterator++] = client.username;
        }
        for (ClientStruct client : clients) {
            client.socketOut.println(createMessage);
        }
        setLudoLogic(new Ludo(playerNames[0], playerNames[1], playerNames[2], playerNames[3]));
        getLudoLogic().setOwner(this);
        broadcastMessage("Request:Roll", "0"); // requests first player to roll
    }

    /**
     * Broadcasts message to all clients in this game
     * 
     * @param type
     *            expected values Update, Request or Info
     * @param data
     *            data in message
     */
    public void broadcastMessage(String type, String data) {

        String sendMessage = "Game:" + gameName + ":" + type + ":" + data;

        for (ClientStruct client : clients) {
            client.socketOut.println(sendMessage);
        }
    }

    /**
     * Handles game requests from clients
     * 
     * @param string
     *            contains request data
     */
    public void handleRequest(String string) {
        String[] strings = string.split(":");
        String type = strings[0];

        switch (type) {
            case "Throw" :
                getLudoLogic().throwDice();
                break;
            case "Move" :
                int player = Integer.parseInt(strings[1]);
                int piece = Integer.parseInt(strings[2]);
                if (getLudoLogic().canMove(piece)) {
                    int from = getLudoLogic().getPiecePositions()[player][piece];
                    int to = from + getLudoLogic().getDice();
                    getLudoLogic().movePiece(player, piece, from, to);
                }
                break;
            default :
                LOGGER.warning("Game::handleRequest::Package not recognized: " + string);
        }
    }

    /**
     * removes a client from the game and game logic
     * 
     * @param client
     *            client to remove
     * @return true if there are no more people in this game
     */
    public boolean removeClient(ClientStruct client) {
        boolean emptyGame = false;

        if (clients.contains(client)) {
            getLudoLogic().removePlayer(client.username);
            clients.remove(client);
            if (clients.isEmpty()) {
                emptyGame = true;
            }
        }
        return emptyGame;
    }

    /**
     * sets a client to be the winnner if there hasn't already been one and
     * notifies other clients in the game
     * 
     * @param winnerName
     *            name of winning client
     */
    public void won(String winnerName) {
        if (numFinished == 0) { // makes sure only one player can win a game
            broadcastMessage("Winner", winnerName);
            server.getDatabaseHandler().registerWinner(winnerName);
            numFinished++;
        }
    }

    /**
     * calls for the server to remove this game this is called when the game is
     * empty
     */
    public void endGame() {
        server.endGame(gameName);
    }

    /**
     * @return the gameName
     */
    public String getGameName() {
        return gameName;
    }

    /**
     * @param gameName
     *            - the gameName to set
     */
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    /**
     * @return the clients
     */
    public Vector<ClientStruct> getClients() {
        return clients;
    }

    /**
     * @param clients
     *            - the clients to set
     */
    public void setClients(Vector<ClientStruct> clients) {
        this.clients = clients;
    }

    /**
     * @return the ludoLogic
     */
    public Ludo getLudoLogic() {
        return ludoLogic;
    }

    /**
     * @param ludoLogic
     *            - the ludoLogic to set
     */
    public void setLudoLogic(Ludo ludoLogic) {
        this.ludoLogic = ludoLogic;
    }

}
