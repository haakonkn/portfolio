/**
 * 
 */
package no.ntnu.imt3281.ludo.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * @author okaad_000
 *
 */
public class WaitForPacketsThread implements Runnable {

    private Socket socket;
    private static final Logger LOGGER = Logger.getLogger(WaitForPacketsThread.class.getName());

    private Client owner;
    private BufferedReader socketIn;

    /**
     * sets socket and waits for packets
     * 
     * @param socket - socket that holds connection to server
     */
    public WaitForPacketsThread(Socket socket) {
        this.socket = socket;
        try {
            socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    @Override
    public void run() {
        String message;
        while (!socket.isClosed()) {
            try {
                if (socketIn.ready()) {
                    message = socketIn.readLine();
                    owner.handleMessage(message);
                }
            } catch (IOException e) {
                LOGGER.warning(e.getMessage());
            }
        }
    }

    /**
     * Sets the thread's owning client.
     * 
     * @param owner
     *            - the client that owns this controller
     */
    public void setOwner(Client owner) {
        this.owner = owner;
    }
}
