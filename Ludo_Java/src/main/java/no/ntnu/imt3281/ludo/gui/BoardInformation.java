/**,6
 * 
 */
package no.ntnu.imt3281.ludo.gui;

import java.awt.Point;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;

/**
 * class containing board information and images
 * 
 * @author okaad_000
 * @author HKNormann
 *
 */
public class BoardInformation {
    /**
     * Array containing images of player pieces
     */
    public final Image playerPieceImages[];
    /**
     * Array containing image of dice sides
     */
    public final Image diceImages[];
    /**
     * player icon for disconnected players
     */
    public final Image disconnectedImage;

    /**
     * player icon for winnning player
     */
    public final Image winnerImage;
    /**
     * class containing top left corners of board squares
     */
    private final TopLeftCorners corners;

    /**
     * constructor setting all the corner points
     */
    BoardInformation() {
        corners = new TopLeftCorners();
        playerPieceImages = new Image[4];
        diceImages = new Image[7];
        disconnectedImage = new Image(getClass().getResourceAsStream("/images/disconnected.png"));
        winnerImage = new Image(getClass().getResourceAsStream("/images/winner.png"));
        // Starts from 1 so that we can use dice value directly
        diceImages[1] = new Image(getClass().getResourceAsStream("/images/dice1.png"));
        diceImages[2] = new Image(getClass().getResourceAsStream("/images/dice2.png"));
        diceImages[3] = new Image(getClass().getResourceAsStream("/images/dice3.png"));
        diceImages[4] = new Image(getClass().getResourceAsStream("/images/dice4.png"));
        diceImages[5] = new Image(getClass().getResourceAsStream("/images/dice5.png"));
        diceImages[6] = new Image(getClass().getResourceAsStream("/images/dice6.png"));
        playerPieceImages[0] = new Image(getClass().getResourceAsStream("/images/BrickeRed.png"));
        playerPieceImages[1] = new Image(getClass().getResourceAsStream("/images/BrickeBlue.png"));
        playerPieceImages[2] = new Image(getClass().getResourceAsStream("/images/BrickeYellow.png"));
        playerPieceImages[3] = new Image(getClass().getResourceAsStream("/images/BrickeGreen.png"));
    }

    public TopLeftCorners getCorners() {
        return corners;
    }

    /**
     * Class containing all top left corners of board positions
     * 
     * @author okaad_000
     * @author HKNormann
     */
    public static class TopLeftCorners {

        /**
         * the array of top left points
         */
        private final Point points[] = new Point[92];

        /**
         * constructor setting all the values
         */
        public TopLeftCorners() {
            // Red's home
            getPoints()[0] = new Point(554, 74);
            getPoints()[1] = new Point(554 + 48, 74 + 48);
            getPoints()[2] = new Point(554, 74 + 48 * 2);
            getPoints()[3] = new Point(554 - 48, 74 + 48);

            // Blue's home
            getPoints()[4] = new Point(554, 506);
            getPoints()[5] = new Point(554 + 48, 506 + 48);
            getPoints()[6] = new Point(554, 506 + 48 * 2);
            getPoints()[7] = new Point(554 - 48, 506 + 48);

            // Yellow's home
            getPoints()[8] = new Point(122, 506);
            getPoints()[9] = new Point(122 + 48, 506 + 48);
            getPoints()[10] = new Point(122, 506 + 48 * 2);
            getPoints()[11] = new Point(122 - 48, 506 + 48);

            // Green's home
            getPoints()[12] = new Point(122, 74);
            getPoints()[13] = new Point(122 + 48, 74 + 48);
            getPoints()[14] = new Point(122, 74 + 48 * 2);
            getPoints()[15] = new Point(122 - 48, 74 + 48);

            // 16-67
            getPoints()[16] = calculateCorner(8, 1);
            getPoints()[17] = calculateCorner(8, 2);
            getPoints()[18] = calculateCorner(8, 3);
            getPoints()[19] = calculateCorner(8, 4);
            getPoints()[20] = calculateCorner(8, 5);

            getPoints()[21] = calculateCorner(9, 6);
            getPoints()[22] = calculateCorner(10, 6);
            getPoints()[23] = calculateCorner(11, 6);
            getPoints()[24] = calculateCorner(12, 6);
            getPoints()[25] = calculateCorner(13, 6);
            getPoints()[26] = calculateCorner(14, 6);
            getPoints()[27] = calculateCorner(14, 7);
            getPoints()[28] = calculateCorner(14, 8);
            getPoints()[29] = calculateCorner(13, 8);
            getPoints()[30] = calculateCorner(12, 8);
            getPoints()[31] = calculateCorner(11, 8);
            getPoints()[32] = calculateCorner(10, 8);
            getPoints()[33] = calculateCorner(9, 8);

            getPoints()[34] = calculateCorner(8, 9);
            getPoints()[35] = calculateCorner(8, 10);
            getPoints()[36] = calculateCorner(8, 11);
            getPoints()[37] = calculateCorner(8, 12);
            getPoints()[38] = calculateCorner(8, 13);
            getPoints()[39] = calculateCorner(8, 14);
            getPoints()[40] = calculateCorner(7, 14);
            getPoints()[41] = calculateCorner(6, 14);
            getPoints()[42] = calculateCorner(6, 13);
            getPoints()[43] = calculateCorner(6, 12);
            getPoints()[44] = calculateCorner(6, 11);
            getPoints()[45] = calculateCorner(6, 10);
            getPoints()[46] = calculateCorner(6, 9);

            getPoints()[47] = calculateCorner(5, 8);
            getPoints()[48] = calculateCorner(4, 8);
            getPoints()[49] = calculateCorner(3, 8);
            getPoints()[50] = calculateCorner(2, 8);
            getPoints()[51] = calculateCorner(1, 8);
            getPoints()[52] = calculateCorner(0, 8);
            getPoints()[53] = calculateCorner(0, 7);
            getPoints()[54] = calculateCorner(0, 6);
            getPoints()[55] = calculateCorner(1, 6);
            getPoints()[56] = calculateCorner(2, 6);
            getPoints()[57] = calculateCorner(3, 6);
            getPoints()[58] = calculateCorner(4, 6);
            getPoints()[59] = calculateCorner(5, 6);

            getPoints()[60] = calculateCorner(6, 5);
            getPoints()[61] = calculateCorner(6, 4);
            getPoints()[62] = calculateCorner(6, 3);
            getPoints()[63] = calculateCorner(6, 2);
            getPoints()[64] = calculateCorner(6, 1);
            getPoints()[65] = calculateCorner(6, 0);
            getPoints()[66] = calculateCorner(7, 0);
            getPoints()[67] = calculateCorner(8, 0);

            // Red's final stretch
            getPoints()[68] = calculateCorner(7, 1);
            getPoints()[69] = calculateCorner(7, 2);
            getPoints()[70] = calculateCorner(7, 3);
            getPoints()[71] = calculateCorner(7, 4);
            getPoints()[72] = calculateCorner(7, 5);
            getPoints()[73] = calculateCorner(7, 6);

            // Blue's final stretch
            getPoints()[74] = calculateCorner(13, 7);
            getPoints()[75] = calculateCorner(12, 7);
            getPoints()[76] = calculateCorner(11, 7);
            getPoints()[77] = calculateCorner(10, 7);
            getPoints()[78] = calculateCorner(9, 7);
            getPoints()[79] = calculateCorner(8, 7);

            // Yellow's final stretch
            getPoints()[80] = calculateCorner(7, 13);
            getPoints()[81] = calculateCorner(7, 12);
            getPoints()[82] = calculateCorner(7, 11);
            getPoints()[83] = calculateCorner(7, 10);
            getPoints()[84] = calculateCorner(7, 9);
            getPoints()[85] = calculateCorner(7, 8);

            // Green's final stretch
            getPoints()[86] = calculateCorner(1, 7);
            getPoints()[87] = calculateCorner(2, 7);
            getPoints()[88] = calculateCorner(3, 7);
            getPoints()[89] = calculateCorner(4, 7);
            getPoints()[90] = calculateCorner(5, 7);
            getPoints()[91] = calculateCorner(6, 7);
        }

        /**
         * function calculating from Integer row and column x,y coordinates of
         * board tiles to actual board position
         * 
         * @param x
         *            column of tile
         * @param y
         *            row of tile
         * @return the actual boardposition of the tile
         */
        private Point calculateCorner(int x, int y) {
            return new Point(x * 48, y * 48);
        }

        public Point[] getPoints() {
            return points;
        }
    }
}
