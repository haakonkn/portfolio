/**
 * 
 */
package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * Listener for player events.
 * 
 * @author okaad_000
 *
 */
public interface PlayerListener extends EventListener {
    /**
     * Abstract function to implement.
     * 
     * @param event
     *            - The relevant event
     */
    void playerStateChanged(PlayerEvent ple);
}
