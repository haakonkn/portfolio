/**
 * 
 */
package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;
import java.util.logging.Logger;

/**
 * @author okaad_000
 *
 */
public class PlayerEvent extends EventObject implements PlayerListener {
    /**
     * A constant value for PLAYING.
     */
    public static final int PLAYING = 0;
    /**
     * A constant value for WAITING.
     */
    public static final int WAITING = 1;
    /**
     * A constant value for LEFTGAME.
     */
    public static final int LEFTGAME = 2;
    /**
     * A constant value for WON.
     */
    public static final int WON = 3;

    private int activePlayer;
    private int state;
    private static final Logger LOGGER = Logger.getLogger(PlayerEvent.class.getName());

    /**
     * A Constructor.
     * 
     * @param ludo
     *            - the gameboard to construct this event with
     */
    public PlayerEvent(Ludo ludo) {
        super(ludo);
    }

    /**
     * A constructor.
     * 
     * @param ludo
     *            - the gameboard to construct this event with
     * @param activePlayer
     *            - index of the active player
     * @param state
     *            - the state of the player
     */
    public PlayerEvent(Ludo ludo, int activePlayer, int state) {
        super(ludo);

        this.activePlayer = activePlayer;
        this.state = state;
    }

    @Override
    public void playerStateChanged(PlayerEvent event) {
        activePlayer = event.activePlayer;
        state = event.state;
    }

    @Override
    public int hashCode() {
        return (activePlayer * 127 + state) % 1337;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other == null || this.getClass() != other.getClass())) {
            try {
                PlayerEvent event = (PlayerEvent) other;
                return (activePlayer == event.activePlayer && state == event.state);
            } catch (RuntimeException e) {
                // Is different object
                LOGGER.warning(e.getMessage());
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return activePlayer + ", " + state;
    }
}
