/**
 * 
 */
package no.ntnu.imt3281.ludo.logic;

import java.util.EventListener;

/**
 * Listener for dice events.
 * 
 * @author okaad_000
 *
 */
public interface DiceListener extends EventListener {

    /**
     * Abstract function to implement.
     * 
     * @param diceEvent
     *            - the event to handle
     */
    void diceThrown(DiceEvent diceEvent);
}
