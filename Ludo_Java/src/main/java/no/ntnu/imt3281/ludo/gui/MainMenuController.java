/**
 * Sample Skeleton for 'MainMenu.fxml' Controller Class
 */

package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.logging.Logger;

import global.Globals;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * Controls all functions in main menu and the main menu window
 * 
 * @author okaad_000
 * @author HKNormann
 */
public class MainMenuController {
    private Client owner;
    private static final Logger LOGGER = Logger.getLogger(WindowController.class.getName());
    private ChatController chatController;

    @FXML
    private HBox titledPaneHBox;

    @FXML
    private TextField chatName;

    @FXML
    private ListView<String> playerList;

    @FXML
    private TextArea chatList;

    /**
     * initializes the main menu window and makes it possible to select multiple
     * players
     */
    @FXML
    void initialize() {
        createGlobalChat();
        playerList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    /**
     * creates the global chat window attached to the main menu
     */
    private void createGlobalChat() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"));
        loader.setResources(Client.getMessages());
        try {
            TitledPane chat = loader.load();
            chat.setText("global");
            chatController = loader.getController();
            titledPaneHBox.getChildren().add(chat);
            chatController.setTextPrefSize(300, 640);
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * sends a challenge request to the selected opponents
     * 
     * @param event
     */
    @FXML
    void challenge(ActionEvent event) {
        ObservableList<String> selectedOpponents = playerList.getSelectionModel().getSelectedItems();

        if (!selectedOpponents.isEmpty()) {
            String friendsMessage = "";
            for (String name : selectedOpponents) {
                friendsMessage = friendsMessage.concat(":" + name);
            }
            owner.sendMessage("Game:Join:Match" + friendsMessage);
        } else {
            owner.createDialog("Error", Client.getMessages().getString("popup.noneSelected"));
        }
    }

    /**
     * requests the server to create a chat with the name written in the chat
     * name textfield returns illegal input message if name in chat name
     * textfield is illegal
     * 
     * @param event
     */
    @FXML
    void createChat(ActionEvent event) {
        final int minLength = 4;
        final int maxLength = 16;

        if (Globals.isValid(chatName.getText(), minLength, maxLength)) {
            owner.createChat(chatName.getText());
        } else {
            String message = Client.getMessages().getString("popup.illegalTextInput");
            message += minLength + " - " + maxLength + ")";
            owner.createDialog("Error", message);
        }
    }

    /**
     * requests to join chat with name given in the chat name textfield creates
     * dialog popup with popup.alreadyInChat message if player is already in
     * chat
     * 
     * @param event
     */
    @FXML
    void joinChat(ActionEvent event) {
        String chat = chatName.getText();
        if (!owner.inChat(chat)) {
            owner.joinChat(chat);
        } else {
            owner.createDialog("", Client.getMessages().getString("popup.alreadyInChat"));
        }
    }

    /**
     * to be implemented when we get a friend system on the database
     * 
     * @param event
     */
    @FXML
    void newFriend(ActionEvent event) {
        owner.createDialog("", Client.getMessages().getString("popup.notYetImplemented"));
    }

    /**
     * sets the owning client
     * 
     * @param owner
     *            new owning client
     */
    public void setOwner(Client owner) {
        this.owner = owner;
        chatController.setOwner(owner);
    }

    /**
     * sends a message to the global chat board
     * 
     * @param clientName
     *            name of sender
     * @param message
     *            message to be sent
     */
    public void addGlobalMessage(String clientName, String message) {
        chatController.addMessage(clientName, message);
    }

    /**
     * updates the chat list
     * 
     * @param data
     *            list of chats from server
     */
    public void updateChatList(String data) {
        String[] chats = data.split(":");
        String newText = "";

        for (String chat : chats) {
            newText = newText.concat(chat + "\n");
        }
        chatList.setText(newText);
    }

    /**
     * updates the player list
     * 
     * @param data
     *            list of players from server
     */
    public void updatePlayerList(String data) {
        String[] players = data.split(":");
        ObservableList<String> items = FXCollections.observableArrayList();

        for (String player : players) {
            if (!player.equals(owner.getUsername())) {
                items.add(player);
            }
        }
        playerList.setItems(items);
    }

}
