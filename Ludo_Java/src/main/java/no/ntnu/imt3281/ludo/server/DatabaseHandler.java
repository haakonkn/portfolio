/**
 * 
 */
package no.ntnu.imt3281.ludo.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/**
 * class handling connection to ntnu database and sql
 * 
 * @author okaad_000
 * @author HKNormann
 *
 */
public class DatabaseHandler {

    private static final String DATABASE_URL = "jdbc:mysql://mysql.stud.ntnu.no/okdahl_Ludo";
    private String username;
    private String password;
    private static final String DB_CLASS = "com.mysql.jdbc.Driver";
    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());
    static java.sql.Connection con;

    /**
     * Connects to the database
     */
    public void connectToDatabase() {
        File credentials = new File("serverCredentials.properties");

        if (credentials.exists()) {
            try (BufferedReader br = new BufferedReader(new FileReader(credentials))) {
                username = br.readLine();
                password = br.readLine();
                Class.forName(DB_CLASS);
                con = DriverManager.getConnection(DATABASE_URL, username, password);
            } catch (SQLException | ClassNotFoundException | IOException e) {
                LOGGER.warning(e.getMessage());
            }
        }
    }

    /**
     * Tries to log in with username and password
     * 
     * @param username
     * @param password
     * @return if able to log in
     */
    public boolean tryLogin(String username, String password) {

        boolean login = false;
        final String loginQuery = "SELECT * FROM Accounts WHERE Accounts.Username = \"" + username + "\" AND Accounts.Password = \"" + password + '"';

        try (Statement statement = con.createStatement(); ResultSet res = statement.executeQuery(loginQuery)) {
            if (res.next()) {
                login = true;
            }
        } catch (SQLException e) {
            LOGGER.warning(e.getMessage());
        }
        return login;
    }

    /**
     * Tries to register user, will fail if user already exists or not able to
     * register
     * 
     * @param username
     * @param password
     * @return if able to register
     */
    public boolean registerUser(String username, String password) {

        final String existingQuery = "SELECT * FROM Accounts WHERE Accounts.Username = \"" + username + '"';
        boolean success = false;
        boolean alreadyExists = false;

        try (Statement statement = con.createStatement(); ResultSet res = statement.executeQuery(existingQuery)) {
            if (res.next()) {
                alreadyExists = true;
            }
        } catch (SQLException e) {
            LOGGER.warning(e.getMessage());
        }

        if (!alreadyExists) {
            final String registerQuery = "INSERT INTO `Accounts`(`Username`, `Password`) " + "VALUES (\"" + username + "\",\"" + password + "\")";
            try (Statement stmnt = con.createStatement()) {
                stmnt.executeUpdate(registerQuery);
                success = true;
            } catch (SQLException e) {
                LOGGER.warning(e.getMessage());
            }
        }
        return success;
    }

    /**
     * Increments win count of a user on database when that user wins
     * 
     * @param winnerName
     *            name of winner
     */
    public void registerWinner(String winnerName) {
        final String winQuery = "UPDATE Accounts Set gamesWon = gamesWon + 1  WHERE Accounts.Username = \"" + winnerName + '"';

        try (Statement stmnt = con.createStatement()) {
            stmnt.executeUpdate(winQuery);
        } catch (SQLException e) {
            LOGGER.warning(e.getMessage());
        }
    }
}
