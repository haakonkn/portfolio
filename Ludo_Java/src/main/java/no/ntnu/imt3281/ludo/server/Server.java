package no.ntnu.imt3281.ludo.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.JFrame;

/**
 * 
 * main class containing all serverside client data. Also contains all game data
 * and chat data
 * 
 * @author okaad_000
 * @author HKNormann
 *
 */
public class Server {

    /**
     * class containing client data and keeping track of last response
     * 
     * @author okaad_000
     * @author HKNormann
     */
    public static class ClientStruct {
        String username;
        PrintWriter socketOut;
        float timeSinceLastResponse;

        ClientStruct(String username, PrintWriter socketOut) {
            timeSinceLastResponse = 0.0f;
            this.username = username;
            this.socketOut = socketOut;
        }
    }

    /**
     * class containing all clients waiting to start a challenge game handles
     * who're ready and when to start
     * 
     * @author okaad_000
     * @author HKNormann
     */
    private class MatchmakingStruct {
        private Vector<ClientStruct> clientsWaiting;
        private Vector<Boolean> clientsReady;
        String matchID;

        /**
         * default contructor initializing values and requesting confirmation
         * from players invited
         * 
         * @param client
         *            host of game challenging the others
         * @param friends
         *            people being challenged by the host
         */
        MatchmakingStruct(ClientStruct client, Vector<ClientStruct> friends) {
            clientsWaiting = new Vector<>();
            clientsReady = new Vector<>();

            this.matchID = "Game" + gameID++;
            clientsWaiting.add(client);
            clientsReady.add(true);

            for (int i = 0; i < friends.size(); i++) {
                clientsWaiting.add(friends.elementAt(i));
                clientsReady.add(false);
                friends.elementAt(i).socketOut.println("Game:" + matchID + ":Confirmation:" + client.username);
            }
        }

        /**
         * sets the ready status of a player in the challenge queue
         * 
         * @param client
         *            client to set status
         * @param check
         *            boolean value of new status
         * @return true if all are ready and game is created
         */
        public boolean setClientStatus(ClientStruct client, boolean check) {
            boolean allReady = true;

            for (int i = 0; i < clientsWaiting.size(); i++) {
                if (clientsWaiting.elementAt(i) == client) {
                    if (check) {
                        clientsReady.set(i, check);
                    } else {
                        clientsWaiting.removeElementAt(i);
                        clientsReady.removeElementAt(i);
                    }
                }
                if (clientsWaiting.size() < 2) {
                    for (ClientStruct inClient : clientsWaiting) {
                        inClient.socketOut.println("Game:" + matchID + ":Canceled:" + client.username);
                    }
                }
                if (!clientsReady.elementAt(i)) {
                    allReady = false;
                }
            }

            if (allReady) {
                startGame(clientsWaiting, matchID);
            }

            return allReady;
        }
    }

    private static ServerSocket serverSocket;
    private static ExecutorService clientThreadPool = Executors.newCachedThreadPool();
    private static final int PORT = 5006;
    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());
    private static Vector<ClientStruct> clients = new Vector<>();
    private static Vector<Chat> chats = new Vector<>();
    private static Vector<Game> games = new Vector<>();
    private static Vector<ClientStruct> randomQueue = new Vector<>();
    private static Vector<MatchmakingStruct> matchmakingQueues = new Vector<>();
    private int gameID = 0;
    private FileHandler fh;

    /**
     * handler connecting server to ntnu database containing user data
     */
    private DatabaseHandler databaseHandler = new DatabaseHandler();

    /**
     * Sets port number and opens the server GUI. Starts connection thread
     * initialized all variables
     */
    public Server() {
        try { // initialize logger file
            fh = new FileHandler("ServerLog.log");
            LOGGER.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            LOGGER.info("Start of Server log");
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }

        databaseHandler.connectToDatabase();

        JFrame frame = new JFrame("Server");
        frame.setSize(200, 1000);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        // Creates global chat
        chats.add(new Chat("global"));

        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }

        ConnectionThread connectionThread = new ConnectionThread(serverSocket);
        connectionThread.setOwner(this);
        ExecutorService es = Executors.newCachedThreadPool();
        es.execute(connectionThread);
        es.shutdown();
    }

    /**
     * Starts a client thread listening for packets on a given socket connection
     * 
     * @param socket
     */
    public void startClientThread(Socket socket) {
        ClientThread clientThread = new ClientThread(socket, this);
        clientThreadPool.execute(clientThread);
    }

    /**
     * Handles register request and contacts database to check if valid
     * 
     * @param data
     *            - expects String "username:password"
     * @param socketOut
     *            - outgoing printWriter to client socket
     */
    public void handleRegister(String data, PrintWriter socketOut) {
        int split = data.indexOf(':');
        String username = data.substring(0, split);
        String password = data.substring(split + 1);

        if (databaseHandler.registerUser(username, password)) {
            socketOut.println("Register:true:");
        } else {
            socketOut.println("Register:false:popup.userAlreadyExists");
        }
    }

    /**
     * Handles login request and checks up against database
     * 
     * @param data
     *            - expects String "username:password"
     * @param socketOut
     *            - outgoing printWriter to client socket
     */
    public void handleLogin(String data, PrintWriter socketOut) {
        int split = data.indexOf(':');
        String username = data.substring(0, split);
        String password = data.substring(split + 1);

        if (getClient(username) == null) {
            // Checks if user info correct with database
            if (databaseHandler.tryLogin(username, password)) {
                // creates new client and adds to global chat and clients vector
                ClientStruct newClient = new ClientStruct(username, socketOut);
                clients.add(newClient);
                socketOut.println("Login:true:" + username);
                chats.get(0).getClients().addElement(newClient);

                updateAllLists();

            } else {
                socketOut.println("Login:false:popup.incorrectUsernamePassword");
            }
        } else {
            socketOut.println("Login:false:popup.userAlreadyLoggedIn");
        }
    }

    private void updateAllLists() {
        for (ClientStruct toClient : clients) {
            listPlayers(toClient.username);
        } // updates player list for all clients
    }

    /**
     * logs a player out and calls for the player to be removed from all lists
     * 
     * @param username
     *            username of client to remove
     * @return true if client could be found
     */
    public boolean handleLogout(String username) {
        Iterator<ClientStruct> iter = clients.iterator();
        while (iter.hasNext()) {
            ClientStruct client = iter.next();
            if (client.username.equals(username)) {
                removeClient(client);
                iter.remove();

                updateAllLists();

                return true;
            }
        }
        return false;
    }

    /**
     * gets client by name
     * 
     * @param username
     *            name of user to find
     * @return client struct if found, null if not
     */
    private ClientStruct getClient(String username) {
        ClientStruct returnClient = null;
        if (!username.equals("")) {
            for (ClientStruct client : clients) {
                if (client.username.equals(username)) {
                    returnClient = client;
                }
            }
        }
        return returnClient;
    }

    /**
     * Broadcasts text to a certain chat
     * 
     * @param chatName
     *            name of chat to broadcast to
     * @param clientName
     *            name of client broadcasting
     * @param message
     *            message client wants to broadcast
     */
    public void handleChatText(String chatName, String clientName, String message) {
        ClientStruct client = getClient(clientName);

        if (client != null) {
            for (Chat chat : chats) {
                if (chat.getChatName().equals(chatName)) {
                    chat.broadcastMessage(client.username, message);
                    break;
                }
            }
        }
    }

    /**
     * Creates a chat with chatName and adds client to it. returns if success or
     * not to client
     * 
     * @param chatName
     *            Name of new chat
     * @param clientName
     *            name of client creating chat
     */
    public void handleChatCreate(String chatName, String clientName) {
        ClientStruct client = getClient(clientName);
        String returnMessage = "Chat:Create:";

        if (client != null) {
            if (getChat(chatName) == null) {
                Chat newChat = new Chat(chatName);
                newChat.getClients().add(client);
                chats.add(newChat);
                returnMessage += "true:" + chatName;

                for (ClientStruct toClient : clients) {
                    listChats(toClient.username);
                } // updates chat list for all clients

            } else {
                returnMessage += "false:popup.chatAlreadyExists";
            }
            client.socketOut.println(returnMessage);
        }
    }

    /**
     * Adds a client to a chat and writes back if success or not to client
     * 
     * @param chatName
     *            name of chat client wants to join
     * @param clientName
     *            name of client requesting to join chat
     */
    public void handleChatJoin(String chatName, String clientName) {
        String returnMessage = "Chat:Join:";
        ClientStruct client = getClient(clientName);
        if (client != null) {
            Chat chat = getChat(chatName);
            if (chat != null) {
                chat.getClients().add(client);
                returnMessage += "true:" + chatName;
            } else {
                returnMessage += "false:popup.chatDoesNotExist";
            }
            client.socketOut.println(returnMessage);
        }
    }

    /**
     * Lists all chats and sends a packet containing the list to the requesting
     * client
     * 
     * @param clientName
     *            name of requesting client
     */
    public void listChats(String clientName) {
        ClientStruct client = getClient(clientName);
        if (client != null) {
            String returnMessage = "List:Chat";
            for (Chat chat : chats) {
                returnMessage = returnMessage.concat(":" + chat.getChatName());
            }
            client.socketOut.println(returnMessage);
        }
    }

    /**
     * Lists all players and sends a packet containing the list to the
     * requesting client
     * 
     * @param clientName
     *            name of requesting client
     */
    public void listPlayers(String clientName) {
        ClientStruct requestingClient = getClient(clientName);
        if (requestingClient != null) {
            String returnMessage = "List:Players";
            for (ClientStruct client : clients) {
                returnMessage = returnMessage.concat(":" + client.username);
            }
            requestingClient.socketOut.println(returnMessage);
        }
    }

    private static Chat getChat(String chatName) {
        Chat returnChat = null;
        for (Chat chat : chats) {
            if (chat.getChatName().equals(chatName)) {
                returnChat = chat;
                break;
            }
        }
        return returnChat;
    }

    private MatchmakingStruct getMatch(String matchID) {
        MatchmakingStruct matchStruct = null;
        for (MatchmakingStruct match : matchmakingQueues) {
            if (match.matchID.equals(matchID)) {
                matchStruct = match;
                break;
            }
        }
        return matchStruct;
    }

    /**
     * creates a new game queue that starts a game when all friends have
     * accepted Will send a waiting packet to host and accept / decline packet
     * to friends
     * 
     * @param clientName
     *            Name of client to start game
     * @param friendNames
     *            Name of friends to invite
     */
    public void handleGameCreate(String clientName, String friendNames) {
        ClientStruct host = getClient(clientName);
        String[] friends = friendNames.split(":");
        Vector<ClientStruct> joiningClients = new Vector<>();

        for (int i = 0; i < friends.length; i++) {
            ClientStruct client = getClient(friends[i]);
            if (client != null) {
                joiningClients.add(client);
            }
        }

        matchmakingQueues.add(new MatchmakingStruct(host, joiningClients));
    }

    /**
     * Sets client in a waiting queue, that will send a start game packet once 4
     * clients have been placed in that queue
     * 
     * @param clientName
     * 
     */
    public void handleGameRandomJoin(String clientName) {
        ClientStruct client = getClient(clientName);
        if (client != null) {
            boolean alreadyInQueue = false;

            for (ClientStruct inqueue : randomQueue) {
                if (inqueue.username.equals(clientName)) {
                    alreadyInQueue = true;
                    break;
                }
            }
            if (!alreadyInQueue) {
                randomQueue.add(client);
                if (randomQueue.size() >= 4) {
                    startGame(randomQueue, "Game" + gameID++);
                    randomQueue.clear();
                }
            }
        }
    }

    private void startGame(Vector<ClientStruct> startingQueue, String gameName) {
        Game game = new Game(gameName, this);
        Chat gameChat = new Chat(gameName + " chat");
        for (ClientStruct client : startingQueue) {
            game.getClients().add(client);
            gameChat.getClients().add(client);
        }
        chats.add(gameChat);
        games.add(game);

        game.startGame();
    }

    /**
     * Sets server to wait for packets.
     * 
     * @param args
     *            Command line arguments
     */
    public static void main(String[] args) {
        new Server();
        long nano = 1000000000;
        long lastTime = System.nanoTime() / nano;
        long delta;
        long now;

        while (!serverSocket.isClosed()) {
            now = System.nanoTime() / nano;
            delta = now - lastTime;
            if (delta >= 1) {
                updateTimeout(delta);
                lastTime = now;
            }
        }
        clientThreadPool.shutdown();
    }

    private static void updateTimeout(long delta) {
        for (ClientStruct client : clients) {
            if (!client.username.equals("")) {
                client.timeSinceLastResponse += delta;
                // timeouts players after 2 min of inactivity
                if (client.timeSinceLastResponse >= 120) {
                    client.socketOut.println("Logout:true:electric bogaloo");
                }
            }
        }
    }

    /**
     * Handles removing a client from the server Removes client from all chats,
     * games and queues and terminates these if they're left empty
     * 
     * @param client
     *            client to remove
     */
    private static void removeClient(ClientStruct client) {
        for (int i = 0; i < chats.size(); i++) {
            Chat chat = chats.elementAt(i);
            if (chat.removeClient(client) && !"global".equals(chat.getChatName())) {
                chats.remove(chat);
                --i;
            }
        }

        for (int i = 0; i < games.size(); i++) {
            Game game = games.elementAt(i);
            if (game.removeClient(client)) {
                chats.remove(getChat(game.getGameName() + " chat"));
                games.remove(game);
                --i;
            }
        }

        randomQueue.remove(client); // remove client from queues
        for (MatchmakingStruct match : matchmakingQueues) {
            for (int i = 0; i < match.clientsWaiting.size(); i++) {
                if (match.clientsWaiting.elementAt(i) == client) {
                    match.clientsWaiting.remove(i);
                    match.clientsReady.remove(i);
                }

                if (match.clientsWaiting.isEmpty()) { // terminates queue if
                                                      // empty
                    matchmakingQueues.remove(match);
                }
            }
        }
    }

    /**
     * passes on request to correct game instance
     * 
     * @param data
     */
    public void sendRequestToGame(String data) {
        String[] strings = data.split(":", 2);
        boolean gameFound = false;
        String requestGameID = strings[0];
        for (Game game : games) {
            if (game.getGameName().equals(requestGameID)) {
                game.handleRequest(strings[1]);
                gameFound = true;
            }
        }
        if (!gameFound) {
            LOGGER.warning("Server.sendRequestToGame - Server couldn't find game " + requestGameID);
        }
    }

    /**
     * Resets time since last response variable in client which controls if the
     * client should be disconnected
     * 
     * @param clientName
     *            username of client
     */
    public void updatePlayer(String clientName) {
        ClientStruct client = getClient(clientName);
        if (client != null) {
            client.timeSinceLastResponse = 0;
        }
    }

    /**
     * Updates if client confirms or denies game invite
     * 
     * @param clientName
     *            name of client sending message
     * @param data
     *            expected format GameID:acceptedBoolean
     */
    public void handleGameConfirm(String clientName, String data) {
        String[] strings = data.split(":");
        String matchID = strings[0];
        String check = strings[1];

        MatchmakingStruct match = getMatch(matchID);
        if (match != null && match.setClientStatus(getClient(clientName), Boolean.parseBoolean(check))) {
            matchmakingQueues.remove(match);
        }
    }

    /**
     * removes game from games vector
     * 
     * @param gameName
     *            name of game to remove
     */
    public void endGame(String gameName) {
        for (int i = 0; i < games.size(); i++) {
            if (games.elementAt(i).getGameName() == gameName) {
                games.removeElementAt(i);
            }
        }
    }

    /**
     * Gets the databaseHandler from server
     */
    public DatabaseHandler getDatabaseHandler() {
        return databaseHandler;
    }
}
