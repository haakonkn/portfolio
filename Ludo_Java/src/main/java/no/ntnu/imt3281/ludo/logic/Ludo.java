/**
 * 
 */
package no.ntnu.imt3281.ludo.logic;

import java.util.Random;
import java.util.Vector;

import exceptions.NoRoomForMorePlayersException;
import exceptions.NotEnoughPlayersException;
import no.ntnu.imt3281.ludo.server.Game;

/**
 * @author okaad_000
 *
 */
public class Ludo {
    private static final int PIECES = 4;

    /**
     * Index of Red player.
     */
    public static final int RED = 0;
    /**
     * Index of Blue player.
     */
    public static final int BLUE = 1;
    /**
     * Index of Yellow player.
     */
    public static final int YELLOW = 2;
    /**
     * Index of Green player.
     */
    public static final int GREEN = 3;

    private static final String WINNER = "Winner: ";

    private Vector<String> playerNames;
    private Vector<DiceListener> diceListeners;
    private Vector<PieceListener> pieceListeners;
    private Vector<PlayerListener> playerListeners;

    private int[][] piecePositions;
    private int[][] globalPiecePositions;

    private int activePlayer = RED;
    private int numThrows = 0;
    private int dice = 0;

    private Game owner;

    /**
     * Initialize a game without known players.
     */
    public Ludo() {
        playerNames = new Vector<>();
        diceListeners = new Vector<>();
        pieceListeners = new Vector<>();
        playerListeners = new Vector<>();

        piecePositions = new int[4][4];
        globalPiecePositions = new int[4][PIECES];
    }

    /**
     * Initializes a game with x amount of players.
     * 
     * @param name1
     *            - player 1.
     * @param name2
     *            - player 2.
     * @param name3
     *            - player 3.
     * @param name4
     *            - player 4.
     * @throws NotEnoughPlayersException
     *             - if there is not enough players, must be 2+.
     */
    public Ludo(String name1, String name2, String name3, String name4) {
        playerNames = new Vector<>();
        diceListeners = new Vector<>();
        pieceListeners = new Vector<>();
        playerListeners = new Vector<>();

        piecePositions = new int[4][PIECES];
        globalPiecePositions = new int[4][PIECES];

        playerNames.addElement(name1);
        playerNames.addElement(name2);
        playerNames.addElement(name3);
        playerNames.addElement(name4);

        if (nrOfPlayers() < 2) {
            throw new NotEnoughPlayersException();
        }
    }

    public void setOwner(Game owner) {
        this.owner = owner;
    }

    /**
     * Returns number of players.
     * 
     * @return number of players.
     */
    public int nrOfPlayers() {
        int numberOfPlayers = 0;

        for (int i = 0; i < playerNames.size(); i++) {
            if (playerNames.elementAt(i) != null) {
                numberOfPlayers++;
            }
        }

        return numberOfPlayers;
    }

    /**
     * Returns the name of the player at index.
     * 
     * @param index
     *            - index to fetch name from.
     * @return name of player at index.
     */
    public String getPlayerName(int index) {
        return playerNames.elementAt(index);
    }

    /**
     * Adds a new player, if there is room for another player.
     * 
     * @param newPlayer
     *            - name of the new player.
     * @throws NoRoomForMorePlayersException
     *             - if there is no more room, max of 4.
     */
    public void addPlayer(String newPlayer) {
        if (nrOfPlayers() < 4) {
            playerNames.addElement(newPlayer);

        } else {
            throw new NoRoomForMorePlayersException();
        }
    }

    /**
     * Completely removes a player
     * 
     * @param name
     *            - player to mark as inactive by prefixing their name with
     *            "Inactive: "
     */
    public void removePlayer(String name) {
        for (int player = 0; player < playerNames.size(); player++) {
            if (playerNames.elementAt(player) != null && playerNames.elementAt(player).equals(name)) {
                playerNames.set(player, "Inactive: " + playerNames.elementAt(player));

                for (PlayerListener listener : playerListeners) {
                    listener.playerStateChanged(new PlayerEvent(this, player, PlayerEvent.LEFTGAME));
                }

                for (int piece = 0; piece < PIECES; piece++) {
                    piecePositions[player][piece] = 0;
                    globalPiecePositions[player][piece] = player * 4 + piece;
                }
                broadcast("Disconnect", name);

                if (player == activePlayer && owner == null) {
                    nextPlayer();
                } else if (owner != null && activePlayers() <= 1) {
                    checkIfOnePlayerOrLess();
                }
            }
        }
    }

    private void checkIfOnePlayerOrLess() {
        if (activePlayers() == 1) {
            String remainingPlayer = lastRemainingPlayer();
            if (remainingPlayer != null) {
                owner.won(remainingPlayer);
            }
        }

        if (activePlayers() == 0) {
            owner.endGame();
        }
    }

    /**
     * @return number of active players.
     */
    public int activePlayers() {
        int count = 0;
        for (int i = 0; i < playerNames.size(); i++) {
            if (playerNames.elementAt(i) != null && !playerNames.elementAt(i).contains("Inactive: ")) {
                count++;
            }
        }
        return count;
    }

    /**
     * @return name of last remaining player
     */
    public String lastRemainingPlayer() {
        String playerName = null;

        for (int i = 0; i < playerNames.size(); i++) {
            if (playerNames.elementAt(i) != null && !playerNames.elementAt(i).contains("Inactive: ")) {
                playerName = playerNames.elementAt(i);
            }
        }
        return playerName;
    }

    /**
     * Gets a player's position in local space
     * 
     * @param player
     *            - player.
     * @param piece
     *            - player's piece.
     * @return positions of player's piece.
     */
    public int getPosition(int player, int piece) {
        return piecePositions[player][piece];
    }

    /**
     * @return active player.
     */
    public int activePlayer() {
        return activePlayer;
    }

    /**
     * Throws dice.
     * 
     * @return value of dice
     */
    public int throwDice() {
        Random rand = new Random();
        return throwDice(rand.nextInt(6) + 1);
    }

    /**
     * Throws a dice and checks if it's valid
     * 
     * @param dice
     *            - dice to handle
     * @return dice thrown
     */
    public int throwDice(int dice) {
        this.dice = dice;
        DiceEvent event = new DiceEvent(this, activePlayer, dice);

        for (DiceListener listener : diceListeners) {
            listener.diceThrown(event);
        }
        if (owner != null) { // if you're not testing
            broadcast("Update:Roll", Integer.toString(dice));
        }

        if (allHome()) {
            numThrows++;
            if (numThrows >= 3 && dice != 6) {
                nextPlayer();
            } else if (dice == 6) {
                broadcast("Request:Move", Integer.toString(activePlayer));
            } else {
                broadcast("Request:Roll", Integer.toString(activePlayer));
            }
        } else {
            if (canMove()) {
                numThrows++;

                if (dice == 6 && numThrows > 2) {
                    nextPlayer();
                } else {
                    broadcast("Request:Move", Integer.toString(activePlayer));
                }
            } else {
                nextPlayer();
            }
        }
        return dice;
    }

    private void broadcast(String type, String data) {
        if (owner != null) {
            owner.broadcastMessage(type, data);
        }
    }

    /**
     * Checks if a specific piece can move
     * 
     * @param piece
     * @return true if piece can be moved
     */
    public boolean canMove(int piece) {
        boolean movable = false;

        if ((piecePositions[activePlayer][piece] + dice < 60 && piecePositions[activePlayer][piece] != 0)
                || (piecePositions[activePlayer][piece] == 0 && dice == 6)) {
            movable = true;
        }

        return movable;
    }

    private boolean canMove() {
        boolean movable = false;

        for (int i = 0; i < PIECES && !movable; i++) {
            if (!isBlocked(i)) {
                movable = canMove(i);
            }
        }
        return movable;
    }

    /**
     * Checks if there are towers blocking the way for a piece.
     * 
     * @param currentPiece
     *            - the current piece to be checked from
     * @return true if there is no tower blocking the way.
     */
    private boolean isBlocked(int currentPiece) {
        for (int player = 0; player < playerNames.size(); player++) {
            if (player != activePlayer) {
                for (int piece = 0; piece < PIECES; piece++) {
                    for (int secondPiece = piece + 1; secondPiece < PIECES; secondPiece++) {
                        if (piecePositions[player][piece] == piecePositions[player][secondPiece]) {
                            for (int currentDice = 1; currentDice <= dice; currentDice++) {

                                int playerPosition = globalPiecePositions[activePlayer][currentPiece] + currentDice;

                                if (piecePositions[player][piece] == 0) {
                                    playerPosition = userGridToLudoBoardGrid(player, 1) + currentDice;
                                }

                                if (globalPiecePositions[player][piece] == playerPosition) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Change to next player.
     */
    private void nextPlayer() {
        numThrows = 0;

        for (PlayerListener listener : playerListeners) {
            listener.playerStateChanged(new PlayerEvent(this, activePlayer, PlayerEvent.WAITING));
        }

        activePlayer = (activePlayer + 1) % playerNames.size();

        for (PlayerListener listener : playerListeners) {
            listener.playerStateChanged(new PlayerEvent(this, activePlayer, PlayerEvent.PLAYING));
        }

        if (owner != null) {
            owner.broadcastMessage("Request:Roll", Integer.toString(activePlayer));
        }

        if (playerNames.elementAt(activePlayer) == null || playerNames.elementAt(activePlayer).contains("Inactive: ")
                || playerNames.elementAt(activePlayer).contains(WINNER)) {
            nextPlayer();
        }
    }

    /**
     * Tries to move the player's piece if possible, it can be blocked, or the
     * player might have gotten a third 6.
     * 
     * @param player
     *            - index of player
     * @param from
     *            - tile to move from
     * @param to
     *            - tile to move to
     * @return true if the piece is possible to move.
     */
    public boolean movePiece(int player, int from, int to) {
        boolean success = false;

        for (int piece = 0; piece < PIECES && !success; piece++) {
            success = movePiece(player, piece, from, to);
        }
        return success;
    }

    /**
     * Moves a player's piece
     * 
     * @param player
     * @param piece
     * @param from
     * @param to
     * @return true if piece could be moved
     */
    public boolean movePiece(int player, int piece, int from, int to) {
        boolean success = false;

        if (piecePositions[player][piece] == from) {

            if (from == 0 && dice == 6) {
                to = 1;
            }

            piecePositions[player][piece] = to;

            success = true;
            checkWinner();

            for (PieceListener listener : pieceListeners) {
                listener.pieceMoved(new PieceEvent(this, player, piece, from, to));
            }

            checkUnfortunateOpponents(player, to);
            updateGlobalPositions();

            int globalTo = globalPiecePositions[player][piece];

            String movedata = player + ":" + piece + ":" + globalTo;
            broadcast("Update:Move", movedata);

            if (numThrows >= 3 || dice != 6 || from == 0) {
                nextPlayer();
            } else {
                broadcast("Request:Roll", Integer.toString(activePlayer));
            }
        }
        return success;
    }

    /**
     * @return status of the game.
     */
    public String getStatus() {
        String state = "Created";

        if (nrOfPlayers() != 0) {
            state = "Initiated";
        }

        if (dice != 0) {
            state = "Started";
        }

        for (int i = 0; i < nrOfPlayers(); i++) {
            boolean finished = true;

            for (int j = 0; j < PIECES; j++) {
                finished = true;
                if (piecePositions[i][j] != 59) {
                    finished = false;
                    break;
                }
            }
            if (finished) {
                state = "Finished";

            }
        }

        return state;
    }

    private void checkWinner() {
        for (int player = 0; player < playerNames.size(); player++) {
            boolean finished = true;

            for (int j = 0; j < PIECES; j++) {
                if (piecePositions[activePlayer][j] != 59) {
                    finished = false;
                }
            }
            if (finished && getWinner() == -1) {

                if (owner != null) {
                    owner.won(playerNames.elementAt(activePlayer));
                }

                playerNames.set(player, WINNER + playerNames.elementAt(activePlayer));

                for (PlayerListener listener : playerListeners) {
                    listener.playerStateChanged(new PlayerEvent(this, player, PlayerEvent.WON));
                }

                break;
            }
        }
    }

    /**
     * 
     * @return winner.
     */
    public int getWinner() {
        int winner = -1;

        for (int player = 0; player < playerNames.size(); ++player) {
            if (playerNames.elementAt(player) != null && playerNames.elementAt(player).contains(WINNER)) {
                winner = player;
            }
        }
        return winner;
    }

    /**
     * @return true if all pieces of active player are home.
     */
    private boolean allHome() {
        for (int i = 0; i < PIECES; i++) {
            if (piecePositions[activePlayer][i] != 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Converts player position to grid.
     * 
     * @param player
     *            - player to convert.
     * @param pos
     *            - player's position.
     * @return - the board grid index.
     */
    public int userGridToLudoBoardGrid(int player, int pos) {
        int result = 0;
        if (pos == 0) {
            result = 4 * player;
        } else if (pos < 54) {
            result = (player * 13 + pos - 1) % 52 + 16;
        } else {
            result = 68 + (pos - 54) + 6 * player;
        }
        return result;
    }

    /**
     * Check whether a piece is occupying a position.
     * 
     * @param player
     * @param position
     */
    private void checkUnfortunateOpponents(int player, int position) {
        int playerPosition = userGridToLudoBoardGrid(player, position);

        for (int otherPlayer = 0; otherPlayer < playerNames.size(); otherPlayer++) {
            if (otherPlayer != player) {
                for (int piece = 0; piece < PIECES; piece++) {
                    if (userGridToLudoBoardGrid(otherPlayer, piecePositions[otherPlayer][piece]) == playerPosition) {
                        for (PieceListener listener : pieceListeners) {
                            listener.pieceMoved(new PieceEvent(this, otherPlayer, piece, piecePositions[otherPlayer][piece], 0));
                        }

                        piecePositions[otherPlayer][piece] = 0;

                        String movedata = otherPlayer + ":" + piece + ":" + Integer.toString(piece + otherPlayer * 4);
                        broadcast("Update:Move", movedata);
                    }
                }
            }
        }
    }

    /**
     * Figures out all the pieces' actual global positions and updates their
     * values. This is updated when a piece is moved.
     */
    private void updateGlobalPositions() {
        for (int player = 0; player < playerNames.size(); player++) {
            for (int piece = 0; piece < PIECES; piece++) {
                if (piecePositions[player][piece] > 0) {
                    globalPiecePositions[player][piece] = userGridToLudoBoardGrid(player, piecePositions[player][piece]);
                }
            }
        }
    }

    /**
     * Adds a DiceListener object to the vector of DiceListeners.
     * 
     * @param diceListener
     *            - DiceListener to add
     */
    public void addDiceListener(DiceListener diceListener) {
        diceListeners.addElement(diceListener);
    }

    /**
     * Adds a PieceListener object to the vector of PieceListeners.
     * 
     * @param pieceListener
     *            - PieceListener to add
     */
    public void addPieceListener(PieceListener pieceListener) {
        pieceListeners.addElement(pieceListener);
    }

    /**
     * Adds a PlayerListener object to the vector of PlayerListeners.
     * 
     * @param playerListener
     *            - PlayerListener to add
     */
    public void addPlayerListener(PlayerListener playerListener) {
        playerListeners.addElement(playerListener);
    }

    /**
     * 
     * @return dice
     */
    public int getDice() {
        return dice;
    }

    /**
     * @param dice
     *            - dice to set
     */
    public void setDice(int dice) {
        this.dice = dice;
    }

    /**
     * @return activePlayer
     */
    public int getActivePlayer() {
        return activePlayer;
    }

    /**
     * 
     * @param activePlayer
     *            - activePlayer to set
     */
    public void setActivePlayer(int activePlayer) {
        this.activePlayer = activePlayer;
    }

    /**
     * @return the piecePositions
     */
    public int[][] getPiecePositions() {
        return piecePositions;
    }

    /**
     * @param piecePositions
     *            - the piecePositions to set
     */
    public void setPiecePositions(int[][] piecePositions) {
        this.piecePositions = piecePositions;
    }

}