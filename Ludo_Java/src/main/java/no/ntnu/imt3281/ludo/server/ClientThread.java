/**
 * 
 */
package no.ntnu.imt3281.ludo.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * class for thread handling communication with client
 * 
 * @author okaad_000
 * @author HKNormann
 */
public class ClientThread extends Thread {
    private Server server;
    private BufferedReader socketIn;
    private PrintWriter socketOut;
    private Socket socket;
    private String myUsername = "";

    /**
     * logger holding error messages
     */
    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    /**
     * constructor for clientThread that sets socket to client and reference
     * server
     * 
     * @param serverSocket
     *            socket connecting server to player
     * @param server
     *            reference to server
     */
    public ClientThread(Socket serverSocket, Server server) {
        this.socket = serverSocket;
        try {
            socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            socketOut = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
        this.server = server;
    }

    /**
     * running loop of thread, constantly looking for messages to handle
     */
    @Override
    public void run() {
        while (!socket.isClosed()) {
            try {
                if (socketIn.ready()) {
                    handleMessage(socketIn.readLine());
                }
            } catch (IOException e) {
                LOGGER.warning(e.getMessage());
            }
        }
    }

    /**
     * Sets the thread's reference to owning server.
     * 
     * @param server
     *            - the server that owns this thread
     * 
     */
    public void setOwner(Server server) {
        this.server = server;
    }

    /**
     * handles incoming messages from connected client
     * 
     * @param message
     *            message to handle
     */
    private void handleMessage(String message) {
        String[] strings = message.split(":", 2);
        String tag = strings[0];
        String data = strings[1];
        // extracts type of message

        LOGGER.info(myUsername + " :" + message); // log message from client

        if (!myUsername.equals("")) {
            server.updatePlayer(myUsername);
        }

        switch (tag) {
            case "Login" :
                handleLogin(data);
                break;
            case "Register" :
                handleRegister(data);
                break;
            case "Logout" :
                handleLogout();
                break;
            case "Chat" :
                handleIncomingChat(data);
                break;
            case "Game" :
                handleGame(data);
                break;
            case "List" :
                handleList(data);
                break;
            default :
                LOGGER.warning("ClientThread::handleMessage::Package not recognized: " + message);
        }
    }

    /**
     * handles list request messages
     * 
     * @param data
     *            rest of message, expected format "Chat" of "Players"
     */
    private void handleList(String data) {
        switch (data) {
            case "Chat" :
                server.listChats(myUsername);
                break;
            case "Players" :
                server.listPlayers(myUsername);
                break;
            default :
                LOGGER.warning("ClientThread::handleList::Package not recognized: " + data);
        }
    }

    /**
     * handles game request messages
     * 
     * @param data
     *            message data, expected format starting with "Join:" or
     *            "Request:"
     */
    private void handleGame(String data) {
        String[] strings = data.split(":", 3);
        String messageType = strings[0];

        switch (messageType) {
            case "Join" : {
                String joinType = strings[1];
                if (joinType.equals("Random")) {
                    server.handleGameRandomJoin(myUsername);
                } else if (joinType.equals("Match")) {
                    server.handleGameCreate(myUsername, strings[2]);
                } else if (joinType.equals("Confirm")) {
                    server.handleGameConfirm(myUsername, strings[2]);
                }
                break;
            }
            case "Request" : { // Handles in-game logic requests
                server.sendRequestToGame(strings[1] + ":" + strings[2]);
                break;
            }
            default :
                LOGGER.warning("ClientThread::handleGame::Package not recognized: " + data);
        }
    }

    /**
     * handle chat requests
     * 
     * @param data
     *            request data, expected format starting with "Text:", "Create"
     *            or "Join"
     */
    private void handleIncomingChat(String data) {
        if (!myUsername.equals("")) {
            String[] strings = data.split(":", 3);
            String type = strings[0];
            String chatRoom = strings[1];
            String message = strings[2];

            switch (type) {
                case "Text" :
                    server.handleChatText(chatRoom, myUsername, message);
                    break;
                case "Create" :
                    server.handleChatCreate(chatRoom, myUsername);
                    break;
                case "Join" :
                    server.handleChatJoin(chatRoom, myUsername);
                    break;
                default :
                    LOGGER.warning("ClientThread::handleIncomingChat::Package not recognized: " + data);
            }
        }
    }

    /**
     * handles login requests and passes them on to server if valid
     * 
     * @param data
     *            login data expected to contain existing username and password
     *            combination
     */
    private void handleLogin(String data) {
        if (myUsername.equals("")) {
            int split = data.indexOf(':');
            String username = data.substring(0, split);
            myUsername = username;
            server.handleLogin(data, socketOut);
        } else {
            socketOut.println("Login:false:popup.clientAlreadyLoggedIn");
        }
    }

    /**
     * handles register requests and passes them on to server if not already
     * logged in
     * 
     * @param data
     *            register data expected to contain new username and password
     */
    private void handleRegister(String data) {
        if (myUsername.equals("")) {
            server.handleRegister(data, socketOut);
        } else {
            socketOut.println("Login:false:popup.clientAlreadyLoggedIn");
        }
    }

    private void handleLogout() {
        if (!myUsername.equals("")) {
            if (server.handleLogout(myUsername)) {
                myUsername = "";
            } else {
                socketOut.println("Logout:false:popup.logoutFailed");
            }
        } else {
            socketOut.println("Logout:false:popup.logoutFailed");
        }
    }
}
