package no.ntnu.imt3281.ludo.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import no.ntnu.imt3281.ludo.gui.WindowController;

/**
 * 
 * This is the main class for the client. **Note, change this to extend other
 * classes if desired.**
 * 
 * @author okaad_000
 *
 */
public class Client extends Application {

    private static final String EMPTYSTRING = "";

    /**
     * internationalized message bundle
     */
    private static ResourceBundle messages;
    private static final int PORT = 5006;
    private Vector<String> chats = new Vector<>();
    private Vector<String> games = new Vector<>();

    private static final Logger LOGGER = Logger.getLogger(Client.class.getName());
    private static Socket socket;
    private static PrintWriter socketOut;

    /**
     * Client's username
     */
    private String username = "";

    /**
     * Windowcontroller controls all windows on client side
     */
    private WindowController windowController;

    WaitForPacketsThread waitForPackageThread;

    /**
     * Constructor.
     */
    public Client() {
        setLanguage();
    }

    /**
     * Lets the user set language
     */
    private void setLanguage() {

        ChoiceDialog<String> choiceDialog = new ChoiceDialog("Norsk", "Norsk", "English");
        choiceDialog.setTitle("Language");
        choiceDialog.setContentText("Select language");
        Optional<String> result = choiceDialog.showAndWait();
        getSelectedLanguage(result);
        choiceDialog.close();

    }

    private void getSelectedLanguage(Optional<String> result) {
        if (result.isPresent()) {

            switch (result.get()) {
                case "Norsk" :
                    setResourceBundle("no", "NO");
                    break;
                case "English" :
                    setResourceBundle("en", "US");
                    break;
                default :
                    LOGGER.warning("Language not recognized");
            }

        }
    }

    private static void setResourceBundle(String language, String country) {
        Locale currentLocale = new Locale(language, country);
        messages = ResourceBundle.getBundle("no.ntnu.imt3281.I18N.MessageBundle", currentLocale);
    }

    /**
     * opens the main window - game window
     * 
     * @see javafx.application.Apllication
     */
    @Override
    public void start(Stage primaryStage) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../gui/GameWindow.fxml"));
            loader.setResources(messages);
            AnchorPane root = loader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setOnCloseRequest(e -> {
                if (socket != null) { // make sure the client can exit
                    handleLogout("true:pls");
                }
                System.exit(0);
            });
            setWindowController(loader.getController());
            getWindowController().setOwner(this);
            getWindowController().openLoginRegisterGUI(null);
            chats.add("global");

            // start thread that listens for packages from server
            waitForPackageThread = new WaitForPacketsThread(socket);
            waitForPackageThread.setOwner(this);
            ExecutorService es = Executors.newCachedThreadPool();
            es.execute(waitForPackageThread);
            es.shutdown();
        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * connects to local host server creates printWriter socketOut for incoming
     * messages from server
     */
    public static void connectToServer() {
        try {
            socket = new Socket(InetAddress.getLocalHost(), PORT);
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
        
        try {
            socketOut = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * A function that generically sends message to server.
     * 
     * @param message
     *            - message to be sent.
     */
    public void sendMessage(String message) {
        socketOut.println(message);
    }

    /**
     * creates a dialog window popoup
     * 
     * @param title
     *            title of dialog
     * @param text
     *            text in dialog
     */
    public void createDialog(String title, String text) {
        Platform.runLater(() -> {
            Alert dialog = new Alert(AlertType.CONFIRMATION);
            dialog.setTitle(title);
            dialog.setContentText(text);
            dialog.setHeaderText(null);
            dialog.show();
        });
    }

    /**
     * Finds type of message and handles it correspondingly
     * 
     * @param message
     *            - expected input tag:data
     */
    public void handleMessage(String message) {

        int index = message.indexOf(':');
        String tag = message.substring(0, index);
        String data = message.substring(index + 1);

        switch (tag) {
            case "Login" :
                handleLogin(data);
                break;
            case "Register" :
                handleRegister(data);
                break;
            case "Logout" :
                handleLogout(data);
                break;
            case "Chat" :
                handleChat(data);
                break;
            case "Game" :
                handleGame(data);
                break;
            case "List" :
                handleList(data);
                break;
            default :
                LOGGER.warning("Client::handleMessage - Package not recognized: " + message);
        }
    }

    /**
     * 
     * @param data
     */
    private void handleGame(String string) {
        String[] strings = string.split(":", 3);
        String gameID = strings[0];
        String type = strings[1];
        String data = strings[2];
        switch (type) {
            case "Created" :
                handleGameCreated(gameID + ":" + data);
                break;
            case "Confirmation" :
                handleGameConfirmation(gameID + ":" + data);
                break;
            case "Canceled" :
                createDialog("", messages.getString("popup.noOneLikesYou"));
                break;
            case "Update" :
                Platform.runLater(() -> getWindowController().passMessageToGame(type, gameID + ":" + data));
                break;
            case "Request" :
                Platform.runLater(() -> getWindowController().passMessageToGame(type, gameID + ":" + data));
                break;
            case "Disconnect" :
                Platform.runLater(() -> getWindowController().passMessageToGame(type, gameID + ":" + data));
                break;
            case "Winner" :
                Platform.runLater(() -> getWindowController().passMessageToGame(type, gameID + ":" + data));
                break;
            default :
                LOGGER.warning("Client::handleGame - Package not recognized: " + string);
                break;
        }
    }

    private void handleGameConfirmation(String string) {
        String[] strings = string.split(":");
        String matchID = strings[0];
        String hostName = strings[1];

        Platform.runLater(() -> {
            Alert dialog = new Alert(AlertType.CONFIRMATION);
            dialog.setTitle(messages.getString("popup.gameInviteTitle") + hostName);
            dialog.setContentText(messages.getString("popup.gameInviteText"));
            dialog.setHeaderText(messages.getString("popup.acceptTimer"));
            PauseTransition delay = new PauseTransition(Duration.seconds(15));
            delay.setOnFinished(event -> {
                sendMessage("Game:Join:Confirm:" + matchID + ":false");
                dialog.close();
            });
            delay.play();

            dialog.showAndWait().ifPresent(response -> {
                if (response == ButtonType.OK) {
                    sendMessage("Game:Join:Confirm:" + matchID + ":true");
                    dialog.close();
                }
                if (response == ButtonType.CANCEL) {
                    sendMessage("Game:Join:Confirm:" + matchID + ":false");
                    dialog.close();
                }
            });
        });

    }

    /**
     * creates a game window with a unique gameID and names of players in game
     * 
     * @param data
     *            game data, expected format:
     *            gameID:"Player1name:Player2name:Player3name..."
     */
    private void handleGameCreated(String data) {
        String[] strings = data.split(":", 2);
        String gameID = strings[0];
        String playerIDs = strings[1];
        Platform.runLater(() -> getWindowController().createGame(gameID, playerIDs));
    }

    /**
     * updates list with data from server
     * 
     * @param message
     *            data for updating, expected format starts with "Chat:" or
     *            "Players:" and a list of elements separated by ":"
     */
    private void handleList(String message) {
        int index = message.indexOf(':');
        String tag = message.substring(0, index);
        String data = message.substring(index + 1);

        switch (tag) {
            case "Chat" : 
                Platform.runLater(() -> getWindowController().mainMenuController.updateChatList(data));
                break;
            case "Players" : 
                Platform.runLater(() -> getWindowController().mainMenuController.updatePlayerList(data));
                break;
            default :
                LOGGER.warning("Client::handleList - Package not recognized: " + message);
                break;
        }

    }

    /**
     * handles chat data
     * 
     * @param data
     *            expected format starts with "Text", "Join" or "Create" +
     *            additional data
     */
    private void handleChat(String data) {
        String[] strings = data.split(":", 4);
        String type = strings[0];
        String message = "";

        if ("Text".equals(type)) {
            String chatName = strings[1];
            String clientName = strings[2];
            message = strings[3];
            if (clientName.equals(getUsername())) {
                clientName = messages.getString("client.me");
            }
            getWindowController().addMessage(clientName, chatName, message);
        } else if ("Join".equals(type) || "Create".equals(type)) {

            String check = strings[1];
            if (Boolean.parseBoolean(check)) {
                String chatName = strings[2];
                chats.add(chatName);
                // makes sure it doesn't open new windows with game chats
                if (!(chatName.startsWith("game") && "Create".equals(type))) {
                    Platform.runLater(() -> getWindowController().addChat(chatName));
                }
            } else {
                message = strings[2];
                createDialog("", messages.getString(message));
            }
        }
    }

    /**
     * Gets logout response from server and displays popup
     * 
     * @param data
     *            data in message from server expected format boolean:message
     */
    public void handleLogout(String data) {
        int index = data.indexOf(':');
        String check = data.substring(0, index);

        if (Boolean.parseBoolean(check)) {
            if (!username.equals(EMPTYSTRING)) {
                Platform.runLater(() -> {
                    getWindowController().setTitle("");
                    getWindowController().openLoginRegisterGUI(null);
                    getWindowController().clearMenu();
                });
                sendMessage("Logout:pls");
                setUsername(EMPTYSTRING);
                chats.clear();
                games.clear();
                createDialog("", messages.getString("popup.logoutSuccessful"));
            }
        } else {
            createDialog("", messages.getString("popup.logoutFailed"));
        }
    }

    /**
     * Gets register response from server and displays popup
     * 
     * @param data
     *            data in message from server expected format boolean:message
     */
    private void handleRegister(String data) {
        int index = data.indexOf(':');
        String check = data.substring(0, index);
        if (Boolean.parseBoolean(check)) {
            createDialog("", messages.getString("popup.registerSuccessful"));
        } else {
            String message = data.substring(index + 1);
            createDialog("", messages.getString(message));
        }
    }

    /**
     * Gets login response from server and opens main menu if successful
     * Displays error popup if not
     * 
     * @param data
     *            data in message from server expected format boolean:message
     */
    private void handleLogin(String data) {
        int index = data.indexOf(':');
        String check = data.substring(0, index);

        if (Boolean.parseBoolean(check)) {
            int split = data.indexOf(':');
            this.username = data.substring(split + 1);
            Platform.runLater(() -> {
                getWindowController().setTitle(messages.getString("gameWindow.loggedInAs") + username);
                getWindowController().getLoginController().close(null);
                getWindowController().createMainMenu();
                sendMessage("List:Chat"); // update chat list TODO add friend
                                          // update
            });
        } else {
            String message = data.substring(index + 1);
            createDialog("", messages.getString(message));
        }
    }

    /**
     * Launches the JavaFX GUI and makes client wait for packets.
     * 
     * @param args
     *            - command line arguments
     */
    public static void main(String[] args) {
        connectToServer();
        launch(args);
    }

    /**
     * Sends message from client to chatRoom
     * 
     * @param chatRoom
     *            - what chat room to send to
     * @param message
     *            - the message to be sent
     */
    public void sendChatMessage(String chatRoom, String message) {
        String data = "Chat:Text:" + chatRoom + ":" + message;
        sendMessage(data);
    }

    /**
     * Requests to create a chat with name
     * 
     * @param chatName
     *            requested name og chat
     */
    public void createChat(String chatName) {
        String data = "Chat:Create:" + chatName + ":pls";
        sendMessage(data);
    }

    /**
     * Requests to join a chat with name
     * 
     * @param chatName
     *            requested name of chat
     */
    public void joinChat(String chatName) {
        String data = "Chat:Join:" + chatName + ":pls";
        sendMessage(data);
    }

    /**
     * requests server to send list of a certain item
     * 
     * @param item
     *            item to get list of, expected format either "Players" or
     *            "Chat"
     */
    public void getList(String item) {
        String data = "List:" + item;
        sendMessage(data);
    }

    /**
     * checks if chat exists
     * 
     * @param chat
     *            name of chat to look for
     * @return true if chat found
     */
    public boolean inChat(String chat) {
        boolean inChat = false;
        for (String chatName : chats) {
            if (chatName.equals(chat)) {
                inChat = true;
                break;
            }
        }
        return inChat;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public WindowController getWindowController() {
        return windowController;
    }

    public void setWindowController(WindowController windowController) {
        this.windowController = windowController;
    }

    /**
     * @return the messages
     */
    public static ResourceBundle getMessages() {
        return messages;
    }

    /**
     * @param messages the messages to set
     */
    public static void setMessages(ResourceBundle messages) {
        Client.messages = messages;
    }
}
