/**
 * 
 */
package no.ntnu.imt3281.ludo.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * class handling the connection of new clients and generating of own threads
 * for these clients
 * 
 * @author okaad_000
 * @author HKNormann
 * 
 */
public class ConnectionThread extends Thread {

    private Server server;
    private ServerSocket serverSocket;

    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    /**
     * Connection thread constructor default socket
     * 
     * @param serverSocket
     *            socket taking connection requests from all clients
     */
    public ConnectionThread(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    /**
     * run loop of thread looking for new clients to accept and give own threads
     */
    @Override
    public void run() {
        while (!serverSocket.isClosed()) {
            try {
                Socket socket = serverSocket.accept();
                server.startClientThread(socket);
            } catch (IOException ioe) {
                LOGGER.warning(ioe.getMessage());
            }
        }
    }

    /**
     * Sets the thread's owning server.
     * 
     * @param server
     *            - the server that owns this thread
     */
    public void setOwner(Server server) {
        this.server = server;
    }
}