/**
 * Sample Skeleton for 'Login.fxml' Controller Class
 */

package no.ntnu.imt3281.ludo.gui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.logging.Logger;
import global.Globals;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * Controls the login window and every related function
 * 
 * @author okaad_000
 * @author HKNormann
 *
 */
public class LoginController {
    private Client owner;
    private static final Logger LOGGER = Logger.getLogger(LoginController.class.getName());
    private static final int MINLENGTH = 3; // min length of password/username
                                            // input
    private static final int MAXLENGTH = 16; // max length of input

    @FXML // fx:id="username"
    private TextField username; // Value injected by FXMLLoader

    @FXML // fx:id="password"
    private PasswordField password; // Value injected by FXMLLoader

    @FXML // fx:id="requirementWarning"
    private Text requirementWarning;

    @FXML // fx:id="remember"
    private CheckBox remember;

    /**
     * Gets remembered username and password from encrypted file and puts them
     * into the username and password fields
     */
    @FXML
    public void initialize() {
        File file = null;
        try {
            file = new File("user.txt");

            if (file.exists()) {
                byte[] bytes = null; // get the content in bytes
                bytes = Files.readAllBytes(file.toPath());

                if (bytes.length > 0) { // decrypts using two-way encryption on
                                        // bytes
                    String data = new String(encrypt(bytes), Charset.defaultCharset());
                    int splitter = data.indexOf(':');
                    String name = data.substring(0, splitter);
                    String pass = data.substring(splitter + 1, data.length());

                    username.setText(name);
                    password.setText(pass);
                }
            }
        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }
    }

    /**
     * Sets the LudoController's owning client.
     * 
     * @param owner
     *            - the client that owns this controller
     */
    public void setOwner(Client owner) {
        this.owner = owner;
    }

    /**
     * closes the login window
     * 
     * @param event
     *            - action performed
     */
    @FXML
    public void close(ActionEvent event) {
        Stage stage = (Stage) username.getScene().getWindow();
        stage.close();
    }

    /**
     * sends login request to server
     * 
     * @param event
     */
    @FXML
    void login(ActionEvent event) {
        if (fieldsValid()) {
            String message = "Login:" + username.getText() + ":" + password.getText();
            owner.sendMessage(message);
            if (remember.isSelected()) {
                writeUserdataToFile();
            }
        }
    }

    /**
     * Encrypts user password and name and writes them to file
     */
    private void writeUserdataToFile() {
        FileOutputStream fop = null;
        File file = null;
        String content = username.getText() + ":" + password.getText();
        byte[] bytes = content.getBytes(Charset.defaultCharset());
        bytes = encrypt(bytes);

        try {
            file = new File("user.txt");
            fop = new FileOutputStream(file);

            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            fop.write(bytes);
            fop.flush();
            fop.close();

        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        } finally {
            try {
                if (fop != null) {
                    fop.close();
                }
            } catch (IOException e) {
                LOGGER.warning(e.getMessage());
            }
        }
    }

    /**
     * XOR encrypts bytes with key using this method twice gives the original
     * bytes
     * 
     * @param bytes
     *            to encrypt
     * @return encrypted bytes
     */
    private byte[] encrypt(byte[] bytes) {

        String key = "ØyvindErEnKulLærer,JegLikerFlyingSkeletonVideoenHansPåYoutube";
        while (key.getBytes().length < bytes.length) {
            key = key.concat(key);
        }
        byte[] keyBytes = key.getBytes();
        byte[] encrypted = new byte[bytes.length];

        int i = 0;
        for (byte b : bytes) {
            encrypted[i] = (byte) (b ^ keyBytes[i++]);
        }

        return encrypted;
    }

    /**
     * sends register message to server
     * 
     * @param event
     */
    @FXML
    void register(ActionEvent event) {
        if (fieldsValid()) {
            String message = "Register:" + username.getText() + ":" + password.getText();
            owner.sendMessage(message);
        }
    }

    /**
     * colors text-boxes based on if they are valid or not checks text fields
     * against global valid function to see if they only contain legal
     * characters
     * 
     * @return true if both fields are valid
     */
    private boolean fieldsValid() {
        boolean valid = true;

        if (Globals.isValid(username.getText(), MINLENGTH, MAXLENGTH)) {
            username.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        } else {
            username.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
            valid = false;
        }

        if (Globals.isValid(password.getText(), MINLENGTH, MAXLENGTH)) {
            password.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        } else {
            password.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
            valid = false;
        }
        // displays requirements if the inputs are not valid
        requirementWarning.setVisible(!valid);
        return valid;
    }
}
