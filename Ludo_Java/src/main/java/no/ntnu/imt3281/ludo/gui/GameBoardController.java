package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import no.ntnu.imt3281.ludo.client.Client;

/**
 * Controls gameboard window and all functions related to the window. this also
 * controlls all of the games graphics
 * 
 * @author okaad_000
 * @author HKNormann
 */
public class GameBoardController {

    /**
     * Player rolling dice enumerator.
     */
    public static final int ROLL = 0;
    /**
     * This player is moving enumerator.
     */
    public static final int MOVE = 1;
    /**
     * This player is waiting enumerator.
     */
    public static final int WAIT = 2;

    /**
     * unique game identification
     */
    private String gameID = "";
    private Client owner; // reference to client
    private ChatController gameChatController; // game chat controller
    private static final Logger LOGGER = Logger.getLogger(WindowController.class.getName());
    private static BoardInformation boardInformation = new BoardInformation();
    private Rectangle[][] playerPieces;
    private Vector<String> playerNames = new Vector<>();
    private boolean[] active = new boolean[4];
    private Label[] playerNameLabels = new Label[4];
    private ImageView[] playerActiveImages = new ImageView[4];
    private int playerNumber = -1;
    private int currentAction = WAIT;

    @FXML
    private AnchorPane anchor;

    @FXML
    private ImageView boardImage;

    @FXML
    private Label player1Name;

    @FXML
    private ImageView player1Active;

    @FXML
    private Label player2Name;

    @FXML
    private ImageView player2Active;

    @FXML
    private Label player3Name;

    @FXML
    private ImageView player3Active;

    @FXML
    private Label player4Name;

    @FXML
    private ImageView player4Active;

    @FXML
    private ImageView diceThrown;

    @FXML
    private Button throwTheDice;

    @FXML
    private VBox chatAnchor;

    /**
     * puts name labels and player images in arrays
     */
    @FXML
    private void initialize() {
        playerNameLabels[0] = player1Name;
        playerActiveImages[0] = player1Active;
        playerNameLabels[1] = player2Name;
        playerActiveImages[1] = player2Active;
        playerNameLabels[2] = player3Name;
        playerActiveImages[2] = player3Active;
        playerNameLabels[3] = player4Name;
        playerActiveImages[3] = player4Active;
    }

    /**
     * initializes a new game window
     * 
     * @param gameID
     *            unique id of new game
     * @param playerNames
     *            vector of player names in game
     * @param owner
     *            reference to client
     */
    public void initializeGame(String gameID, Vector<String> playerNames, Client owner) {
        this.setGameID(gameID);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"));
        loader.setResources(Client.getMessages());

        for (int i = 0; i < 4; i++) {
            active[i] = i <= playerNames.size();
        } // defaults empty player slots to inactive

        try {
            TitledPane chat = loader.load(); // adds chat to game window
            chat.setText(gameID + " chat");
            gameChatController = loader.getController();
            chat.setMaxSize(290, 490); // scale chat to fit anchor
            chatAnchor.getChildren().add(chat);
            chatAnchor.setMaxSize(290, 490);

            for (int i = 0; i < playerNames.size(); i++) {
                playerNameLabels[i].setText(playerNames.elementAt(i));
            }

        } catch (IOException e) {
            LOGGER.warning(e.getMessage());
        }

        this.owner = owner;
        gameChatController.setOwner(owner);
        owner.getWindowController().chatControllers.addElement(gameChatController);

        this.playerNames = playerNames;
        playerPieces = new Rectangle[4][4];

        // creates player pieces and adds on clicked event

        for (int player = 0; player < 4; player++) {
            for (int piece = 0; piece < 4; piece++) {
                final int x = player;
                final int y = piece;
                playerPieces[player][piece] = new Rectangle(48, 48);
                playerPieces[player][piece].setFill(new ImagePattern(boardInformation.playerPieceImages[player]));
                playerPieces[player][piece].setX(boardInformation.getCorners().getPoints()[player * 4 + piece].getX());
                playerPieces[player][piece].setY(boardInformation.getCorners().getPoints()[player * 4 + piece].getY());
                playerPieces[player][piece].setOnMouseClicked(e -> clickOnPiece(x, y));
                anchor.getChildren().add(playerPieces[player][piece]);
            }
        }

        int iterator = 0;

        for (String name : playerNames) {
            if (name.equals(owner.getUsername())) {
                playerNumber = iterator;
            }
            iterator++;
        }
    }

    /**
     * requests to throw the die
     * 
     * @param event
     */
    @FXML
    void throwDice(ActionEvent event) {
        String throwRequestMessage = "Game:Request:" + getGameID() + ":Throw";
        owner.sendMessage(throwRequestMessage);
    }

    /**
     * sets the die image
     * 
     * @param dice
     */
    public void setDice(int dice) {
        diceThrown.setImage(boardInformation.diceImages[dice]);
    }

    /**
     * sends a move request if you have been requested to move
     * 
     * @param player
     *            number of player owning piece clicked on
     * @param piece
     *            number of piece
     */
    private void clickOnPiece(int player, int piece) {
        if (player == playerNumber && currentAction == MOVE) {
            owner.sendMessage("Game:Request:" + getGameID() + ":Move:" + player + ":" + piece);
        }
    }

    /**
     * Sets status of self based on what server has requested self to do update
     * based on who is the requested player
     * 
     * @param string
     *            new status, expected format "Roll" or "Move" and active player
     *            number
     */
    public void handleRequest(String string) {
        String[] strings = string.split(":");
        String type = strings[0];
        int activePlayer = Integer.parseInt(strings[1]);

        for (int i = 0; i < 4; i++) {
            if (active[i]) {
                playerActiveImages[i].setVisible(i == activePlayer);
            }
        }

        if (activePlayer == playerNumber) {
            if ("Roll".equals(type)) {
                currentAction = ROLL;
                throwTheDice.setDisable(false);
            } else {
                currentAction = MOVE;
                throwTheDice.setDisable(true);
            }
        } else {
            currentAction = WAIT;
            throwTheDice.setDisable(true);
        }
    }

    /**
     * Handles game board update from server
     * 
     * @param string
     *            update data, expected to start with "Roll", "Move" or
     *            "Disconnect"
     */
    public void handleUpdate(String string) {
        String[] strings = string.split(":");
        String type = strings[0];

        switch (type) {
            case "Roll" :
                setDice(Integer.parseInt(strings[1]));
                break;
            case "Move" :
                int player = Integer.parseInt(strings[1]);
                int piece = Integer.parseInt(strings[2]);
                int to = Integer.parseInt(strings[3]);
                movePiece(player, piece, to);
                break;
            case "Disconnect" :
                handleDisconnect(strings[1]);
                break;
            default :
                LOGGER.warning("GameBoardController::HandleUpdate game: " + getGameID() + " recieved invalid message: " + string);
                break;
        }
    }

    /**
     * moves a piece on the playerboard
     * 
     * @param player
     *            player owning piece
     * @param piece
     *            number of piece
     * @param to
     *            position to move to
     */
    private void movePiece(int player, int piece, int to) {
        Rectangle playerPiece = playerPieces[player][piece];

        playerPiece.setX(boardInformation.getCorners().getPoints()[to].getX());
        playerPiece.setY(boardInformation.getCorners().getPoints()[to].getY());
    }

    /**
     * Handles a player disconnecting by representing it graphically and moving
     * all their pieces to their starting positions
     * 
     * @param playerNameString
     *            number of disconnected player
     */
    public void handleDisconnect(String playerNameString) {
        int disconnectNumber = -1;
        for (int i = 0; i < playerNames.size(); i++) {
            if (playerNames.elementAt(i).equals(playerNameString)) {
                disconnectNumber = i;
            }
        }

        for (int piece = 0; piece < 4; piece++) { // moves pieces to starting
                                                  // positions
            Rectangle pieceRectangle = playerPieces[disconnectNumber][piece];
            pieceRectangle.setX(boardInformation.getCorners().getPoints()[disconnectNumber * 4 + piece].getX());
            pieceRectangle.setY(boardInformation.getCorners().getPoints()[disconnectNumber * 4 + piece].getY());
        }
        gameChatController.addMessage("*System*", "Player: " + playerNames.elementAt(disconnectNumber) + " disconnected");
        playerActiveImages[disconnectNumber].setImage(boardInformation.disconnectedImage);
        playerActiveImages[disconnectNumber].setVisible(true);
        active[disconnectNumber] = false;
    }

    /**
     * sets a player to be the winnner and adds a message in the chat if the
     * local player is the winning player a dialogue will appear
     * 
     * @param winnerString
     *            name of winning player
     */
    public void handleWinner(String winnerString) {
        int winnerNumber = -1;
        for (int i = 0; i < playerNames.size(); i++) {
            if (playerNames.elementAt(i).equals(winnerString)) {
                winnerNumber = i;
            }
        }
        // sets a winning message in the chat
        String winningMessage = Client.getMessages().getString("ludogameboard.player");
        winningMessage += winnerString + Client.getMessages().getString("ludogameboard.isWinningPlayer");
        gameChatController.addMessage("*System*", winningMessage);

        if (winnerNumber == playerNumber) {
            owner.createDialog(Client.getMessages().getString("ludogameboard.congratulations"), Client.getMessages().getString("ludogameboard.youWon"));
        }
        playerActiveImages[winnerNumber].setImage(boardInformation.winnerImage);
        playerActiveImages[winnerNumber].setVisible(true);
        active[winnerNumber] = false;
    }

    public String getGameID() {
        return gameID;
    }

    public void setGameID(String gameID) {
        this.gameID = gameID;
    }
}