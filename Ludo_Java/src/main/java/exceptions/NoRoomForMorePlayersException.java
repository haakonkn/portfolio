/**
 * 
 */
package exceptions;

/**
 * Throws an exception when there is no more room for players.
 * 
 * @author okaad_000
 *
 */
public class NoRoomForMorePlayersException extends RuntimeException {

    public NoRoomForMorePlayersException() {
        super("No room for more players exception thrown!");
    }
}
