/**
 * 
 */
package exceptions;

/**
 * @author okaad_000
 *
 */
public class InvalidUsernameException extends Exception {

    public InvalidUsernameException() {
        super("Invalid username exception thrown!");
    }
}