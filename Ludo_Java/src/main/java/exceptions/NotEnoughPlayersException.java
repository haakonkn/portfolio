/**
 * 
 */
package exceptions;

/**
 * Throws an exception when there are not enough players to start a game.
 * 
 * @author okaad_000
 *
 */
public class NotEnoughPlayersException extends RuntimeException {

    public NotEnoughPlayersException() {
        super("Not enough players exception thrown!");
    }
}
