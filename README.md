**Portfolio repository for Håkon Kleppe Normann**

I dette repositoriet er det diverse kode og prosjekter jeg har jobbet med.
Noen av disse er gruppeprosjekter, og andre solo prosjekter.
Alle er beskrevet nedenfor.

Prosjektene er listet alfabetisk.

---

## CBT_Android_Java

Dette var et gruppeprosjekt hvor jeg var ett av 4 medlemmer. 

Applikasjon skrevet med Java i android studio.
Applikasjonen er en prototyp, og ment å være et hjelpemiddel i Cognitiv Behavioural Therapy.
Brukere kan lage kort for å plotte ned tanker, situasjoner, konsekvenser og mot-tanker. 
Appen lar brukerne logge inn med google og lagre dataen sin opp mot google firebase.
Applikasjonen lar også brukere se andres kort og filtrere disse.

---

## Ludo_Java

Denne applikasjonen ble laget i samarbeid med én annen student.
Dette var hovedprosjektet i Applikasjonsutviklings i 3. året på NTNU Gjøvik og var ment for 3-6 medlemmer.
Applikasjonen og prosjektet ble fullført med karakteren A. 

Applikasjonen har server og klient programmer som er skrevet i Java.
All kommunikasjon og protokoller mellom disse er laget fra bunnen av.
Programmet lar brukere koble seg opp mot ludo servere og chat servere og spille sammen.
Her kan de også invitere andre brukere til å spille over nett.
Brukerene logger seg inn / oppretter kontoer for å kunne spille.
Alt av chat og spill blir loggført på server.

---

## MovieDB_Java

Denne applikasjonen ble laget alene.
Dette var eksamen i Applikasjonsutviklings i 3. året på NTNU Gjøvik.
Oppgaven hadde en 36 timers frist på å bli fullført. 

Applikasjonen har en JSON leser laget fra bunnen av, og henter JSON dokumenter fra TheMovieDB.com
Den lar brukere søke på filmer og skuespillere, og viser relaterte treff i et eget vindu.
Om brukeren trykker på filmer, kommer alle skuespillere i filmen i en dropdown.
Beskrivelse av filmen og bilder fra cover blir hentet og vist i et vindu.
All data blir cashet så liknende søkeresultater hentes lokalt i etterkant.
Denne cachen kan vises størrelsen på og tømmes. 

---

## Ooprg_c++

Denne applikasjonen ble laget i gruppe på 3.
Dette var hovedoppgaven i Objekt orientert programmering i 2. året på NTNU Gjøvik.

Programmet kjører i kommando vindu, og lar brukeren skrive inn og vise data i olympiske leker.
Brukeren kan skrive inn rekorder, land, konkurranser, deltagere, medaljer, poeng og mye mer.
Disse bindes opp mot hverandre og lagres i separate filer med egenlagd format.
Programmet er koblet opp mot et Liste-API laget av en programmeringslærer på Gjøvik som en del av oppgaven.
Dette gjorde det mye vanskeligere enn om jeg kunne håndtert det selv, men lærte meg å forstå andres kode og bruke andres API'er.

