#ifdef _MSC_VER
 #define _CRT_SECURE_NO_WARNINGS
 #endif

#include <iostream>
#include <fstream>
#include "deltager.h"
#include "funksjoner.h"

using namespace std;


Deltager::Deltager()
{
	cout << "ERROR - SKAL IKKE FOREKOMME!\n";
}

Deltager::Deltager(int id, char* L) : NumElement(id)		// Leser inn data for ny deltager:
{
	char buffer[STRLEN];
	land = L;
	kj�nn = (lesNum("Er deltager mann(0), kvinne(1): ", 0, 1)) ? Kvinne : Mann;

	les("Deltagers navn: ", buffer, STRLEN);
	navn = new char[strlen(buffer) + 1];
	strcpy(navn, buffer);
	les("annen data: ", buffer, STRLEN);
	data = new char[strlen(buffer) + 1];
	strcpy(data, buffer);
}

Deltager::~Deltager()			
{
	delete[] land;
	delete[] navn;
}

void Deltager::skrivInfo()		// Skriver tropp/alle deltagere:
{
	cout << "\nID: " << number
		<< "\nNavn: " << navn
		<< "\nKjonn: ";

	(kj�nn) ? cout << "Kvinne" : cout << "Mann";
	cout << endl;
}

void Deltager::display()		// Skriver data om en spesifikk deltager:
{
	cout << "\nID: " << number
		<< "\nNavn: " << navn
		<< "\nLand: " << land
		<< "\nKjonn: ";
	(kj�nn) ? cout << "Kvinne" : cout << "Mann";
	cout << "\nAnnen data: " << data << endl;
}

void Deltager::display2()		// Skriver data for resultater:
{
	printf("\nID: %d \t Land: %s \t Navn: %s", number, land, navn);
}

void Deltager::endreData(Nasjoner* nasjoner)
{
	char buffer[STRLEN];
	char* nasjonId;
	cout << "\nTidligere data:";
	display();

	les("\nDeltagers nye navn: ", buffer, STRLEN);		// Bruker endrer navn:
	navn = new char[strlen(buffer) + 1];
	strcpy(navn, buffer);

	les("\nAnnen data om deltager: ", buffer, STRLEN);
	data = new char[strlen(buffer) + 1];
	strcpy(data, buffer);

	do										// Looper til bruker skriver gyldig nasjon:
	{
		nasjonId = lesId();
	} while (!(nasjoner->iLista(nasjonId)));
	land = new char[strlen(nasjonId) + 1];
	strcpy(land, nasjonId);

											// Bruker skriver kj�nn:
	kj�nn = (lesNum("\nDeltagers nye kjonn er mann(0), kvinne(1): ", Mann, Kvinne)) ? Kvinne : Mann;
}

void Deltager::skrivTilFilen(std::ofstream & ut)
{
	ut << number << '\n' << land << '\n'
	   << navn << '\n' << kj�nn << '\n'
		<< data << endl;
}

Deltager::Deltager(std::ifstream & inn, int id) : NumElement(id)
{
	int temp;
	char buffer[STRLEN];

	inn.getline(buffer, STRLEN);		// Leser inn land:
	land = new char[strlen(buffer) + 1];
	strcpy(land, buffer);

	inn.getline(buffer, STRLEN);		// Leser inn navn:
	navn = new char[strlen(buffer) + 1];
	strcpy(navn, buffer);

	inn >> temp; inn.ignore();			// Leser inn kj�nn
	kj�nn = (temp) ? Kvinne : Mann;

	inn.getline(buffer, STRLEN);
	data = new char[strlen(buffer) + 1];
	strcpy(data, buffer);

}

char* Deltager::getNavn()
{
	return navn;
}

char* Deltager::getNasjon()
{
	return land;
}

int Deltager::getId()
{
	return number;
}