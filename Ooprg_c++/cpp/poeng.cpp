#ifdef _MSC_VER						//S� vi kan kj�re med strcpy
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "poeng.h"

Poeng::Poeng()
{
	antallNasjoner = 0;
}

void Poeng::display()
{
	if (antallNasjoner > 0)		//sjekker om det finnes nasjoner
	{
		std::cout << "\n\n\t++Poengoversikt++" << std::endl;
		for (int i = 0; i < antallNasjoner; i++)			//g�r gjennom alle
		{
			printf("\nNasjon: %s har %d poeng",		//skriver ut poeng
				nasjon[i], poeng[i]);
		}
		std::cout << std::endl;
	}
	else
		std::cout << "\nIngen poeng registrert" << std::endl;		//gir feilmelding om tom
}

void Poeng::oppdater(char* forkortelse, int poeng, bool fjern)
{
	int nr = nyNasjon(forkortelse);		//lager ny nasjon eller finner gammel

	if (fjern)
		this->poeng[nr] -= poeng;				//oppdaterer poengsum
	else
		this->poeng[nr] += poeng;

	sorterElementer(0, antallNasjoner - 1);							//sorterer elementene
	skrivTilFil();
}

void Poeng::sorterElementer(int start, int slutt)				//sorterer poeng og tilh�rende nasjoner med quicksort
{																					// quickosrt er forklart i dybden i medaljer.cpp
	int poengTemp;
	char nasjonTemp[NASJONID];

	int pivot, j, i;

	if (start < slutt)
	{
		pivot = start;
		i = start; j = slutt;

		while (i < j)
		{
			while ((poeng[i] <= poeng[pivot]) && (i < slutt))
				i++;

			while (poeng[j] > poeng[pivot])
				j--;

			if (i < j)
			{
				poengTemp = poeng[i];
				strcpy(nasjonTemp, nasjon[i]);

				poeng[i] = poeng[j];
				strcpy(nasjon[i], nasjon[j]);

				poeng[j] = poengTemp;
				strcpy(nasjon[j], nasjonTemp);
			}
		}

		strcpy(nasjonTemp, nasjon[pivot]);
		poengTemp = poeng[pivot];

		poeng[pivot] = poeng[j];
		strcpy(nasjon[pivot], nasjon[j]);

		poeng[j] = poengTemp;
		strcpy(nasjon[j], nasjonTemp);

		sorterElementer(start, j - 1);
		sorterElementer(j + 1, slutt);


		
		i = 0; j = antallNasjoner - 1;			//inverterer til synkende rekkef�lge
		while (i < j)
		{
			poengTemp = poeng[i];
			strcpy(nasjonTemp, nasjon[i]);

			poeng[i] = poeng[j];
			strcpy(nasjon[i], nasjon[j]);

			poeng[j] = poengTemp;
			strcpy(nasjon[j], nasjonTemp);
			i++; j--;
		}
	}
}

void Poeng::skrivTilFil()		//skriver poeng til fil
{
	std::ofstream poengfil("poeng.dta");

	poengfil << antallNasjoner << std::endl;

	for (int i = 0; i < antallNasjoner; i++)
	{
		poengfil << nasjon[i] << std::endl;
		poengfil << poeng[i] << std::endl;
	}
}

void Poeng::lesFraFil()		//leser poeng fra fil
{
	std::ifstream poengfil("poeng.dta");
	char navn[NASJONID];
	int nNasjoner;


	if (!poengfil)
	{
		std::cout << "\n\tpoeng.dta ikke funnet";
	}
	else
	{			//om den �pner filen
		poengfil >> nNasjoner; poengfil.ignore();
		std::cout << "\n\tpoeng.dta laster inn ";
		int i = 0;
		
		for (int i = 0; i < nNasjoner; i++)		//henter alle nasjonene med poeng
		{
			std::cout << ".";

			poengfil.getline(navn, NASJONID);
			int n = nyNasjon(navn);

			poengfil >> poeng[n]; poengfil.ignore();
		}

		sorterElementer(0, antallNasjoner - 1);		//passer p� at de er sorterte
	}
	
}


int Poeng::nyNasjon(char* forkortelse)			//legger til ny nasjonsforkortelse eller returnerer nummer til eksisterende
{
	for (int i = 0; i < antallNasjoner; i++)
	{
		if (strcmp(forkortelse, nasjon[i]) == 0)		//om den blir funnet
			return i;												// returnerer dets nummer

	}																	//hvis ikke
	nasjon[antallNasjoner] = new char[NASJONID];
	strcpy(this->nasjon[antallNasjoner], forkortelse);	//gir ny nasjon navn

	poeng[antallNasjoner] = 0;				//initialiserer verdi til ny nasjon
	antallNasjoner++;

	return (antallNasjoner - 1);		//returnerer nummer i array p� nyopprettet nasjon
}
