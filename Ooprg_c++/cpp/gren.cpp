#ifdef _MSC_VER						//S� vi kan kj�re med strcpy
#define _CRT_SECURE_NO_WARNINGS
#endif


#include <iostream>
#include <fstream>
#include <string>			//Brukes i fjernOvelse.
#include "funksjoner.h"
#include "gren.h"
#include "const.h"
#include "ovelse.h"

using namespace std;


Gren::Gren(char* navn) : TextElement(navn)
{
	ovelser = new List(Sorted);
	char buffer[STRLEN];

	cout << "\n\tHvordan skal prestasjoner maales?\n"
		<< "1 = tideler\n"
		<< "2 = hundredeler\n"
		<< "3 = tusendeler\n"
		<< "4 = x.poeng / hopp\n"
		<< "5 = xx.poeng / kunstlop\n";
	regType = lesNum("Tall: ", tideler, kunstlop);
}

//-----------------------------------------------------------------------------

Gren::~Gren()
{ delete ovelser; }

//----------------------------------------------------------------------------
//Endre data p� gren.
void Gren::endreData()
{
	char buffer[STRLEN];
	les("\nNytt navn pa Gren: ", buffer, STRLEN);	
	text = new char[strlen(buffer) + 1];
	strcpy(text, buffer);
}

//------------------------------------------------------------------------------

void Gren::skrivFullData()
{
	Ovelse* ovelse;
	cout << "\n\t++" << text << "++" << endl;		//Skriver ut data om ovelser.

	int antallOvelser = ovelser->noOfElements();
	cout << "\nAntall ovelser i grenen: " << antallOvelser << endl;

	for (int i = 1; i <= antallOvelser; i++)
	{
		ovelse = (Ovelse*)ovelser->removeNo(i);
		ovelse->skrivHovedData();
		ovelser->add(ovelse);
	}
	cout << endl;
}

//------------------------------------------------------------------------------

void Gren::skrivHovedData()	
{
	cout << "\n\t++" << text << "++"
		<< "\nPrestasjoner maales i formatet: ";

	switch (regType)		//Sjekker hvilke type poengformat grenen har.
	{
	case tideler:		cout << "timer, minutt, tideler";		break;
	case hundredeler:	cout << "timer, minutt, hundredeler";	break;
	case tusendeler:	cout << "timer, minutt, tusendeler";	break;
	case hopp:			cout << "poeng x";						break;
	case kunstlop:		cout << "poeng xx";						break;
	}

	int antallOvelser = ovelser->noOfElements();
	cout << "\nAntall ovelser i grenen: " << antallOvelser << endl;
}

//-------------------------------------------------------------------------------

void Gren::skrivTilFil(ofstream& grenFil)
{
	int antallOvelser = ovelser->noOfElements();

	grenFil << text << endl;				//Skriver inn data om grener og
	grenFil << antallOvelser << endl;		//ovelser til fil.
	grenFil << regType << endl;


	Ovelse* ovelse;

	for (int i = 1; i <= antallOvelser; i++)
	{
		ovelse = (Ovelse*)ovelser->removeNo(i);
		ovelse->skrivTilFil(grenFil);
		ovelser->add(ovelse);
	}
}

//------------------------------------------------------------------------------

Gren::Gren(std::ifstream& grenFil, char* navn) : TextElement(navn)
{
	int ID;
	Ovelse* ovelse;
	int antallOvelser;
	ovelser = new List(Sorted);

													//Henter data om grener 
	grenFil >> antallOvelser; grenFil.ignore();		//og ovelser fra fil.
	grenFil >> regType; grenFil.ignore();

	for (int i = 1; i <= antallOvelser; i++)
	{
		grenFil >> ID; grenFil.ignore();
		ovelse = new Ovelse(grenFil, ID);
		ovelser->add(ovelse);
	}
}

//-------------------------------------------------------------------------------

void Gren::startListe(Deltagere* deltagere, int* antOvelser)
{
	Ovelse * ovelse;
	int ovelsesNr;
	if (ovelser->noOfElements() >= 1)		//wDet m� finnes en �velse for at startliste
	{										//skal kunne lages.
		do
		{									//Leser inn eksisterende ovelse.
			ovelsesNr = lesNum("\nVelg ovelse (ID):", 1000, 999 + *antOvelser);
		} while (!(ovelse = (Ovelse*)ovelser->remove(ovelsesNr)));

		ovelse->startListe(deltagere);
		ovelser->add(ovelse);
	}
	else
	{
		cout << "Finnes ingen ovelser innenfor denne grenen!\n\n";
	}
}

//------------------------------------------------------------------------------

void Gren::ovelseMeny(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, int* antOvelser)
{
	char valg;
	do
	{
		cout << "\n\n\t++Ovelse Meny++\n"
			<< "N - ny ovelse\n"
			<< "E - endre ovelse\n"
			<< "F - Slett ovelse\n"
			<< "A - Skriv alle ovelser\n"
			<< "L - Startlistemeny\n"
			<< "R - Resultatmeny\n"
			<< "U - Ut av meny\n";

		valg = les();
		valg = toupper(valg);

		switch (valg)
		{
		case 'N': nyOvelse(antOvelser); break;
		case 'E': endreOvelse(antOvelser);  break;
		case 'F': fjernOvelse(medaljer, poeng, deltagere, antOvelser); break;
		case 'A': skrivOvelsedata(); break;
		case 'L': startListe(deltagere, antOvelser); break;
		case 'R': resultatListeMeny(medaljer, poeng, deltagere, (RegType)regType); break;
		case 'U': break;
		default: cout << "\n" << valg << " er en ugyldig kommando!" << endl;
		}
	} while (valg != 'U');
}

//-------------------------------------------------------------------------------

void Gren::nyOvelse(int* antOvelse)
{
	Ovelse* ny;
	char* navn;
	char buffer[STRLEN];
	int id = 1000;
	int nr = ovelser->noOfElements();

	if (nr < MAXOVELSE)									//F�r lage ny s� lenge nr
	{													//er mindre en MAXOVELSE.
		do
		{
			les("\nOvelsens navn: ", buffer, STRLEN);		//Leser inn navn.
			navn = new char[strlen(buffer) + 1];			//helt til det skrives et
			strcpy(navn, buffer);							//ikke-eksisterende navn.
		} while (navnEksisterer(navn));

		id += *antOvelse;						//Autogenererer ovelsesID.
		ny = new Ovelse(id, navn);				//Sender med det som id til den nye grenen.
		ovelser->add(ny);
		*antOvelse += 1;						//Plusser p� for � �ke med en til
	}											//da man lager neste ovelse.
	else
	{
		cout << "\n\nIkke plass til flere ovelser\n";
	}


}

//-------------------------------------------------------------------------------

bool Gren::navnEksisterer(char* nvn)
{
	bool lik = false;
	Ovelse* sjekk;
	int nr = ovelser->noOfElements();
	for (int i = 1; i <= nr; i++)				//G�r for s� mange ovelser som det er i lista.
	{
		sjekk = (Ovelse*)ovelser->removeNo(i);
		if (strcmp(nvn, sjekk->getNavn()) == 0)	//Sjekker om navnet som er sendt med er lik
		{										//noen av de allerede lagrede 
			lik = true;							//navnene.
		}
		ovelser->add(sjekk);					//Putter tilbake.
	}
	return lik;									//Returnerer ture eller false.
}

//----------------------------------------------------------------------------------

void Gren::endreOvelse(int* antOvelser)
{
	Ovelse* endre;
	char* navn;
	char buffer[STRLEN], valg;
	int id;
	do
	{													//Sjekker at ovelsesId finnes.
		id = lesNum("Ovelsens IDnr: ", 1000, 999 + *antOvelser); 
	} while (!ovelser->inList(id));

	
	do
	{
		les("\nOvelsens navn: ", buffer, STRLEN);		//Leser inn navn
		navn = new char[strlen(buffer) + 1];			//p� �velsen og sjekker
		strcpy(navn, buffer);							//om det eksisterer fra f�r.
	} while (navnEksisterer(navn));
	

	endre = (Ovelse*)ovelser->remove(id);				//Henter.
	endre->endreData(navn);								//Endrer.
	ovelser->add(endre);								//Legger tilbake.
}

//--------------------------------------------------------------------------------------

void Gren::skrivOvelsedata()
{
	if (ovelser->noOfElements() >= 1)	//sjekker om det er registrert noen ovelser
	{
		Ovelse* ovelse;
		int antall;
		antall = ovelser->noOfElements();

		for (int i = 1; i <= antall; i++)
		{
			ovelse = (Ovelse*)ovelser->removeNo(i);		//henter ovelse fra liste
			ovelse->skrivHovedData();					//skriver ut data
			ovelser->add(ovelse);						//legger tilbake til liste
		}
	}
	else
		cout << "\nError, ingen ovelser registrert!\n";
}

//---------------------------------------------------------------------------------

void Gren::resultatListeMeny(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, RegType regtype)
{
	int n = ovelser->noOfElements();

	if (n > 0)												//Sjekker om det eksisterer noen �velser.
	{
		int ovelsesId = 0;
		do {												//Leser inn ovelsesId.
			ovelsesId = lesNum("Ovelsens IDnr: ", MINOVELSE, MINOVELSE * 10);		//optill 9000 �velser totalt, men kun 20 i hver gren
			if (!ovelser->inList(ovelsesId))
			{
				printf("\n\tError ID'en %s ikke i ovelsesliste!\n", ovelsesId);
			}
		} while (!ovelser->inList(ovelsesId));
		Ovelse* ovelse = (Ovelse*)ovelser->remove(ovelsesId);

		//--------------ResultatListe-meny----------------//

		char valg;
		do
		{
			cout << "\n\n\t++ResultatListe-Meny++\n"
				<< "S - Skriv resultatliste\n"
				<< "N - Ny resultatliste\n"
				<< "F - Fjern resultatliste\n"
				<< "U - Ut av meny\n";

			valg = les();
			valg = toupper(valg);

			switch (valg)
			{
			case 'S': ovelse->skrivResultatliste(medaljer, poeng, deltagere, regtype);  break;
			case 'N': ovelse->nyResultatliste(medaljer, poeng, deltagere, regtype);  break;
			case 'F': ovelse->slettResultatliste(medaljer, poeng, deltagere);  break;
			case 'U': break;
			default: cout << "\n" << valg << " er en ugyldig kommando!" << endl;
			}
		} while (valg != 'U');

		//--------------------------//

		ovelser->add(ovelse);
	}
	else cout << "\n\tError, Ingen ovelser registrert!" << endl;

}

//-----------------------------------------------------------------------------------------------

void Gren::fjernOvelse(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, int* antOvelser)
{
	Ovelse* fjern;
	char* num;							//Brukes for og slette en startliste.
	char buffer[STRLEN];
	int id;
	//Passer p� at ovelsen finnes.		
	id = lesNum("Ovelsens IDnr: ", MINOVELSE, MINOVELSE * 10);

	if (ovelser->inList(id))
	{
		string resultatnavn = "OV" + to_string(id) + ".RES";		//Brukes for � sjekke om RES fila finnes.
		string startnavn = "OV" + to_string(id) + ".STA";			//Gj�r om id til char* for � bruke i fjernStartliste.

		ifstream resultatfil(resultatnavn);

		fjern = (Ovelse*)ovelser->remove(id);
		if (!resultatfil)											//Om .RES ikke finnes.
		{
			fjern->fjernStartliste(startnavn);						//Sletter Startliste.
			delete fjern;											//Sletter ovelse.
		}
		else
		{
			resultatfil.close();									//Lukker fila.
			fjern->slettResultatliste(medaljer, poeng, deltagere);	//Sletter resultatliste og oppdaterer statistikk.
			fjern->fjernStartliste(startnavn);						//Deretter slettes startliste.
			delete fjern;											//Ogs� slettes ovelsen.
		}
	}
	else cout << "\n\tOvelsen " << id << " eksisterer ikke!" << endl;
}