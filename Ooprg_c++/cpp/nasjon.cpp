#ifdef _MSC_VER						//S� vi kan kj�re med strcpy
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <fstream>
#include "nasjon.h"
#include "funksjoner.h"
#include "nasjoner.h"
#include "const.h"

using namespace std;

//---------------------------------------------------------------------------------------------------------------

Nasjon::Nasjon()
{
	cout << "\nSkal ikke forekomme!\n";
}

//---------------------------------------------------------------------------------------------------------------

//Constructoren som skal brukes.
Nasjon::Nasjon(char* text)									//NasjonId'en sendes med.
	: TextElement(text)
{
	char buffer[STRLEN];

	antDeltakere = 0;										//Ingen deltakere enda.

	les("\nNasjonens fulle navn: ", buffer, STRLEN);		//Leser inn Landnavn.
	navn = new char[strlen(buffer) + 1];
	strcpy(navn, buffer);

	les("\nNasjonens kontaktperson: ", buffer, STRLEN);		//Leser inn kontaktperson.
	kontaktNavn = new char[strlen(buffer) + 1];
	strcpy(kontaktNavn, buffer);

	tlf = lesNum("\nTLF: ", MINTLF, MAXTLF);				//Kontaktpersons tlf.

	les("\nEn liten mengde data om landet: ", buffer, STRLEN);
	data = new char[strlen(buffer) + 1];					//Leser inn en liten mengde data.
	strcpy(data, buffer);
}

//---------------------------------------------------------------------------------------------------------
//Deconstructor
Nasjon::~Nasjon()
{
	delete[] navn;
	delete[] kontaktNavn;
	delete[] data;
}
//---------------------------------------------------------------------------------------------------------

void Nasjon::display()
{
	cout << "\n\t++" << navn << "++";									//Sier seg selv, displayer
	cout << "\nNasjonsID: " << text;									//data.
	cout << "\nKontaktperson: " << kontaktNavn;
	cout << "\nKontaktpersons tlf: " << tlf;
	cout << "\nAntall deltakere fra nasjon: " << antDeltakere;
	cout << "\nData om nasjon:\n" << data << '\n';

}

//---------------------------------------------------------------------------------------

void Nasjon::endreData()
{
	char valg;
	char buffer[STRLEN];

	cout << "\nVil du endre nasjonens navn(j/n)\n";				//Lar bruker velge om han vil endre den spesifike
	valg = les();												//dataen eller ikke.
	if (valg == 'J')
	{
		les("\nNasjonens fulle navn: ", buffer, STRLEN);		//Leser inn Landnavn.
		navn = new char[strlen(buffer) + 1];
		strcpy(navn, buffer);
	}
	valg = 'O';
	cout << "\nVil du endre nasjonens kontaktperson(j/n)\n";
	valg = les();
	if (valg == 'J')
	{
		les("\nNasjonens kontaktperson: ", buffer, STRLEN);		//Leser inn kontaktperson.
		kontaktNavn = new char[strlen(buffer) + 1];
		strcpy(kontaktNavn, buffer);
	}
	valg = 'O';
	cout << "\nVil du endre kontaktpersons tlfnr(j/n)\n";
	valg = les();
	if (valg == 'J')
	{
		tlf = 0;
		tlf = lesNum("\nTLF: ", MINTLF, MAXTLF);				//Kontaktpersons tlf.
	}
	valg = 'O';
	cout << "\nVil du endre data om landet(j/n)\n";
	valg = les();
	if (valg == 'J')
	{
		les("\nEn liten mengde data om landet: ", buffer, STRLEN);
		data = new char[strlen(buffer) + 1];					//En liten mengde data om landet.
		strcpy(data, buffer);
	}
}

//----------------------------------------------------------------------------------------------

void Nasjon::visHovedData()
{
	cout << "\n\t++" << navn << "++";							//Displayer kun hoveddataen
	cout << "\nNasjonsID: " << text;							//om nasjonen.
	cout << "\nAntall i troppen: " << antDeltakere << '\n';
}

//---------------------------------------------------------------------------------------------

void Nasjon::skrivTilFilen(ofstream & ut)
{
	ut << text << endl;
	ut << antDeltakere << endl;									//Skriver all nasjonens data til fil.
	ut << navn << endl;
	ut << kontaktNavn << endl;
	ut << tlf << endl;
	ut << data << endl;
}

//---------------------------------------------------------------------------------------------

Nasjon::Nasjon(std::ifstream & inn, char* text) : TextElement(text)
{
	char buffer[STRLEN];

	inn >> antDeltakere; inn.ignore();

	inn.getline(buffer, STRLEN);
	navn = new char[strlen(buffer) + 1];						//Leser all nasjonens data fra fil.
	strcpy(navn, buffer);


	inn.getline(buffer, STRLEN);
	kontaktNavn = new char[strlen(buffer) + 1];
	strcpy(kontaktNavn, buffer);

	inn >> tlf; inn.ignore();

	inn.getline(buffer, STRLEN);
	data = new char[strlen(buffer) + 1];
	strcpy(data, buffer);
}

//--------------------------------------------------------------------------

char* Nasjon::getNavn()
{
	return text;												//Henter nasjonNavn.
}

//--------------------------------------------------------------------------

int Nasjon::getAntDeltakere()
{
	return antDeltakere;										//Henter antDeltakere.
}

//----------------------------------------------------------------------------

void Nasjon::leggTilDeltager()
{
	antDeltakere++;												//Legger til en ny deltaker
}																//i antDeltaker.