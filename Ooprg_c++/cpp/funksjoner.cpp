#ifdef _MSC_VER						//S� vi kan kj�re med strcpy
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <cctype>		//Toupper.
#include <cstdlib>		//atoi
#include "funksjoner.h"	
#include "const.h"


using namespace std;

//---------------------------------------------------------------------------
//  Leser inn en ikke-blank tekst.
void les(const char t[], char s[], const int LEN)
{
	cout << '\t' << t;			//  Skriver ledetekst.
	cin.getline(s, LEN);        //  Leser inn tekst.
}

//---------------------------------------------------------------------------
//Leser inn ett uppercasetegn.
char les()
{
	char ch;
	cout << "\nKommando: ";				//Ganske rett frem, henter ett tegn
	cin >> ch;							//returnerer den som stor bokstav.
	cin.ignore();
	cout << endl;
	return toupper(ch);
}

//---------------------------------------------------------------------------
//Hovedmenyen.
void skrivMeny()
{
	cout << "\n\tMENY:\n"
		<< "N = Nasjonmeny\n"
		<< "D = Deltagermeny\n"	
		<< "G = Grenmeny\n"
		<< "M = Medaljeoversikt\n"
		<< "P = poengoversikt\n"
		<< "Q = quit\n";
}

//-----------------------------------------------------------------------------
//Leser inn et tall mellom min og max.
int lesNum(const char t[], const int MIN, const int MAX ) 
{
	int nr;
	char buffer[STRLEN];
	char* num;
	
	do {
		bool valid;									//Setter opp en bool.
		do
		{
			valid = true;							//Setter bool som sann.
			les(t, buffer, STRLEN);					//Leser inn nummer som en char*.
			num = new char[strlen(buffer) + 1];
			strcpy(num, buffer);
			for (int i = 0; i < strlen(num); ++i)
			{
				if (!isdigit(num[i]))				//G�r gjennom alle tegn i num
				{									//og sjekker om de er ett tall
					valid = false;					//om ikke, returner false.
				}
			}
		} while (valid != true);					//Gj�r p� nytt om ikke alle er tall

		nr = atoi(num);								//Gj�r om til int.
	} while (nr < MIN || nr > MAX);
	return nr;										//Returner int'en.
}

//-------------------------------------------------------------------------------
//lese en nasjon id.
char* lesId()
{
	char buffer[NASJONID];
	char* nasjonid;

	les("\nNasjonens trebokstavs nasjonID: ", buffer, NASJONID);
	nasjonid = new char[strlen(buffer) + 1];		//Leser inn nasjonId.
	strcpy(nasjonid, buffer);
	
	for (int i = 0; i < NASJONID; i++)
	{												//G�r gjennom bokstavene i 
		nasjonid[i] = toupper(nasjonid[i]);			//nasjonId og gj�r de toUpper.
	}
	return nasjonid;
}

//-----------------------------------------------------------------------------
//Leser inn tall basert p� hva slags type �velse det er
int lesRegtype(RegType regtype)
{
	int num;
	int min, sec, less;
	string mm, ss, ls;
	string poeng;
	char buffer[10];

	if (regtype == hopp)								//henter forksjellige typer data
		num = lesNum(" Poeng(XX) : ", 0, 99);	
	else if (regtype == kunstlop)
		num = lesNum(" Poeng(X) : ", 0, 9);
	else
	{
		min = lesNum("\nMin (mm): ", 0, 59);		//henter antall minutter
		if (min < 10)
			mm += "0";					//om lavere enn 10 legges 0 foran tallet

		_itoa(min, buffer, 10);			//tallet konverteres til array
		mm += buffer;						//og legges til string

		sec = lesNum("Sec (ss): ", 0, 59);		//sekund hentes
		if (sec < 10)
			ss += "0";

		_itoa(sec, buffer, 10);
		ss += buffer;

		if (regtype == tideler)		//om det er tideler
		{
			less = lesNum("Tideler (x): ", 0, 9);		//henter tidel

			_itoa(less, buffer, 10);
			ls += buffer;						//legges i ls
		}	
		else if (regtype == hundredeler)		//om det er hundredeler
		{
			less = lesNum("Hundredeler (xx): ", 0, 99);		//henter hundredel
			if (less < 10)				//legger 0 foran om lavere enn 10
				ls += "0";

			_itoa(less, buffer, 10);
			ls += buffer;
		}
		else if (regtype == tusendeler)		//om tusendeler
		{
			less = lesNum("Tusendeler (xxx): ", 0, 999);		//henter tusendel
			if (less < 10)			//legger tll to 0'er om under 10
				ls += "00";
			else if (less < 100)		// og 0 om under 100
				ls += "0";

			_itoa(less, buffer, 10);
			ls += buffer;
		}

		poeng = mm + ss + ls;			//legger sammen stringene til ett tall
		num = atoi(poeng.c_str());  //gj�r om tall til int og legger inn i num
	}
	return num;								//returnerer num
}

int lesDato()
{
	int dag, maaned, aar;
	char buffer[10];
	string dd, mm, aa;		
	
		dag = lesNum("\nDag: ", 1, 31);
		if (dag < 10)
			dd += "0";			//legger til 0 foran ensiffrede dag-nummere

		_itoa(dag, buffer, 10);		//konverterer tall til char array
		dd += buffer;					//legger tall til i dager string

		maaned = lesNum("\nMaaned: ", 1, 12);	//henter m�ned
		if (maaned < 10)
			mm += "0";

		_itoa(maaned, buffer, 10);
		mm += buffer;

		aar = lesNum("\nAar: ", 0, 99);		//henter �r 
		if (aar < 10)
			aa += "0";

		_itoa(aar, buffer, 10);
		aa += buffer;

		string dato = aa + mm + dd;			//legger sammen stringene

	return atoi(dato.c_str());								//Gj�r om til int ved retur.
}

