#include <iostream>
#include <fstream>
#include "funksjoner.h"
#include "grener.h"
#include "gren.h"
#include "const.h"
#include "deltagere.h"

using namespace std;

Grener::Grener()		//constructor
{	grener = new List(Sorted); }

Grener::~Grener()		//destructor
{	delete grener; }


void Grener::meny()
{
	char valg;

	do {
		cout << "\n\t++Gren Meny++\n"
			<< "N = registrer ny gren\n"
			<< "E = endre en gren\n"
			<< "A = skriv hoveddata om alle grener\n"
			<< "S = skriv all data om en gren\n"
			<< "U = avslutt meny" << endl;

		valg = les();			//henter brukervalg
		valg = toupper(valg);

		switch (valg) {
		case 'N': nyGren(); skrivTilFil(); break;
		case 'E': endreGren(); skrivTilFil(); break;
		case 'A':skrivHovedData(); break;
		case 'S':skrivFullData(); break;
		case 'U': break;
		default: cout << "\n" << valg << " er en ugyldig kommando!" << endl;
		}
	} while (valg != 'U');		//looper s� lenge brukeren ikke �nsker � avslutte
}

void Grener::nyGren()
{
	Gren* nyGren;
	char navn[STRLEN];

	les("\nNavn pa ny gren: ", navn, STRLEN);

	if (!grener->inList(navn))	//om gren ikke allerede eksisterer
	{
		nyGren = new Gren(navn);	//lager ny gren og legger til liste
		grener->add(nyGren);
		cout << "\nGren " << navn <<  " er registrert" << endl;
	}
	else				//om gren allerede eksisterer f�r brukeren feilmelding og sendes tilbake til meny
		cout << "\nGrenen er allerede registrert" << endl;
}


void Grener::endreGren()
{
	if (grener->noOfElements() >= 1)	//sjekker om det er registrert noen grener
	{
		Gren* gren;
		char navn[STRLEN];
		les("\nSkriv navn pa Gren: ", navn, STRLEN);

		if (!grener->inList(navn))	//om grenen ikke eksisterer
		{											// skrives feilmelding
			cout << "\nGrenen: " << navn << " finnes ikke!" << endl;
		}
		else							//om grenen eksisterte
		{
			gren = (Gren*)grener->remove(navn);
			gren->endreData();	//kan den bli forandret
			grener->add(gren);
		}
	}
	else
		cout << "\nError, ingen grener er registrert!" << endl;
}

void Grener::skrivFullData()
{
	if (grener->noOfElements() >= 1)
	{
		Gren* gren;
		char navn[STRLEN];

		les("\nNavn pa gren: ", navn, STRLEN);

		if (grener->inList(navn))	//om gren eksisterer
		{
			gren = (Gren*)grener->remove(navn);
			gren->skrivFullData();
			grener->add(gren);
		}
		else									//om gren ikke eksisterer
			cout << "\nGrenen " << navn << " er ikke registrert!" << endl;

	}
	else
		cout << "\nError, ingen grener er registrert!" << endl;
}

void Grener::skrivHovedData()
{
	if (grener->noOfElements() >= 1)	//sjekker om det er registrert noen grener
	{
		Gren* gren;
		int antallGrener = 0;
		antallGrener = grener->noOfElements();		
		for (int i = 1; i <= antallGrener; i++)	
		{
			gren = (Gren*)grener->removeNo(i);		//henter gren fra liste
			gren->skrivHovedData();						//skriver ut data
			grener->add(gren);								//legger tilbake til liste
		}
		cout << endl;
	}
	else
		cout << "\nError, ingen grener er registrert!" << endl;
}

void Grener::skrivTilFil()
{
	Gren* gren;
	ofstream grenFil("grener.dta");
	int antallGrener = 0;
	antallGrener = grener->noOfElements();

	grenFil << antOvelser << endl;
	grenFil << antallGrener << endl;

	for (int i = 1; i <= antallGrener; i++)
	{
		gren = (Gren*)grener->removeNo(i);
		gren->skrivTilFil(grenFil);
		grener->add(gren);
	}
}

void Grener::lesFraFil()
{
	int antallGrener;
	char buffer[STRLEN];
	Gren* gren;

	ifstream grenFil("grener.dta");

	if (grenFil)
	{
		cout << "\n\tgrener.dta laster inn ";

		grenFil >> antOvelser; grenFil.ignore();	//Henter antall ovelser.
		grenFil >> antallGrener; grenFil.ignore();	//henter antall

		for (int i = 1; i <= antallGrener; i++)		//henter data fra hver gren
		{
			cout << ".";
			grenFil.getline(buffer, STRLEN);
			gren = new Gren(grenFil, buffer);
			grener->add(gren);
		}

	}
	else
		cout << "\n\tgrener.dta ikke funnet";
}


//----------------------------------------------------------------------------------------------------

void Grener::meny2(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere) // sender bruker til ovelses meny
{
	Gren* gren;
	char navn[STRLEN];
	les("\nGrenens navn: ", navn, STRLEN);

	if (grener->inList(navn))
	{				//om grenen eksisterer
		gren = (Gren*)grener->remove(navn);
		gren->ovelseMeny(medaljer, poeng, deltagere, &antOvelser);
		grener->add(gren);
		skrivTilFil();
	}
	else
		printf("\n\tError, grenen %s eksisterer ikke!\n\n", navn);
}