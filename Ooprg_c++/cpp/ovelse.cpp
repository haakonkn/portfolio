#ifdef _MSC_VER						//S� vi kan kj�re med strcpy
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include "ovelse.h"
#include "const.h"
#include "funksjoner.h"
#include "deltager.h"
#include "deltagere.h"
using namespace std;

Ovelse::Ovelse(int ID, char* nvn) : NumElement(ID)		//lager ny �velse
{
	navn = nvn;
	antallDeltakere = 0;
	dato = lesDato();						//henter gyldig dato
	klokkeslett = lesNum("\nKlokkeslett(xxxx): ", 0, MAXKLOKKE);		//henter gyldig klokkelsett
}

Ovelse::~Ovelse()
{	
	delete navn;
}


void Ovelse::endreData(char* nvn)				//endrer dato og klokkeslett
{
	navn = nvn;
	dato = lesDato();
	klokkeslett = lesNum("\nNytt klokkeslett(xxxx): ", 0, MAXKLOKKE);
}


void Ovelse::skrivHovedData()		//skriver ut nummer, navn, dato, klokkeslett og antall deltagere
{
	cout << "\n\tOvelse: " << number << " - " << navn << "\n";
	cout << "Dato: "; if (dato < 100000) cout << "0"; cout << dato << endl;
	cout << "Klokkeslett: ";
	
	if (klokkeslett < 10)
		cout << "000";
	else if (klokkeslett < 100)
		cout << "00";
	else if (klokkeslett < 1000)
		cout << "0";
	cout << klokkeslett << endl;
	cout << "Antall deltagere: " << antallDeltakere << endl;
}


void Ovelse::skrivTilFil(ofstream& grenFil)		//skriver ovelse til fil
{
	grenFil << number << endl;
	grenFil << navn << endl;
	grenFil << dato << endl;
	grenFil << klokkeslett << endl;
	grenFil << antallDeltakere << endl;
}


Ovelse::Ovelse(std::ifstream& grenFil, int ID) : NumElement(ID)		//leser ovelse fra fil
{
	char buffer[STRLEN];

	grenFil.getline(buffer, STRLEN);
	navn = new char[strlen(buffer) + 1];
	strcpy(navn, buffer);

	grenFil >> dato; grenFil.ignore();

	grenFil >> klokkeslett; grenFil.ignore();

	grenFil >> antallDeltakere; grenFil.ignore();

	lesStartlisteFraFil();
}


//---------------------StartListe-relatert--------------------//


void Ovelse::startListe(Deltagere* deltagere)			//start liste meny
{
				// Navn p� startliste: (OV-XXX-.STA)
	string temp;			
	ostringstream konverter;
	konverter << number;
	temp = konverter.str();			//genererer filnavn

	string filNavn = "OV";
	filNavn += temp;
	filNavn += ".STA";


	char valg;
	do {
		cout << "\n\t++StartlisteMeny++\n\n"
			<< "N - ny liste\n"
			<< "E - endre starliste\n"
			<< "S - skriv startliste\n"
			<< "F - fjern startliste\n"
			<< "U - ut av startlistemeny\n";

		valg = les();
		valg = toupper(valg);

		switch (valg) {
		case 'N': nyStartliste(deltagere, filNavn.c_str()); break;
		case 'E': endreStartliste(deltagere, filNavn.c_str()); break;
		case 'S': skrivStartliste(deltagere); break;
		case 'F': fjernStartliste(filNavn.c_str()); break;
		case 'U': break;
		default: cout << "\n" << valg << " er en ugyldig kommando!\n" << endl;
		}
	}
	while (valg != 'U');			//s� lenge bruker ikke vil avslutte
}

void Ovelse::lesStartlisteFraFil()
{
	string temp;				// Skaffer filnavnet:
	ostringstream konverter;
	konverter << number;
	temp = konverter.str();

	string filNavn = "OV";
	filNavn += temp;
	filNavn += ".STA";



	fstream startListe(filNavn);
	if (startListe)
	{
		startListe >> antallDeltakere; startListe.ignore();		// Leser antall deltagere.
		for (int i = 0; i < antallDeltakere; i++)				// Leser ID for alle deltagere:
		{
			startListe >> deltakere[i]; startListe.ignore();
		}
	}
}

void Ovelse::nyStartliste(Deltagere* deltagere, string filNavn)
{
	char valg;				
	int deltagerId;
	antallDeltakere = 0;

	fstream tilFil(filNavn);
	if (tilFil)					
	{
		cout << "Startliste finnes allerede!\n";
	}
	else
	{
		do
		{
			do												// Looper til bruker har skrevet gyldig deltagerID:
			{
				deltagerId = lesNum("Deltager ID: ", 0, MAXDELTAGERE);
			}
			while (!deltagere->finnes(deltagerId) || iLista(deltagerId));		

			deltakere[antallDeltakere++] = deltagerId;
			cout << "Vil du fortsette?(Ja(j), Nei(N) )\n"; valg = les();
		}
		while (valg != 'N');
		skrivStartlisteTilFil(filNavn);		// skriver ut startnummer, deltagers ID, navn og nasjon
	}
}


bool Ovelse::iLista(int ID)			// Returnerer om deltakerID ligger i startLista:
{
	for (int i = 0; i < antallDeltakere; i++)
	{
		if (ID == deltakere[i])
			return true;
	}
	return false;
}

void Ovelse::endreStartliste(Deltagere* deltagere, string filNavn)
{
	int temp, bytt;			// Verdier for � endre startNr til deltager.

	char tall[STRLEN];
	sprintf(tall, "%d", this->number);

	std::string resultatlistenavn = "OV";			//genererer filnavn
	resultatlistenavn += tall;
	resultatlistenavn += ".RES";

	ifstream resultatfil(resultatlistenavn);

	if (!resultatfil)
	{
		lesStartlisteFraFil();
		if (antallDeltakere > 0)
		{
			temp = lesNum("Deltagers startnummer du vil endre: ", 1, antallDeltakere) - 1;
			bytt = lesNum("Nytt nummer som deltager skal starte som: ", 1, antallDeltakere) - 1;

			int i = temp;
			temp = deltakere[temp];


			while (i != bytt)			// Flytter en og en deltager ett hakk opp eller ned p� startlista:
			{
				if (i > bytt)
				{
					deltakere[i] = deltakere[i - 1];
					i--;
				}
				else if (i < bytt)
				{
					deltakere[i] = deltakere[i + 1];
					i++;
				}
			}
			deltakere[bytt] = temp;		// Putter deltager p� ny startPlass.
			skrivStartlisteTilFil(filNavn);
		}
		else
		{
			cout << "Startliste finnes ikke!\n";
		}
	}
	else cout << "\n\tError - resultatfil for denne ovelsen eksisterer allerede!\n";
}


void Ovelse::skrivStartliste(Deltagere* deltagere)
{
	antallDeltakere = 0;
	lesStartlisteFraFil();
	Deltager* deltager;

	if (antallDeltakere > 0)		
	{
		std::cout << "\nStartnummer\tID\tNavn\t\tNasjon\n";
		for (int i = 0; i < antallDeltakere; i++)					// Skriver data for alle deltakere i startLista:
		{
			deltager = deltagere->getDeltager(deltakere[i]);	// Henter data for deltager som skal skrives.
			std::cout << i + 1 << "\t\t" << deltakere[i] << "\t"
				<< deltager->getNavn() << setw(20 - strlen(deltager->getNavn())) << deltager->getNasjon() << endl;
		}
	}
	else
	{
		cout << "\nFinnes ingen deltakere i startLista!\n";
	}
}


void Ovelse::fjernStartliste(string tempNavn)
{
	const char* filNavn = tempNavn.c_str();		// Konverterer fra string til char*.
	
	if (remove(filNavn) == 0)			
	{
		printf("%s er na slettet!\n\n", filNavn);
		antallDeltakere = 0;
	}
	else
	{
		printf("\n\tkunne ikke slette '%s' \n\n", filNavn);
	}
}

void Ovelse::skrivStartlisteTilFil(string filNavn)		// Lagrer ID til deltagere i fil:
{
	ofstream tilFil(filNavn);
	tilFil << antallDeltakere << endl;

	for (int i = 0; i < antallDeltakere; i++)			// Skriver deltakerID til fil:
	{
		tilFil << deltakere[i] << endl;
	}
}

int Ovelse::hentStartnummer(int ID)
{
	for (int i = 0; i < antallDeltakere; i++)		// Henter nr til resultatliste:
	{
		if (deltakere[i] == ID)
			return i;
	}
	cout << "\n\tKunne ikke finne startnummer " << ID << endl;
	return -1;
}

//---------------------Resultat-relatert--------------------//

void Ovelse::skrivResultatliste(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, RegType regtype)	//henter og skriver resultatliste
{
	char tall[STRLEN];
	sprintf(tall, "%d", this->number);

	std::string filnavn = "OV";			//genererer filnavn
	filnavn += tall;
	filnavn += ".RES";

	ifstream resultatfil(filnavn);
	if (!resultatfil)
		printf("\n\tError, resultatfilen %s for ovelsen %s eksisterer ikke!\n", filnavn.c_str(), this->navn);
	else											//om filen eksisterte
	{
		Resultat resultater[MAXDELTAGERE];
		int nResultater;
		bool sortert = true;

		for (nResultater = 0; nResultater < antallDeltakere; nResultater++)		//henter alle resultater fra filen
		{
			resultatfil >> resultater[nResultater].poeng;					//legger inn nytt resultat og �ker antall
			resultatfil >> resultater[nResultater].ID; resultatfil.ignore();
		}

		for (int i = 0; i < nResultater - 1; i++)				//sjekker om den er sortert i riktig rekkef�lge i forhold til regType
		{
			if (resultater[i].poeng < resultater[i+1].poeng && regtype >= hopp)
				sortert = false;

			if (resultater[i].poeng > resultater[i+1].poeng && regtype < hopp)
				sortert = false;
		}
		resultatfil.close();

		
		if (!sortert)			//om filen ikke var sortert
		{
			if (regtype == kunstlop || regtype == hopp)				//sorteres synknde for poeng  (hopp og kunstl�p)
			{
				sorterResultater(resultater, 0, nResultater -1);		//sorterer resultatene stigende
				inverterResultater(resultater, nResultater - 1);		// inverterer array
			}
			else 
				sorterResultater(resultater, 0, nResultater);			//sorterer stigende for tider
		
			skrivResultaterTilFil(filnavn, resultater, nResultater);		//oppdaterer filen
			printf("\nResultatfilen %s var usortert og ble oppdatert, oppdatere medaljer og poeng for dette resultatet? (Y/N) ", filnavn.c_str());
			char valg;		
			do {
				valg = les();		//henter gyldig brukervalg
				valg = toupper(valg);
			} while (valg != 'Y' && valg != 'N');
	
			if (valg == 'Y')		//legger til medaljer og poeng om brukeren �nsker det
			{
				oppdaterMedaljer(deltagere, medaljer, resultater, nResultater, false);
				oppdaterPoeng(deltagere, poeng, resultater, nResultater, false);
			}
		}
		
	
		Deltager* deltager;
		cout << "\n\tResultatliste for " << this->navn << endl;

		std::cout << "\nStartnummer\tID\tNavn\t\tNasjon\t\t";		//skriver ut navn p� kolonner
		if (regtype == hopp || regtype == kunstlop)
			cout << "Poeng" << endl;
		else
			cout << "Tid" << endl;
		
		for (int i = 0; i < antallDeltakere; i++)
		{
			deltager = deltagere->getDeltager(resultater[i].ID);		//skriver data for resultater til skjerm p� en pen m�te (ikke mtp koden)
			std::cout << hentStartnummer(deltager->getId()) << "\t\t" << deltager->getId() << "\t" << deltager->getNavn() << setw(20 - strlen(deltager->getNavn()))
				<< deltager->getNasjon() << setw(20 - strlen(deltager->getNasjon())) << resultater[i].poeng << endl;
		}
	}
}

void Ovelse::nyResultatliste(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, RegType regtype)
{
	char tall[STRLEN];
	sprintf(tall, "%d", this->number);

	std::string filnavn = "OV";			//genererer filnavn for resultatfil
	filnavn += tall;
	filnavn += ".RES";

	std::string filnavn2 = "OV";			//genererer filnavn for startfil
	filnavn2 += tall;
	filnavn2 += ".STA";

	ifstream startfil(filnavn2.c_str());

	if (!startfil)
		printf("\n\tError, startfil %s for ovelsen %s eksisterer ikke!\n", filnavn2.c_str(), this->navn);
	else
	{										//om startfil eksisterer
		startfil.close();
		ifstream resultatfil(filnavn.c_str());

		if (resultatfil)
			printf("\n\tError, resultatfil %s for ovelsen %s eksisterer allerede!\n", filnavn.c_str(), this->navn);
		else if (!resultatfil)			//om det ikke var en resultatfil
		{
			resultatfil.close();				//lukker fil stream'en
			Resultat resultater[MAXDELTAGERE];

			for (int i = 0; i < antallDeltakere; i++)
			{
				cout << "\nStartnummer = " << i + 1;
				resultater[i].ID = deltakere[i];
				resultater[i].poeng = lesRegtype(regtype);
				printf("score %d av %d registrert!\n", i + 1, antallDeltakere);
			}

			if (regtype == kunstlop || regtype == hopp)							//sorteres synknde for hopp og kunstl�p
			{
				sorterResultater(resultater, 0, antallDeltakere - 1);
				inverterResultater(resultater, antallDeltakere - 1);
			}
			else
				sorterResultater(resultater, 0, antallDeltakere - 1);			//eller �kende for tid'er

			oppdaterMedaljer(deltagere, medaljer, resultater, antallDeltakere, false);	//oppdaterer medaljer og poeng
			oppdaterPoeng(deltagere, poeng, resultater, antallDeltakere, false);

			skrivResultaterTilFil(filnavn, resultater, antallDeltakere);		//skriver til fil
		}
	}
}

void Ovelse::slettResultatliste(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere)
{
	
	char tall[STRLEN];
	sprintf(tall, "%d", this->number);

	std::string filnavn = "OV";			//genererer filnavn
	filnavn += tall;
	filnavn += ".RES";

	ifstream resultatfil(filnavn.c_str());
	if (!resultatfil)
		printf("\n\tError, resultatfil %s for ovelsen %s eksisterer ikke!\n", filnavn.c_str(), this->navn);
	else						//om fil eksisterer
	{
		
		Resultat resultater[MAXDELTAGERE];
	
		for (int i = 0; i < antallDeltakere; i++)			//henter all data fra fil
		{
			resultatfil >> resultater[i].poeng;					//legger inn nytt resultat og �ker antall
			resultatfil >> resultater[i].ID;
			resultatfil.ignore();
		}	
		resultatfil.close();

		if (remove(filnavn.c_str()) != 0)		//om den ikke sletter filen
			printf("\n\tkunne ikke slette %s", filnavn.c_str());

		else							//om den sletter filen
		{
			oppdaterMedaljer(deltagere, medaljer, resultater, antallDeltakere, true);		//fjerner poeng / medaljer
			oppdaterPoeng(deltagere, poeng, resultater, antallDeltakere, true);

			printf("\n\tFil %s slettet", filnavn.c_str());
		}
	}
}

void Ovelse::sorterResultater(Resultat* resultater, int start, int slutt)	//bruker quicksort for � sortere resultat-array
{																					//quicksort forklart i medaljer.cpp, dette er nesten samme kode
	int pivot, j, i;
	Resultat temp;
	
	if (start < slutt)
	{
		pivot = start;
		i = start; j = slutt;

		while (i < j)
		{
			{
				while ((resultater[i].poeng <= resultater[pivot].poeng) && (i < slutt))
					i++;

				while (resultater[j].poeng > resultater[pivot].poeng)
					j--;
			}
			
			if (i < j)
			{
				temp = resultater[i];
				resultater[i] = resultater[j];
				resultater[j] = temp;
			}
		}
		temp = resultater[pivot];
		resultater[pivot] = resultater[j];
		resultater[j] = temp;
		sorterResultater(resultater, start, j - 1);
		sorterResultater(resultater, j + 1, slutt);
	}

}

void Ovelse::oppdaterMedaljer(Deltagere* deltagere, Medaljer* medaljer, Resultat* resultater, int nResultater, bool fjern)
{
	if (nResultater > 3)		//deler ut eller fjerner max 3 medaljer
		nResultater = 3;

	for (int i = 0; i < nResultater; i++)	
	{
		Deltager* deltager = deltagere->getDeltager(resultater[i].ID);		//henter topp deltagere
		char* nasjon = deltager->getNasjon();		//henter nasjon navn
		medaljer->oppdater(nasjon, (Medaljetype)i, fjern);		//gir eller tar medaljer fra nasjon
	}
}

void Ovelse::oppdaterPoeng(Deltagere* deltagere, Poeng* poeng, Resultat* resultater, int nResultater, bool fjern)
{
	if (nResultater > 6)				//gir eller tar poeng fra max 6 deltagere
		nResultater = 6;

	for (int i = 0; i < nResultater; i++)	//g�r gjennom de beste
	{
		Deltager* deltager = deltagere->getDeltager(resultater[i].ID);
		char* nasjon = deltager->getNasjon();		//henter nasjon navnet
		if (i == 0)
			poeng->oppdater(nasjon, 7, fjern);		//fjerner / gir 7 poeng for gull
		else
			poeng->oppdater(nasjon, 6 - i, fjern);	//og 5-1 for de resterende plassene
	}
}

void Ovelse::inverterResultater(Resultat* resultater, int nResultater)
{
	int i = 0;
	Resultat temp;

	while (i < nResultater)		//g�r gjennom alle resultater
	{
		temp = resultater[i];				//butter de bakerste lengst frem vice versa
		resultater[i] = resultater[nResultater];
		resultater[nResultater] = temp;
		i++;
		nResultater--;
	}
}

void Ovelse::skrivResultaterTilFil(std::string filnavn, Resultat* resultater, int nResultater)
{
	ofstream resultatfil(filnavn);

	for (int i = 0; i < nResultater; i++)	//skriver alle resultatene til fil
	{
		resultatfil << resultater[i].poeng << " " << resultater[i].ID << endl;
	}
	resultatfil.close();
}

char* Ovelse::getNavn()			//henter navnet til �velse
{
	return navn;
}