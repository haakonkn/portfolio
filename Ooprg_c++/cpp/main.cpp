// main.cpp

#include <iostream>
#include "nasjoner.h"
#include "funksjoner.h"
#include "deltagere.h"
#include "grener.h"
#include "poeng.h"
#include "medaljer.h"

using namespace std;


Nasjoner* nasjonsbase;
Deltagere* deltagerbase;
Grener* grenbase;
Poeng* poengstatistikk;
Medaljer* medaljestatistikk;


void meny();
void initialiser_baser();
void lesFraFil();
void skrivTilFil();


int main()
{
	initialiser_baser();
	lesFraFil();
	
	meny();

	//skrivTilFil()
	return 0;
}

void meny()
{
	char valg;
	do {
		cout << "\n\t++Hoved Meny++\n"			//skriver ut menyen
			<< "N = Nasjonmeny\n"
			<< "D = Deltagermeny\n"
			<< "G = Grenmeny\n"
			<< "M = Medaljeoversikt\n"
			<< "P = poengoversikt\n"
			<< "O = Ovelsemeny\n"
			<< "Q = quit\n";

		valg = les();										//henter brukervalg
		valg = toupper(valg);

		switch (valg)
		{
		case 'N': nasjonsbase->meny(deltagerbase, medaljestatistikk, poengstatistikk); break;
		case 'D': deltagerbase->meny(nasjonsbase); break;
		case 'M': medaljestatistikk->display();  break;
		case 'P': poengstatistikk->display(); break;
		case 'G': grenbase->meny(); break;
		case 'O': grenbase->meny2(medaljestatistikk, poengstatistikk, deltagerbase); break;
		case 'Q': break;
		default: cout << "\n" << valg << " er en ugyldig kommando!" << endl;
		}
	} while (valg != 'Q');		//looper s� lenge brukeren ikke �nsker � avslutte

	skrivTilFil();
}


void lesFraFil()			//henter alle dataer fra filer utenom .res og .sta formatene
{
	nasjonsbase->lesFraFil(medaljestatistikk, poengstatistikk);
	grenbase->lesFraFil();
	deltagerbase->lesFraFil();
	medaljestatistikk->lesFraFil();
	poengstatistikk->lesFraFil();

	cout << endl;
}

void skrivTilFil()		//skriver  alt til fil utenom .res og .sta
{
	nasjonsbase->skrivTilFil();
	grenbase->skrivTilFil();
	deltagerbase->skrivTilFil();
	medaljestatistikk->skrivTilFil();
	poengstatistikk->skrivTilFil();
}


void initialiser_baser()	//initialiserer alle database objektene
{
	nasjonsbase = new Nasjoner;
	grenbase = new Grener;
	deltagerbase = new Deltagere;
	medaljestatistikk = new Medaljer;
	poengstatistikk = new Poeng;
}