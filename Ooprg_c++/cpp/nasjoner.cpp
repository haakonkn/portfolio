#include <iostream>
#include <fstream>
#include "nasjoner.h"
#include "funksjoner.h"
#include "nasjon.h"
#include "const.h"
#include "deltager.h"
#include "deltagere.h"



using namespace std;

//---------------------------------------------------------------------------------------------------
//Constructor og deconstructor.
Nasjoner::Nasjoner()
{
	nasjonene = new List(Sorted);
}

Nasjoner::~Nasjoner()
{
	delete nasjonene;
}
//--------------------------------------------------------------------------------------------------

//Legg inn ny nasjon, om den ikke finnes da s�klart.
void Nasjoner::nyNasjon(Medaljer* medaljer, Poeng* poeng)
{
	Nasjon* ny;
	char* nasjonId;
	int nr = nasjonene->noOfElements();
	if (nr < STRLEN)								//Ny nasjon s� lenge det er mindre
	{												//nasjoner enn STRLEN i lista.
		do											//Les inn ny nasjonId s� lenge
		{											//den man skriver inn finnes
			nasjonId = lesId();						//i lista.
		} while (nasjonene->inList(nasjonId));

		ny = new Nasjon(nasjonId);					//Da man har f�tt en ny nasjonId
		nasjonene->add(ny);							//lager man ny nasjon.


		skrivTilFil();
	}
	else
	{
		cout << "\n\nFor mange nasjoner!\n";
	}
}

//--------------------------------------------------------------------------------------------------

//Skriver nasjonMeny.
void Nasjoner::meny(Deltagere* deltagere, Medaljer* medaljer, Poeng* poeng)
{
	char valg;
	cout << "\n\t++NasjonMeny++\n"
		<< "N = Ny nasjon\n"
		<< "E = Endre en nasjon\n"					//Meny for nasjoner.
		<< "A = Hoveddata om alle nasjoner\n"
		<< "T = En nasjons deltagertropp\n"
		<< "S = Alle data om en nasjon\n"
		<< "U = Ut av nasjonMeny\n";
	valg = les();
	while (valg != 'U')								//Fortsett mens valg ikke er 'U'.
	{

		switch (valg)
		{
		case 'N':
			nyNasjon(medaljer, poeng);
			break;
		case 'E':
			endreNasjon();
			break;
		case 'A':
			skrivHovedData();
			break;
		case 'T':
			skrivTropp(deltagere);
			break;
		case 'S':
			skrivNasjonData();
			break;
		default:
			break;
		}

		cout << "\n\t++NasjonMeny++\n"
			<< "N = Ny nasjon\n"
			<< "E = Endre en nasjon\n"
			<< "A = Hoveddata om alle nasjoner\n"
			<< "T = En nasjons deltagertropp\n"
			<< "S = Alle data om en nasjon\n"
			<< "U = Ut av nasjonMeny\n";
		valg = les();
	}

}

//-------------------------------------------------------------------------------------------------

void Nasjoner::skrivNasjonData()
{
	Nasjon* vis;
	char* nasjonId;

	do
	{													//Fortsett s� lenge nasjonId'en
		nasjonId = lesId();								//ikke finnes.
	} while (!nasjonene->inList(nasjonId));

	vis = (Nasjon*)nasjonene->remove(nasjonId);			//Hent ut.
	vis->display();										//Display.
	nasjonene->add(vis);								//Add tilbake.
}

//-----------------------------------------------------------------------------------------

void Nasjoner::endreNasjon()
{
	Nasjon* endre;
	char* nasjonId;
	do
	{
		nasjonId = lesId();							//Fortsett s�lenge nasjonId'en
	} while (!nasjonene->inList(nasjonId));			//ikke finnes.

	endre = (Nasjon*)nasjonene->remove(nasjonId);	//Hent.
	endre->endreData();								//Endre data.
	nasjonene->add(endre);							//Add tilbake.

	skrivTilFil();
}

//-------------------------------------------------------------------------------------------

void Nasjoner::skrivHovedData()
{
	Nasjon* vis;
	int nr;

	nr = nasjonene->noOfElements();					//Skriver ut hoveddata for alle
	for (int i = 1; i <= nr; i++)					//nasjoner i lista nasjonene.
	{
		vis = (Nasjon*)nasjonene->removeNo(i);		//Hent.
		vis->visHovedData();						//Vis.
		nasjonene->add(vis);						//Putt tilbake.
	}
}

//-----------------------------------------------------------------------------------------

void Nasjoner::skrivTilFil()
{
	Nasjon* nasjon;
	ofstream nasjonfil("nasjoner.dta");
	int nNasjoner = nasjonene->noOfElements();

	nasjonfil << nNasjoner << endl;						//Antall nasjoner skal bare skrives en gang.

	for (int i = 1; i <= nNasjoner; i++)
	{													//Skriver ut alle nasjoner
		nasjon = (Nasjon*)nasjonene->removeNo(i);		//til fil.
		nasjon->skrivTilFilen(nasjonfil);
		nasjonene->add(nasjon);
	}
}

//-------------------------------------------------------------------------------------------

int Nasjoner::finnes()
{
	return nasjonene->noOfElements();					//Returnerer antall elementer i nasjoneneLista.
}

//-------------------------------------------------------------------------------------------

void Nasjoner::lesFraFil(Medaljer* medaljer, Poeng* poeng)
{
	int nNasjoner;
	char buffer[STRLEN];
	Nasjon* nasjon;
	ifstream nasjonfil("nasjoner.dta");

	if (nasjonfil)
	{
		nasjonfil >> nNasjoner; nasjonfil.ignore();		//Henter antall nasjoner.
		cout << "\n\tnasjoner.dta laster inn ";

		for (int i = 1; i <= nNasjoner; i++)
		{
			cout << ".";
			nasjonfil.getline(buffer, STRLEN);			//Leser inn alle nasjonene
			nasjon = new Nasjon(nasjonfil, buffer);		//fra fil.
			nasjonene->add(nasjon);
		}

	}
	else
	{
		cout << "\n\tnasjoner.dta ikke funnet";
	}
}

//---------------------------------------------------------------------------------------------

void Nasjoner::skrivTropp(Deltagere* deltagerne)
{
	char* nasjonId;

	do
	{										//Sjekker at nasjonId'en 
		nasjonId = lesId();					//finnes.
	} while (!nasjonene->inList(nasjonId));
	
	deltagerne->skrivDeltagereITropp(nasjonId);	//Skriver ut alle deltagere til troppen.
}

//-----------------------------------------------------------------------------------------

bool Nasjoner::iLista(char* text)
{
	if (nasjonene->inList(text))
		return true;						//Sjekker om nasjonen er i lista.
	else
		return false;
}

//------------------------------------------------------------------------------------------
//funksjon for � lage idnr for deltakere.
void Nasjoner::leggTilDeltager(char* nasjonNavn)
{
	Nasjon* nasjon;
	nasjon = (Nasjon*)nasjonene->remove(nasjonNavn);
	nasjon->leggTilDeltager();				//Ant deltagere i nasjonen +=1;
	nasjonene->add(nasjon);

	skrivTilFil();
}