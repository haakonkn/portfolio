#ifdef _MSC_VER						//S� vi kan kj�re med strcpy
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <fstream>
#include "deltagere.h"
#include "deltager.h"
#include "nasjoner.h"
#include "nasjon.h"
#include "funksjoner.h"
#include "const.h"

Deltagere::Deltagere()
{
	deltagere = new List(Sorted);
}

Deltagere::~Deltagere()
{
	delete deltagere;
}

void Deltagere::nyDeltager(Nasjoner* nasjoner)					
{
	int tempId;
	char buffer[STRLEN];				// Landet til deltager.
	char* forkortelse;
	Deltager* nyDeltager;

	if (nasjoner->finnes())
	{
		tempId = lesNum("\nDeltager-ID: ", 0, MAXDELTAGERE);
		if (!deltagere->inList(tempId)) // Hvis deltager ikke eksisterer:
		{

			do							// Looper til bruker skriver id med lengde 3:
			{
				forkortelse = lesId();
			} while (strlen(forkortelse) != 3);
		
			if (nasjoner->iLista(forkortelse))
			{
				nasjoner->leggTilDeltager(forkortelse); // �k antall deltakere i nasjon.
				nyDeltager = new Deltager(tempId, forkortelse);		// Lager deltager.
				cout << "\nLagt til deltager:"; nyDeltager->display();
				deltagere->add(nyDeltager);			// Legg til i liste
			}
			else
			{
				cout << "\nLandet finnes ikke!";
			}
		}
		else
		{
			cout << "\nDeltager eksisterer allerede!";
		}
	}
	else
	{
		cout << "\nIngen nasjoner finnes!";
	}
}

void Deltagere::endreDeltager(Nasjoner* nasjoner)
{
	Deltager* deltager;
	int nr = deltagere->noOfElements();
	if (nasjoner->finnes())									// antNasjoner > 0
	{
		if (nr) {											// antDeltagere > 0
			int id = lesNum("\nID-nummer til deltager: ", 101, nr * 100 + 99);		//  Hvis 5 nasjoner: 101->599.
			if (deltagere->inList(id))						// Endre data hvis deltager ligger i lista:
			{
				deltager = (Deltager*)deltagere->remove(id);
				deltager->endreData(nasjoner);
				deltagere->add(deltager);
			}
		}
		else
		{
			cout << "\nIngen deltagere finnes!";
		}
	}
	else
	{
		cout << "\nIngen nasjoner finnes!";
	}
}

void Deltagere::skrivAlleDeltagere()
{
	Deltager* deltager;
	int nr = deltagere->noOfElements();
	if (nr > 0) {									// Skriv info for nr deltagere n�r de finnes:
		for (int i = 1; i <= nr; i++)
		{
			deltager = (Deltager*)deltagere->removeNo(i);
			deltager->skrivInfo();
			deltagere->add(deltager);
		}
	}
	else
	{
		cout << "\nFinnes ingen deltagere!" << endl;
	}
}

void Deltagere::skrivDeltager(int ID)
{
	Deltager* deltager;
	if (deltager = (Deltager*)deltagere->remove(ID))		// Skriv data om en deltager for resultatene:
	{		
		deltager->display2();
		deltagere->add(deltager);
	}
	else cout << "\n\terror, fant ikke deltaker " << ID << endl;
}




void Deltagere::skrivDeltager(Nasjoner* nasjoner)
{
	Deltager* deltager;
	int id, nr;
	if (nr = nasjoner->finnes())		// antNasjoner > 0
	{
		if (deltagere->noOfElements()) {				// antDeltagere > 0
			id = lesNum("\nID-nummer til deltager: ", 101, nr * 100 + 99);		//  Hvis 5 nasjoner: 101->599.
			if (deltager = (Deltager*)deltagere->remove(id)) {			//  Da ETT land kan ha 99 deltagere.
				deltager->display();
				deltagere->add(deltager);
			}
			else
			{
				cout << "\nDeltager finnes ikke!";
			}
		}
		else
		{
			cout << "\nIngen deltagere finnes";
		}
	}
	else
	{
		cout << "\nIngen nasjoner finnes!";
	}
}

void Deltagere::meny(Nasjoner* nasjoner)
{
	char valg;
	skrivMeny();
	valg = les();
	while (valg != 'U')
	{
		switch (valg)
		{
		case 'N': nyDeltager(nasjoner); 	skrivTilFil();	break;	//  Trenger nasjonbase for � sjekke om nasjoner finnes
		case 'E': endreDeltager(nasjoner);  skrivTilFil(); break;
		case 'A': skrivAlleDeltagere(); break;
		case 'S': skrivDeltager(nasjoner); break; //  Trenger nasjonbase for � sjekke om nasjoner finnes
		}
		skrivMeny();
		valg = les();
	}
}

void Deltagere::skrivMeny()
{
	cout << "\n\n\t++DeltagerMeny++\n"
		<< "N - Registrer en ny deltager\n"
		<< "E - Endre en deltager\n"
		<< "A - Skriv hoveddata om alle deltagere\n"
		<< "S - Skriv data om en deltager\n"
		<< "U - Ut av deltagermeny\n\n";
} 

int Deltagere::finnes()
{
	return deltagere->noOfElements();
}

void Deltagere::skrivDeltagereITropp(char* nasjonId)
{
	Deltager* deltager;
	int nr = finnes();
	for (int i = 1; i <= nr; i++)				// Skriver data om alle deltagerne som har gitt nasjonId:
	{
		deltager = (Deltager*)deltagere->removeNo(i);
		if (*deltager->getNasjon() == *nasjonId)
		{
			deltager->skrivInfo();
		}
		deltagere->add(deltager);
	}
	cout << endl;
}

void Deltagere::lesFraFil()
{
	ifstream innfil("deltagere.dta");
	Deltager* deltagerFraFil;
	int nr, id;
	if (innfil)
	{
		cout << "\n\tdeltagere.dta laster inn ";
		innfil >> nr; innfil.ignore();		// Henter antall deltagere.
		for (int i = 1; i <= nr; i++)		// Henter deltagerId for alle deltagere.
		{
			cout << ".";
			innfil >> id; innfil.ignore();				
			deltagerFraFil = new Deltager(innfil, id);
			deltagere->add(deltagerFraFil);
		}

	}
	else
	{
		cout << "\n\tdeltakere.dta ikke funnet";
	}
}

void Deltagere::skrivTilFil()
{
	ofstream tilFilen("deltagere.dta");
	Deltager* deltagerTilFil;
	int nr = deltagere->noOfElements();

	tilFilen << nr << endl;						// Skriver antall deltagere.
	for (int i = 1; i <= nr; i++)				// Skriver ID til alle deltagere:
	{
		deltagerTilFil = (Deltager*)deltagere->removeNo(i);
		deltagerTilFil->skrivTilFilen(tilFilen);
		deltagere->add(deltagerTilFil);
	}
}

Deltager* Deltagere::getDeltager(int ID)
{
	Deltager* deltager = (Deltager*)deltagere->remove(ID);
	deltagere->add(deltager);
	return deltager;
}

bool Deltagere::finnes(int ID)
{
	return deltagere->inList(ID);
}