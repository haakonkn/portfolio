#if !defined(GREN_H)
#define GREN_H

#include "ListTool2B.h"
#include <fstream>
#include "deltagere.h"
#include "enum.h"
#include "medaljer.h"
#include "poeng.h"

class Gren : public TextElement
{
private:
	int regType;
	List* ovelser;

public:
	Gren() {};
	Gren(char* navn);	
	Gren(std::ifstream& grenFil, char* navn);
	~Gren();
	void endreData();
	void skrivHovedData();
	void skrivFullData();

	void skrivTilFil(std::ofstream& grenFil);
	void startListe(Deltagere*, int* antOvelser);
	void resultatListeMeny(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, RegType regtype);

	void ovelseMeny(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, int* antOvelser);
	void nyOvelse(int* antOvelser);
	void endreOvelse(int* antOvelser);
	void skrivOvelsedata();
	void fjernOvelse(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, int* antOvelser);
	
	bool navnEksisterer(char*nvn);
};

#endif 