#if !defined(DELTAGER_H)
#define DELTAGER_H

#include "ListTool2B.h"
#include "const.h"
#include "enum.h"
#include "nasjoner.h"

using namespace std;

class Deltager : public NumElement
{
private:
					// ID-nummer blir satt i number
	char* navn;		// Fullt navn			
	char* land;		// Forkortelse p� land
	char* data;
	Kj�nn kj�nn;
public:
	Deltager();
	Deltager(int id, char* land);
	Deltager(std::ifstream & inn, int id);
	~Deltager();
	void skrivInfo();
	void display();
	void display2();
	void endreData(Nasjoner* nasjoner);
	void skrivTilFilen(std::ofstream & ut);
	char* getNavn();
	char* getNasjon();
	int getId();
};
#endif 