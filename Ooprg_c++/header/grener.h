#if !defined(GRENER_H)
#define GRENER_H

#include "ListTool2B.h"
#include "deltagere.h"
#include "medaljer.h"
#include "poeng.h"

class Grener
{
private:
	int antOvelser = 0;				//brukes for og lage unik ID for ovelser.
	List* grener;
public:
	Grener();
	~Grener();

	void nyGren();
	void meny();
	void endreGren();

	void skrivFullData();
	void skrivHovedData();

	void skrivTilFil();
	void lesFraFil();

//-----------------------------------------------------------------------
	void meny2(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere);
};

#endif 