#if !defined(__NASJONER_H)   
#define __NASJONER_H 

#include "ListTool2B.h"
#include "medaljer.h"
#include "poeng.h"

class Deltagere;		//Brukes for � slippe bugg med syntax error.

class Nasjoner
{
private:
	List* nasjonene;	//Nasjon liste.
public:
	Nasjoner();
	~Nasjoner();
	void nyNasjon(Medaljer* medaljer, Poeng* poeng);
	void endreNasjon();
	void skrivHovedData();
	void skrivTropp(Deltagere* deltagere);
	void skrivNasjonData();
	void meny(Deltagere* deltagere, Medaljer* medaljer, Poeng* poeng);
	void skrivTilFil();
	int finnes();
	void lesFraFil(Medaljer* medaljer, Poeng* poeng);
	bool iLista(char* text);
	void leggTilDeltager(char* nasjonNavn);

};


#endif