#if !defined(__CONST_H) 
#define __CONST_H

//constants
const int STRLEN = 200;			//Tekst lengde.		
const int NASJONID = 4;			//Lengden p� nasjonens forkortelse.
const int MINTLF = 10000000;	//Minste nummer, bare 8 siffer inntil videre
const int MAXTLF = 99999999;	//Max nummer, bare 8 siffer intill videre
const int TLF = 8;
const int MAXDELTAGERE = 1000;	
const int MINOVELSE = 1000;	//min ID p� ovelse
const int MAXOVELSE = 20;		//max antall ovelser i en gren
const int MAXKLOKKE = 2359;	//h�yeste verdi for tidspunkt
#endif