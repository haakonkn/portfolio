#if !defined(OVELSE_H)
#define OVELSE_H

#include "ListTool2B.h"
#include <fstream>
#include "const.h"
#include "deltagere.h"
#include "deltager.h"
#include "medaljer.h"
#include "poeng.h"

struct Resultat
{
	int ID, poeng;
};


class Ovelse : public NumElement
{
private:
	char* navn;
	int antallDeltakere;
	int dato;
	int klokkeslett;
			//antar at resultatilste kun skal ligge p� fil og hentes inn ved foresp�rsel
	int deltakere[MAXDELTAGERE];

public:
	Ovelse() {};
	Ovelse(int ID, char* nvn);
	Ovelse(std::ifstream& grenFil, int ID);
	~Ovelse();

	void endreData(char* nvn);
	void skrivHovedData();

	void skrivTilFil(std::ofstream& grenFil);

	bool iLista(int ID);						// Sjekker om deltager allerede ligger i lista.
	int hentStartnummer(int ID);
	void startListe(Deltagere*);
	void nyStartliste(Deltagere*, string);
	void endreStartliste(Deltagere*, string);
	void skrivStartliste(Deltagere*);
	void fjernStartliste(string);
	void skrivStartlisteTilFil(string);
	void lesStartlisteFraFil();


	void skrivResultatliste(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, RegType regtype);
	void nyResultatliste(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere, RegType regtype);
	void slettResultatliste(Medaljer* medaljer, Poeng* poeng, Deltagere* deltagere);
	void sorterResultater(Resultat* resultater,int start, int slutt);
	void skrivResultaterTilFil(std::string filnavn, Resultat* reslutater, int nResultater);
	void inverterResultater(Resultat* resultater, int nResultater);

	void oppdaterMedaljer(Deltagere* deltagere, Medaljer* medaljer, Resultat* resultater, int nResultater, bool fjern);
	void oppdaterPoeng(Deltagere* deltagere, Poeng* poeng, Resultat* resultater, int nResultater, bool fjern);

	char* getNavn();
};

#endif 