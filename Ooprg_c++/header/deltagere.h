#if !defined(DELTAGERE_H)
#define DELTAGERE_H
#include "enum.h"
#include "nasjoner.h"
#include "deltager.h"


class Deltagere {
private:
	List* deltagere;
public:
	Deltagere();
	~Deltagere();
	void meny(Nasjoner*);
	void skrivMeny();
	void nyDeltager(Nasjoner*);
	void endreDeltager(Nasjoner* nasjoner);
	void skrivAlleDeltagere();
	void skrivDeltager(Nasjoner*);
	void skrivDeltager(int ID);
	void skrivDeltagereITropp(char*);
	void lesFraFil();
	void skrivTilFil();
	int finnes();
	bool finnes(int);
	Deltager* getDeltager(int);
};
#endif 