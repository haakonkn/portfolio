#pragma once

#include "ListTool2B.h"
#include <fstream>

class Nasjon : public TextElement
{
private:
	char* navn;			//Landets fulle navn.
	char* kontaktNavn;	//Kontaktpersonens navn.
	char* data;			//Data om landet.
	int tlf;			//Kontaktpersonens tlfnr.
	int antDeltakere;	//Antall deltakere.

public:
	Nasjon();										//Skal ikke forekomme.
	Nasjon(char* text);								//Vanlig constructor.
	Nasjon(std::ifstream & inn, char* text);		//Les fra fil constructor.
	~Nasjon();
	void endreData();
	void visHovedData();
	void display();
	char* getNavn();
	int getAntDeltakere();
	void leggTilDeltager();
	void skrivTilFilen(std::ofstream & ut);
};
