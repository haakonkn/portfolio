#if !defined(__FUNKSJONER_H)
#define __FUNKSJONER_H

#include "enum.h"


void les(const char t[], char s[], const int LEN);
char les();
int lesNum(const char t[], const int MIN, const int MAX);
char* lesId();
int lesDato();
int lesRegtype(RegType regtype);

#endif