#if !defined(POENG_H)
#define POENG_H

#include "statistikk.h"
#include "const.h"
#include <fstream>
#include <cstring>

class Poeng : public Statistikk
{
private:
	int poeng[STRLEN];
public:
	Poeng();
	void display();
	int nyNasjon(char* forkortelse);
	void oppdater(char* nasjonNavn, int poeng, bool fjern);
	void sorterElementer(int start, int slutt);
	void skrivTilFil();
	void lesFraFil();
};

#endif 