package com.example.oyste.app_cbt;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CardActivity extends AppCompatActivity {
    Card card;
    boolean editMode;
    SlidingTabsFragmentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        Intent cardIntent = getIntent();
        card = (Card)cardIntent.getSerializableExtra("card");
        editMode = cardIntent.getBooleanExtra("editMode", false);

        adapter = new SlidingTabsFragmentAdapter(this, getSupportFragmentManager());

        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupViews();
    }

    /**
     * Sets up all views contained in CardActivity
     */
    private void setupViews() {
        Button deleteButton = findViewById(R.id.delete_button);
        Button editButton = findViewById(R.id.edit_button);
        Button viewButton = findViewById(R.id.done_button);
        Button commitButton = findViewById(R.id.commit_button);
        TextView modeView = findViewById(R.id.mode_view);

        Drawable editModeDrawable = getResources().getDrawable(R.drawable.view_mode, null);
        Drawable viewModeDrawable = getResources().getDrawable(R.drawable.edit_mode, null);
        String editModeText = getResources().getString(R.string.edit_mode);
        String viewModeText = getResources().getString(R.string.view_mode);

        if(!card.editable) {
            deleteButton.setEnabled(false);
            editButton.setEnabled(false);
        }

        if(editMode) {
            viewButton.setVisibility(View.VISIBLE);
            editButton.setVisibility(View.GONE);
            updateModeView(modeView, editModeText, editModeDrawable);
        } else {
            viewButton.setVisibility(View.GONE);
            editButton.setVisibility(View.VISIBLE);
            updateModeView(modeView, viewModeText, viewModeDrawable);
        }

        commitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editMode = false;
                adapter.notifyDataSetChanged();
                viewButton.setVisibility(View.GONE);
                editButton.setVisibility(View.VISIBLE);
                updateModeView(modeView, viewModeText, viewModeDrawable);
            }
        });
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (card.editable) {
                    editMode = true;
                    adapter.notifyDataSetChanged();
                    viewButton.setVisibility(View.VISIBLE);
                    editButton.setVisibility(View.GONE);
                    updateModeView(modeView, editModeText, editModeDrawable);
                }
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                card.clear();
                onBackPressed();
            }
        });
    }

    private void updateModeView(TextView modeView, String text, Drawable background) {
        modeView.setText(text);

        int paddingTop = modeView.getPaddingTop();
        int paddingBottom = modeView.getPaddingBottom();

        modeView.setBackground(background);

        modeView.setPadding(0, paddingTop, 0, paddingBottom);
    }

    @Override
    public void onBackPressed() {
        Intent result = new Intent();
        result.putExtra("card", card);
        result.putExtra("id", getIntent().getStringExtra("id"));
        setResult(RESULT_OK, result);
        finish();
    }
}