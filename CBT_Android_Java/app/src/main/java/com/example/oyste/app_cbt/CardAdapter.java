package com.example.oyste.app_cbt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.View;
import android.widget.Filter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentSnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;

public class CardAdapter extends ArrayAdapter<DocumentSnapshot> {

    ArrayList<DocumentSnapshot> filteredList;

    public CardAdapter(@NonNull Context context, int resource, ArrayList<DocumentSnapshot> cards) {
        super(context, resource, cards);
        filteredList = cards;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.card, parent, false);
        TextView textView = convertView.findViewById(R.id.textView);
        ProgressBar progressBar = convertView.findViewById(R.id.progressBar);
        TextView dateTextView = convertView.findViewById(R.id.dateTextView);

        // Get data from snapshot and place in views
        DocumentSnapshot documentSnapshot = getItem(position);
        Card card = new Card(documentSnapshot.getData(), true);
        progressBar.setProgress(card.calculateProgress());
        textView.setText(card.texts[0]);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd");
        dateTextView.setText(dateFormat.format(card.creationDate));

        return convertView;
    }

    @Override
    public int getCount() {
        return filteredList.size();
    }

    /**
     * Used by Filter to get size of original list
     * @return Count of unfiltered list
     */
    private int getUnfilteredCount() {
        return super.getCount();
    }

    @Override
    public void clear() {
        filteredList.clear();
        super.clear();
    }

    @Nullable
    @Override
    public DocumentSnapshot getItem(int position) {
        return filteredList.get(position);
    }

    /**
     * Used by Filter to get DocumentSnapshot of original list
     * @param position Position in array
     * @return DocumentSnapshot from unfiltered list
     */
    private DocumentSnapshot getUnfilteredItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new CompletedFilter();
    }

    private class CompletedFilter extends Filter {

        /**
         * Performs filtering according to user's choice in the settings
         * @param pattern True or False whether or not user wants to show completed cards
         * @return results Filtered list viewed in the mainactivity
         */
        @Override
        protected FilterResults performFiltering(CharSequence pattern) {
            boolean showCompleted = Boolean.parseBoolean(pattern.toString());

            FilterResults results = new FilterResults();

            int count = getUnfilteredCount();
            final ArrayList<DocumentSnapshot> newList = new ArrayList<DocumentSnapshot>(count);

            // Iterates through complete list and filters out on condition
            DocumentSnapshot currentDocument;
            Card currentCard;
            for (int i = 0; i < count; i++) {
                currentDocument = getUnfilteredItem(i);
                currentCard = new Card(currentDocument.getData(), true);
                if (currentCard.calculateProgress() < 100 || showCompleted) {
                    newList.add(currentDocument);
                }
            }

            results.values = newList;
            results.count = newList.size();
            return results;
        }

        /**
         * Updates the adapter with the filtered list
         * @param charSequence NOT used
         * @param filterResults filtered list from performFiltering method
         */
        @Override
        protected void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            filteredList = (ArrayList<DocumentSnapshot>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}