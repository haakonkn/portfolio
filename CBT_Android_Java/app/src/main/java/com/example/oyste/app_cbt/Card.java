package com.example.oyste.app_cbt;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Stores information about a Card
 * Is Serializable to be usable with Intents
 */
public class Card implements Serializable {
    String[] texts = new String[4];

    // Cards that are not owned by the user cannot be edited by the user
    boolean editable;

    Date creationDate;

    Card(){
        // Frode constructor
    }

    Card(Map<String, Object> data, boolean editable) {
        texts[0] = (String)data.get("A");
        texts[1] = (String)data.get("B");
        texts[2] = (String)data.get("C");
        texts[3] = (String)data.get("D");
        creationDate = (Date)data.get("Date");
        this.editable = editable;
    }

    Card(boolean editable, String A, String B, String C, String D) {
        this.editable = editable;
        texts[0] = A;
        texts[1] = B;
        texts[2] = C;
        texts[3] = D;
        creationDate = Calendar.getInstance().getTime();
    }

    public static Card makeBlankCard() {
        return new Card(true, "", "", "", "");
    }

    /**
     * Calculates a % progress
     * @return int between 0 and 100 depending on filled out cards
     */
    public int calculateProgress() {
        int progress = 0;

        for (String text : texts) {
            progress += !text.isEmpty() ? 25 : 0;
        }

        return progress;
    }

    /**
     * Creates a Map from the object
     * @return Map
     */
    public Map<String, Object> toMap() {
        Map<String, Object> result = new HashMap<>();

        result.put("A", texts[0]);
        result.put("B", texts[1]);
        result.put("C", texts[2]);
        result.put("D", texts[3]);
        result.put("Date", creationDate);

        return result;
    }

    /**
     * Clears all text fields
     */
    public void clear() {
        for (int i = 0; i < texts.length; i++) {
            texts[i] = "";
        }
    }
}
