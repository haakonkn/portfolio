package com.example.oyste.app_cbt;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {

    private SeekBar columns;
    private Switch online;
    private Switch completed;
    private TextView currentColumns;
    private Spinner numUsers;
    private Spinner numCards;
    private Spinner sort;
    private TextView  numUsersText;
    private TextView  numCardsText;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sort = findViewById(R.id.spin_sort);
        columns = findViewById(R.id.seek_columns);
        online = findViewById(R.id.switch_online);
        completed = findViewById(R.id.switch_completed);
        currentColumns = findViewById(R.id.txt_columns_current);
        numUsers = findViewById(R.id.spin_num_users);
        numCards = findViewById(R.id.spin_num_cards);
        numUsersText = findViewById(R.id.txt_num_users);
        numCardsText = findViewById(R.id.txt_num_cards);





        Button saveButton = findViewById(R.id.settings_save_button);
        Button cancelButton = findViewById(R.id.settings_cancel_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveChanges();
                finish();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        columns.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                currentColumns.setText(Integer.toString(progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        online.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                toggleOnlineOptionsVisibility();
            }
        });

        // Getting Values from preferences and setting the views' attributes
        SharedPreferences settings = getSharedPreferences(getResources().getString(R.string.shared_preferences), 0);
        online.setChecked(settings.getBoolean("online", false));
        completed.setChecked(settings.getBoolean("completed", true));
        int prefColumns = settings.getInt("columns", 1);
        columns.setProgress(prefColumns - 1);
        currentColumns.setText(Integer.toString(prefColumns));
        numUsers.setSelection(settings.getInt("numUsers", 0));
        numCards.setSelection(settings.getInt("numCards", 0));
        sort.setSelection(settings.getInt("sort", 0));

        toggleOnlineOptionsVisibility();

    }

    private void toggleOnlineOptionsVisibility() {
        if(online.isChecked()) {
            numUsersText.setVisibility(View.VISIBLE);
            numCardsText.setVisibility(View.VISIBLE);
            numCards.setVisibility(View.VISIBLE);
            numUsers.setVisibility(View.VISIBLE);
        } else {
            numUsersText.setVisibility(View.INVISIBLE);
            numCardsText.setVisibility(View.INVISIBLE);
            numCards.setVisibility(View.INVISIBLE);
            numUsers.setVisibility(View.INVISIBLE);
        }
    }

    @SuppressLint("ApplySharedPref")
    public void saveChanges() {
        SharedPreferences settings = getSharedPreferences(getResources().getString(R.string.shared_preferences), 0);
        SharedPreferences.Editor editor = settings.edit();

        editor.putInt("columns", columns.getProgress() + 1);
        editor.putBoolean("online", online.isChecked());
        editor.putBoolean("completed", completed.isChecked());
        editor.putInt("numCards", numCards.getSelectedItemPosition());
        editor.putInt("numUsers", numUsers.getSelectedItemPosition());
        editor.putInt("sort", sort.getSelectedItemPosition());

        editor.commit();
    }
}
