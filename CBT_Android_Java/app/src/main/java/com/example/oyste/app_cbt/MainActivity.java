package com.example.oyste.app_cbt;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity {
    final String TAG = "MAIN_ACTIVITY";
    final int SETTINGS_REQUEST = 1;
    final int SIGN_IN_REQUEST = 69;
    final int CARD_REQUEST = 42;

    private GoogleSignInOptions gso;
    private FirebaseFirestore database;
    private FirebaseAuth auth;
    private DocumentReference docRef;
    private SharedPreferences settings;

    private GridView gridView;
    private Button newButton;
    private Button settingsButton;
    private Button signinButton;
    private Button signoutButton;
    private TextView fetchingText;

    //commit needs to be used so that the change in prefs is instant
    @SuppressLint("ApplySharedPref")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        settings = getSharedPreferences(getResources().getString(R.string.shared_preferences), 0);

        gso = new GoogleSignInOptions
                    .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.token))
                    .requestEmail()
                    .build();

        newButton = findViewById(R.id.new_button);
        settingsButton = findViewById(R.id.setting_button);
        signinButton = findViewById(R.id.signin_button);
        signoutButton = findViewById(R.id.signout_button);
        gridView = findViewById(R.id.gridView);
        fetchingText = findViewById(R.id.fetching_text);

        auth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();

        setUpButtons();

        if(GoogleSignIn.getLastSignedInAccount(this) != null) {
            firebaseAuthWithGoogle(GoogleSignIn.getLastSignedInAccount(this));
        } else {
            updateUI(null);
        }
    }

    /**
     * Adds listeners to all buttons
     */
    private void setUpButtons() {
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivityForResult(settingsIntent, SETTINGS_REQUEST);
            }
        });

        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newButton.setEnabled(false);
                settingsButton.setEnabled(false);
                signoutButton.setEnabled(false);
                gridView.setOnItemClickListener(null);
                newButton.setText(getResources().getString(R.string.creating_card));
                docRef.collection("cards").add(Card.makeBlankCard().toMap())
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Intent cardIntent = new Intent(MainActivity.this, CardActivity.class);
                        cardIntent.putExtra("card", Card.makeBlankCard());
                        cardIntent.putExtra("id", documentReference.getId());
                        cardIntent.putExtra("editMode", true);
                        startActivityForResult(cardIntent, CARD_REQUEST);
                    }
                });


            }
        });

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GoogleSignInClient client = GoogleSignIn.getClient(signinButton.getContext(), gso);
                startActivityForResult(client.getSignInIntent(), SIGN_IN_REQUEST);
            }
        });

        signoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                GoogleSignIn.getClient(signoutButton.getContext(), gso).signOut();
                updateUI(null);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SETTINGS_REQUEST:
                handleSettingsRequest();
                break;
            case SIGN_IN_REQUEST:
                handleSignInRequest(data);
                break;
            case CARD_REQUEST:
                handleCardRequest(data);
                break;
            default:
                break;
        }
    }

    private void handleCardRequest(Intent data) {
        if (!settings.getBoolean("online", false)) {
            Card card = (Card)data.getSerializableExtra("card");
            if (card.calculateProgress() != 0) {
                docRef.collection("cards").document(data.getStringExtra("id")).set(card.toMap());
            } else {
                docRef.collection("cards").document(data.getStringExtra("id")).delete();
            }
        }

        updateUI(auth.getCurrentUser());

        if (settings.getBoolean("online", false)) {
            newButton.setEnabled(false);
        }
        newButton.setText(getResources().getString(R.string.new_button));
    }

    private void handleSignInRequest(Intent data) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            // Google Sign In was successful, authenticate with Firebase
            GoogleSignInAccount account = task.getResult(ApiException.class);
            firebaseAuthWithGoogle(account);
        } catch (ApiException e) {
            // Google Sign In failed, update UI appropriately
            Log.w(TAG, "Google sign in failed", e);
        }
    }

    private void handleSettingsRequest() {
        updateUI(auth.getCurrentUser());

        if (settings.getBoolean("online", false)) {
            newButton.setEnabled(false);
        } else {
            newButton.setEnabled(true);
        }
    }

    /**
     * Reads preferences and updates gridview accordingly
     */
    private void syncWithPreferences() {
        gridView.setNumColumns(settings.getInt("columns", 1));

        CardAdapter cardAdapter = (CardAdapter)gridView.getAdapter();

        int sort = settings.getInt("sort", -1);
        switch (sort) {
            case 0: // Date Ascending
                cardAdapter.sort(new Comparator<DocumentSnapshot>() {
                    @Override
                    public int compare(DocumentSnapshot t1, DocumentSnapshot t2) {
                        Date date1 = (Date)t1.getData().get("Date");
                        Date date2 = (Date)t2.getData().get("Date");
                        return date2.compareTo(date1);
                    }
                });
                break;
            case 1: // Date Descending
                cardAdapter.sort(new Comparator<DocumentSnapshot>() {
                    @Override
                    public int compare(DocumentSnapshot t1, DocumentSnapshot t2) {
                        Date date1 = (Date)t1.getData().get("Date");
                        Date date2 = (Date)t2.getData().get("Date");
                        return date1.compareTo(date2);
                    }
                });
                break;
            case 2: // Progress Ascending
                cardAdapter.sort(new Comparator<DocumentSnapshot>() {
                    @Override
                    public int compare(DocumentSnapshot t1, DocumentSnapshot t2) {
                        Card card1 = new Card(t1.getData(), false);
                        Card card2 = new Card(t2.getData(), false);
                        return card1.calculateProgress() - card2.calculateProgress();
                    }
                });
                break;
            case 3: // Progress Descending
                cardAdapter.sort(new Comparator<DocumentSnapshot>() {
                    @Override
                    public int compare(DocumentSnapshot t1, DocumentSnapshot t2) {
                        Card card1 = new Card(t1.getData(), false);
                        Card card2 = new Card(t2.getData(), false);
                        return card2.calculateProgress() - card1.calculateProgress();
                    }
                });
                break;
            default:
                break;
        }


        // The list get filtered according to the users choice in the settings
        cardAdapter.getFilter().filter("" + settings.getBoolean("completed", true));
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = auth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Snackbar.make(findViewById(R.id.gridView), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    /**
     * Gets cards from database and fills in gridview
     * @param user Logged in user
     */
    void updateUI(FirebaseUser user) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (user != null) {
                    newButton.setEnabled(!settings.getBoolean("online", false));
                    settingsButton.setEnabled(true);
                    signinButton.setVisibility(View.GONE);
                    signoutButton.setEnabled(true);
                    signoutButton.setVisibility(View.VISIBLE);

                    fetchData();
                } else {
                    // Sign in button, remove other stuff

                    if(gridView.getAdapter() != null) {
                        ((CardAdapter) gridView.getAdapter()).clear();
                    }

                    newButton.setEnabled(false);
                    settingsButton.setEnabled(false);
                    signinButton.setVisibility(View.VISIBLE);
                    signinButton.setEnabled(true);
                    signoutButton.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * Fetches data from database
     */
    private void fetchData() {
        Boolean online = settings.getBoolean("online", false);
        fetchingText.setVisibility(View.VISIBLE);

        if(!online) {
            fetchUserCards(auth.getCurrentUser());
        } else {
            fetchCommunityCards();
        }
    }

    /**
     * Finds other users and starts tasks to fetch their cards
     */
    private void fetchCommunityCards() {
        ArrayList<DocumentSnapshot> cards = new ArrayList<>();
        int numUsers = Integer.parseInt(getResources()
                .getStringArray(R.array.numUsersArray)[settings.getInt("numUsers", 1)]);
        database.collection("data").limit(numUsers).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                    if (!documentSnapshot.getId().equals(auth.getCurrentUser().getUid())) {
                        fetchCards(documentSnapshot.getId(), cards);
                    }
                }
            }
        });
    }

    /**
     * Fetches cards from a specific users, amount of cards depending on settings
     * @param userId User to fetch cards from
     * @param cards List with all cards found
     */
    private void fetchCards(String userId, ArrayList<DocumentSnapshot> cards) {
        int numCards = Integer.parseInt(getResources().getStringArray(R.array.numCardsArray)[settings.getInt("numCards", 1)]);
        database.collection("data").document(userId).collection("cards").limit(numCards).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                cards.addAll(queryDocumentSnapshots.getDocuments());
                populateGridView(cards);
                syncWithPreferences();
            }
        });
    }

    /**
     * Fetches all cards from local user
     * @param user Connected user
     */
    private void fetchUserCards(FirebaseUser user) {
        docRef = database.collection("data").document(user.getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        docRef.collection("cards").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                ArrayList<DocumentSnapshot> cards = new ArrayList<>();
                                cards.addAll(task.getResult().getDocuments());

                                populateGridView(cards);
                                syncWithPreferences();
                            }
                        });
                    } else {
                        HashMap<String, Object> newData = new HashMap<>();
                        newData.put("name", user.getDisplayName());

                        database.collection("data").document(user.getUid()).set(newData);
                    }
                }
            }
        });
    }

    /**
     * Passes card list to gridView and sets up gridView
     * @param cards ArrayList of cards
     */
    private void populateGridView(ArrayList<DocumentSnapshot> cards) {
        CardAdapter cardAdapter = new CardAdapter(getApplicationContext(), 0, cards);
        gridView.setAdapter(cardAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DocumentSnapshot document = (DocumentSnapshot)adapterView.getItemAtPosition(i);
                Intent cardIntent = new Intent(MainActivity.this, CardActivity.class);
                cardIntent.putExtra("card", new Card(document.getData(), !settings.getBoolean("online", false)));
                cardIntent.putExtra("id", document.getId());
                cardIntent.putExtra("editMode", false);
                startActivityForResult(cardIntent, CARD_REQUEST);
            }
        });

        fetchingText.setVisibility(View.GONE);
    }
}
