package com.example.oyste.app_cbt;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class TextFragment extends Fragment {
    private int index;
    private boolean editMode;

    public TextFragment() {
        // Required empty constructor
    }

    public static TextFragment newInstance(int index, boolean editMode) {
        TextFragment textFrag = new TextFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        args.putBoolean("editMode", editMode);
        textFrag.setArguments(args);
        return textFrag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        index = getArguments().getInt("index");
        editMode = getArguments().getBoolean("editMode");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View newView = inflater.inflate(R.layout.text_fragment, container, false);

        EditText editText = newView.findViewById(R.id.editText);

        switch (index) {
            case 0:
                editText.setHint(getResources().getString(R.string.hint_a));
                break;
            case 1:
                editText.setHint(getResources().getString(R.string.hint_b));
                break;
            case 2:
                editText.setHint(getResources().getString(R.string.hint_c));
                break;
            case 3:
                editText.setHint(getResources().getString(R.string.hint_d));
                break;
            default:
                break;
        }

        if (!editMode) {
            editText.setFocusable(false);
            editText.setBackground(null);
        }

        String text = ((CardActivity)getActivity()).card.texts[index];
        editText.setText(text);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                ((CardActivity)getActivity()).card.texts[index] = editable.toString();
            }
        });

        return newView;
    }
}